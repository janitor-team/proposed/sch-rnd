#!/bin/sh

help_params()
{
awk  "$@" '
BEGIN {
	prm=0
	q="\""
	fp_base=fp
	sub("[(].*", "", fp_base)
}

function urlencode(s)
{
	gsub("[#]", "%23", s)
	return s
}

function proc_include(fn)
{
	close(fn)
	while(getline < fn)
		proc_line()
	close(fn)
}

function proc_line()
{
	if (/^[^\"]*@@include/) {
		proc_include(gendir $2)
		return
	}

	if ((match($0, "@@desc") || match($0, "@@params") || match($0, "@@purpose") || match($0, "@@example"))) {
		txt=$0
		key=substr($0, RSTART, RLENGTH)
		sub(".*" key "[ \t]*", "", txt)
		HELP[key] = HELP[key] " " txt
		next
	}

	if (/@@param:/) {
		sub(".*@@param:", "", $0)
		p=$1
		sub(p "[\t ]*", "", $0)
		PARAM[prm] = p 
		PDESC[prm] = $0
		prm++
		return
	}

# build PPROP[paramname,propname], e.g. PPROP[pin_mask, dim]=1
	if (/@@optional:/ || /@@dim:/ || /@@bool:/) {
		key = $0
		sub("^.*@@", "", key)
		val = key
		sub(":.*", "", key)
		sub("^[^:]*:", "", val)
		PPROP[val,key] = 1
		return
	}

# build PDATA[paramname,propname], e.g. PDATA[pin_mask, default]=123
	if ((/@@default:/) || (/@@preview_args:/)) {
		key = $1

		txt = $0
		txt = substr(txt, length(key)+1, length(txt))
		sub("^[ \t]*", "", txt)

		sub("^.*@@", "", key)
		val = key
		sub(":.*", "", key)
		sub("^[^:]*:", "", val)
		PDATA[val,key] = txt
		return
	}

# build:
#  PDATAK[paramname,n]=key (name of the value)
#  PDATAV[paramname,n]=description (of the given value)
#  PDATAN[paramname]=n number of parameter values
	if (/@@enum:/) {
		key = $1

		txt = $0
		txt = substr(txt, length(key)+1, length(txt))
		sub("^[ \t]*", "", txt)

		sub("^.*@@enum:", "", key)
		val = key
		sub(":.*", "", key)
		sub("^[^:]*:", "", val)
		idx = int(PDATAN[key])
		PDATAK[key,idx] = val
		PDATAV[key,idx] = txt
		PDATAN[key]++
		return
	}
}

{ proc_line() }

END {
	if (header) {
		print "<b>" HELP["@@purpose"] "</b>"
		print "<p>"
		print HELP["@@desc"]
	}

	if (content)
		print "<h4>" content "</h4>"

	if ((print_params) && (HELP["@@params"] != ""))
		print "<p>Ordered list (positions): " HELP["@@params"]


	if (content) {
		print "<table border=1 cellspacing=0 cellpadding=10>"
		print "<tr><th align=left>name <th align=left>man<br>dat<br>ory<th align=left>description <th align=left>value (bold: default)"
		for(p = 0; p < prm; p++) {
			name=PARAM[p]
			print "<tr><td>" name
			if (PPROP[name, "optional"])
				print "<td> &nbsp;"
			else
				print "<td> yes"
			print "<td>" PDESC[p]
			print "<td>"
			vdone=0
			if (PDATAN[name] > 0) {
				print "<table border=1 cellpadding=10 cellspacing=0>"
				for(v = 0; v < PDATAN[name]; v++) {
					print "<tr><td>"
					isdef = (PDATA[name, "default"] == PDATAK[name, v])
					if (isdef)
						print "<b>"
					print PDATAK[name, v]
					if (isdef)
						print "</b>"

					print "<td>" PDATAV[name, v]
					if (PDATA[name, "preview_args"] != "") {
						prv= fp_base "(" PDATA[name, "preview_args"] "," name "=" PDATAK[name, v] ")"
						print "<td>"
# TODO: thumbnail
#						thumb(prv, thumbsize, PDATA[name, "thumbsize"], PDATA[name, "thumbnum"])
						print "&nbsp;"
					}
				}
				print "</table>"
				vdone++
			}
			if (PPROP[name, "dim"]) {
				print "Dimension: a number with an optional unit (mm or mil, default is mil)"
				if (PDATA[name, "default"] != "")
					print "<br>Default: <b>" PDATA[name, "default"] "</b>"
				vdone++
			}
			if (PPROP[name, "bool"]) {
				print "Boolean: yes/no, true/false, 1/0"
				if (PDATA[name, "default"] != "")
					print "; Default: <b>" PDATA[name, "default"] "</b>"
				if (PDATA[name, "preview_args"] != "") {
					print "<br>"
					print "<table border=0>"
					print "<tr><td>true:<td>"
#TODO thumbnail
#					thumb(fp_base "(" PDATA[name, "preview_args"] "," name "=" 1 ")", thumbsize, PDATA[name, "thumbsize"], PDATA[name, "thumbnum"])
					print "&nbsp;"
					print "<td>false:<td>"
#TODO thumbnail
#					thumb(fp_base "(" PDATA[name, "preview_args"] "," name "=" 0 ")", thumbsize, PDATA[name, "thumbsize"], PDATA[name, "thumbnum"])
					print "&nbsp;"
					print "</table>"
				}
			}
			if (!vdone)
				print "&nbsp;"
		}
		print "</table>"
	}

	if (footer) {
		print "<h3>Example</h3>"
		print HELP["@@example"]
		print "</a>"
		print "</body></html>"
	}
}
'
}


help_params "$@"

