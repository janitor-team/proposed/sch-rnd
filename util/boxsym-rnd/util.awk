#                            COPYRIGHT
#
#  cschem - modular/flexible schematics editor - box symbol generator utility
#  Copyright (C) 2022 Tibor 'Igor2' Palinkas
#
#  (Supported by NLnet NGI0 PET Fund in 2022)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  Contact:
#    Project page: http://repo.hu/projects/sch-rnd
#    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
#    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html

BEGIN {
	debug=0
	stderr="/dev/stderr"

	# given in pin_grid coords:
	border=0
}

function min(a, b) { return a < b ? a : b }
function max(a, b) { return a > b ? a : b }
function dbg(s) { if(debug) print s > stderr }
function Dbg(s) { print s > stderr }

function draw_box(SYM, w, h, border, shape)
{
	if ((shape ~ "^box") || (shape == ""))
		sym_rect(SYM, 0-border*pin_grid, 0-border*pin_grid,  (w+border)*pin_grid, (h+border)*pin_grid)
	else
		error("can not draw box of unknown shape " shape)
}
