ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				li:objects {
					ha:group.2 {
						loclib_name=library/symbol/passive/resistor-1.sym
						li:objects {
							ha:group.1 {
								x=20000; y=0;
								li:objects {
									ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
									ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
								}
								ha:attrib {
									name=2
									role=terminal
								}
							}
							ha:group.2 {
								mirx=1;
								li:objects {
									ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
									ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
								}
								ha:attrib {
									name=1
									role=terminal
								}
							}
							ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.value%; floater=1; }
							ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
							ha:polygon.5 {
								li:outline {
									ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
									ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
									ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
									ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
								}
								stroke=sym-decor;
							}
						}
						ha:attrib {
							-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
							-sym-license-dist=GPLv2+
							-sym-license-use=Public Domain
							-sym-source=sch-rnd default symbol lib
							name=R??
							role=symbol
							value=4k7
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=symbol; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				x=72000; y=100000;
				li:objects {
					ha:group.1 {
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=C1
					role=symbol
					value=100n
				}
			}
			ha:group_ref.3 {
				x=100000; y=100000;
				ref=/1/1/2
				li:child_xform {
				}
				ha:attrib {
					name=R1
					value=4k7
				}
			}
			ha:group.4 {
				li:objects {
					ha:line.1 { x1=92000; y1=100000; x2=100000; y2=100000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.5 {
				li:conn {
					/2/4/1
					/2/2/1/1
				}
			}
			ha:connection.6 {
				li:conn {
					/2/4/1
					/2/3/2/1
				}
			}
			ha:arc.7 { cx=96000; cy=52000; r=8000; sang=45.000000; dang=180.000000; stroke=sheet-decor; }
			ha:line.8 { x1=104000; y1=56000; x2=104000; y2=52000; stroke=sheet-decor; }
			ha:line.9 { x1=104000; y1=52000; x2=108000; y2=52000; stroke=sheet-decor; }
			ha:polygon.10 {
				li:outline {
					ha:line { x1=116000; y1=64000; x2=116000; y2=56000; }
					ha:line { x1=116000; y1=56000; x2=124000; y2=56000; }
					ha:line { x1=124000; y1=56000; x2=124000; y2=64000; }
					ha:line { x1=124000; y1=64000; x2=116000; y2=64000; }
				}
				stroke=sheet-decor;
				fill=hub;
			}
			ha:text.11 { x1=92000; y1=76000; dyntext=0; stroke=sheet-decor; text=Hello World!; }
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title=file format example
		}
	}
 li:sch-rnd-conf-v1 {
  ha:overwrite {
   ha:editor {
    grids_idx = 2
    grid = 4.0960 mm
   }
  }
 }
}
