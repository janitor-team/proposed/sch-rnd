0. old reports waiting for ACK =============================================

1. 0.9.4 (Beta) ==========================================================
+ ADMIN: apply for nlnet NGI0 Entrust with librnd4 and sch-rnd 1.0.0 before end of September [report: Igor2]

2. 0.9.5 (Beta) ==========================================================
- FEATURE: abstract model dialog
	- sort names
	- do not lose cursor pos on update
	- filter
- BUG: io_geda: bug_files/TODO/leptop-shift-click.sch: shift click to replace symbol imported lepton schematic: replacement symbol always ends up at 0;0 [report: Erich]
- BUG: mirror tool ignores lock-floaters [report: Igor2]
- BUG: devmap output attributes are hardwired prio=250 instead of inheriting the plugin's prio [report: aron]
- BUG: bug_files/TODO/attr_del.rs: select both symbols, {e p}, select devmap, click 'del' -> segf [report: Igor2]
- BUG: edit stroke, create new pen -> can't set width using up/down arrow when value is 0, 1 or 0k or 1k -> may be a librnd DAD bug [report: Igor2]
- BUG: bug_files/TODO/F.rs: copy the whole F shaped net to buffer, place in a way that the vertical wire endpoints hit the original horizontal wire; junction within the newly placed F disappears; csch_wirenet_recalc_unfreeze() [report: Igor2]
- CLEANUP: do not hardwire pcb/ lookups in export_tedax (should be display name only)
- FEATURE: expose DOTDIR (~/.sch-rnd, as ./configured) in the conf tree in so $(rc.path.user_dotdir) would work [report: Ade]
- FEATURE: indicate "sheet changed" on sheetsel, maybe with a * prefix [report: Aron]
- FEATURE: bom export [report: Erich]
- FEATURE: views, project file: GUI for editing engine list
- FEATURE: portmap/devmap: a singe terminal connected to multiple pins (multiple pinnums), e.g. fet in so(8) with drains and sources on may pins; at the moment one terminal is always compiled into one port with a single pin number [report: aron]
- FEATURE: project file handling corners: sch-rnd pool prj_corners:
	- indicate partial: {load by sheet names} vs. {root sheet list in project file}
		- compile should throw an error for partial project load: maintain a mapping to know which sheet is loaded for which file
		- context menu on sheetsel to load missing files
	- test project load with no file name list
	- project file with sheet A and B, but then load C as well; compilation should consider A and B only because we have a sheet list in the project file
- BUG: csch_grp_ref_embed(), used in sym loclib paste and right click symbol context menu toref (to loclib) conversion is not undoable [report: Igor2]
- TODO#38: rethink grp-ref-in-grp-ref with child xforms, maybe cache=1 is a bad idea
	- problem: ref1 -> ref2 -> grp -> text; ref2 is floater; if whole ref2 is rotated, we won't update anything in ref1's central xform list
- BUG: io_geda: load bug_files/translated-labels.sch. label of left-right mirrored and rotated NPN Q2 is drawn upside down in sch-rnd vs lepton-schematic rendering [report: erich]
	- BUG: load bug_files/spooky.rs and note that pinlabel floaters are not visible for darlington. viewing area determination/{z e} does not cope well with pinlabels with huge negative coordinates. but not evident with bug_files/spookyPositive.rs [report: Erich] 
- BUG: wiring: see bug_files/TODO/conn-residual.rs  launch sch-rnd with conn-residual.rs from CLI, select TP1 and vertical wire connected to it. drag it three grid points to the right. select TP1 and attached vertical wire again, and drag 3 grid point to the left, returning it to where it was originally. double click on horizontal wire. vertical stub is not connected to wire net. [report: Erich]
- BUG: wiring: see bug_files/TODO/conn-gone.rs launch sch-rnd with conn-gone.rs from CLI, select horizontal wire and delete it. select resistor and drag it two grid points to the right. select vertical wire attached to diode, and drag two grid points to the left. sch-rnd quits with assert [report: Erich]
- BUG: wiring: bug_files/TODO/undo-bug.rs load this sheet. select wire tool. start wire at 60,128, move cursor to 60,136, click, then move cursor to 72,136 and double click to finish new wire. Delete newly created horizontal wire. Use {u u} and get ERROR: Attempt to csch_undo() with Serial == 0. [report: Erich]
- BUG: wiring: bug_files/TODO/undo-bug.rs load this sheet. select wire tool. start wire at 60,128, move cursor to 60,136, click, then move cursor to 72,136 and double click to finish new wire. Use wire tool to create wire between TP4 and TP5. Use {u u} and see two wires disappear. [report: Erich]
- BUG: wiring: bug_files/TODO/intersect-fail.rs load this sheet. select connector CONN1 and attached wirenet with selection rectangle/cursor. move selection down 4 grid points so that CONN1 connected wire meets ground net wire. Ground net fails to have moved wire connected to CONN1 added to ground net. [report: Erich]
- BUG: wiring: bug_files/TODO/edit-copy-crash.rs load sheet. use cursor to select area from 235k,175k down to 247k, 137k. {e c}. segfault ensues. [report: Erich]
- BUG: wiring: load bug_files/TODO/wire-on-wire.ry then select wire tool. click at 40,40, then shift click at 36,44, then click at 40,48, then click at 36,48, then click at 40,52, then click at 36,52, then hit escape. use selection tool to select rectangle from 24,52 to 44,40, which will select newly drawn ground net. ctrl-x, then hit escape, then undo. sch-rnd unable to redraw wirenet. [report: Erich]
- BUG: wiring: load bug_files/TODO/wire-on-wire.ry then select wire tool. click at 40,40, then shift click at 36,44, then click at 40,48, then click at 36,48, then click at 40,52, then click at 36,52, then hit escape. {u u} {u u} {u u} {u r] {u r} {u r}. Use selection tool to select rectangle from 24,52 to 44,40, which will select newly drawn ground net. ctrl-x, segfault ensues. [report: Erich]
- BUG: wire transform: see bug_files/TODO/floaters.sch. start sch-rnd with floaters.sch, click on wire to highlight it. {e c}, click 32k to the right to place a copy of the wire. hit "esc" key. hover over newly placed wire, {a a}, enter label "bus2", hit enter, then hit floater button. Floater appears very distant to this new wire. Floater label displacement does not affect wires that have not undergone paste after copy from elsewhere. [report: Erich]
- BUG: io_geda (and others): scriptable attribute post-processing so refres can be renamed to name and pin attributes translated too [report: Erich]
- io_tinycad:
	- TOTEST: symdef field pos: does it matter? create test file and load in tinycad to see
	- BUG: io_tinycad: modify some of the FIELD show to 0, sym_attr_vis() will add a child xform with ->remove = 0 which is not respected
	+ SHOWS: displays builtin titlebox -> won't implement
	+ polygon:
		+ polygon=1: close
		+ fill=1: close+fill
	+ parse_name: in tinycad it's the tab name on the GUI for the sheet -> won't implement
	+ footprint attribute -> Package (free form)
	- parse_symdef_ref_point: what's power and part? maybe vcc/gnd uses power?
		- power: show power pin (sym_power.dsn)
			- pow1.dsn, pow2.dsn
			- there are two ref points, with power pin shown or not shown
			- <symbol> show_power tells which one to use _and_ tunes the position
		- part: it's the slot number (sym_power.dsn)
			- all pins of all slots are drawn!
	+ rotated labels don't work, parse_label ignores rot
		+ sym and term labels are always horizontal
		+ net label: labrot.dsn
	+ rotated text doesn't work, parse_text ignores rot -> textrot.dsn, only two are valid
	+ net label styles: labmode.dsn
	- rotated label with arrow (group not rotated): labmodern.dsn
- BUG: load bug_files/translated-labels.sch. use rotate tool on centre point of Q2 label/refdes. Translation as well as rotation occurring. [report: erich]
- BUG: load bug_files/impliedwires.sch and note lack of connections between power rails and resistor which are implied by coincident terminal ends when a netlist is generated in gschem/lepton [report: Erich] 
- BUG: wire merge bug: bug_files/merge.* [report: Aron]

3. Low prio ==========================================================
- BUG: {e t} over a non-dyntext, resize window larger: entry remains small [report: aron]
- FEATURE: consider dangling wire end indication (see pool node)
- BUG: wirenet in group should work: load symnet.rs. select terminal and adjacent vertical line using negative selection box; convert selection to symbol. connect test point 1 to wire net. export netlist. do not assume wirenet is directly under the &direct in the tree [report: Erich]
- BUG: back annotation: abstract model: abstract model UUIDs are not implemented, annotation doesn't use them; either figure persistent uuids or use CMRs [report: Igor2]
- BUG: rewrite get_prjname() in dytext render
	- figure the path of the project file
	- may project name change runtime? if so, inalidate text objects (->rtext = NULL using csch_text_dyntext_inval()) to re-render the new name
- OPTIMIZE: do not re-create views multiple times in sch_rnd_prj_conf2prj(): start 'sch-rnd A.rs B.rs' from the same dir [report: Igor2]
- CLEANUP: code dups with pcb-rnd, consider moving some code to src_3rd/rnd_inclib:
	- query
	- propedit
	- undodialog
	- rename csch_ symbols to sch_rnd_ in plugins/
	- act_read
- librnd4.0.0:
	- remove the whole project loading plug io: project files will be handled by librnd
	- once multi is moved over:
		- extend oidpath to generate and accept sheet prefix with $uuid/
		- act_draw should be able to use it as scope
		- act_draw should be able to return oidpath with $uuid/
		- query() should be able to return/convert lists like that for scripting

4. After beta ==========================================================
- a connection object should have x;y displacement for the graphical object to be useful (or is it a grp_ref?); at the moment we are not drawing it at all
- FEATURE: DRC (requires query() on the abstract model):
	- noslot attribute (e.g. for resistors)
	- "require graphical connection" attribute for gnd and vcc and rail, see TODO#rgc
	- figure if fully overlapping ports (or symbols) can be or should be detected (see: two gnd symbols on top of eachother) [report: Erich]
	- it is easy to accidentally add a footprint to a terminal on a symbol instead of the symbol itself. This is not flagged on netlist export. Should it be harder to do this, or maybe a netlist exporter could indicate if footprints associated with terminals in symbols were not included in the export? [report: Erich]
	- accidentally adding a name to a rail exports a connection in the netlist with no associated component. Perhaps this would benefit from some sort of DRC check, like the "footprint attribute put on non-symbol" issue above. [report: Erich]
- FEATURE: text vertical alignment (in design doc and code); same rules as in halign [report: Ade]
- FEATURE: export of the schematic as a pcb-rnd/tedax subcircuit/footprint for placement on the PCB as a graphical element? For simpler circuits, this would be good for "documentation on the silk layer". [report: Erich]
- BUG: copy&paste wirenet line extension merge, see TODO#merge31 in the code [report: Igor2]

5. TODO() tags ==========================================================
	- symedit:    needed for symbol editor support
	- bitmap:     needed for bitmap objects
	- fungw:      may need fungw API change
	- multi:      multiple sheet support
	- hierarchic: needed for hierarchic projects
