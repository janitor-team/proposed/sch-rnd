<html>
<body>
<h1> 6. cschem implementation - hierarchy </h1>

<p>{imp6:0}
Hierarchical design is implemented in a way that keeps non-hierarchical
design simple while does not complicate hierarchical design too much either.
Carefully designed non-hierarchical sheets can be easily reused in hierarchical
designs - the most important thing to note is how global networks are used.

<h2> 6.1. Topologies </h2>
<p>{imp6:1}
The following table demonstrates all 3 topologies mentioned in the specification:
<br>
<table border=1 cellspacing=0>
<tr>
	<th>flat
	<th>single-tree
	<th>multi-tree
<tr>
	<td><img src="06_topo1.svg">
	<td><img src="06_topo2.svg">
	<td><img src="06_topo3.svg">
</table>

<p>{imp6:2}
Notable special cases:
<ul>
	<li> A single-sheet schematics is considered flat
	<li> A multi-page schematics with a single train of reference is considered hierarchical
	<li> as the above multi-tree example shows, a single root page with no children is also considered a subtree in such a setup
</ul>


<p>{imp6:3}
The multi-tree setup could have a new top sheet that just references all the roots
and it's then a single-tree setup. So how does the multi-tree setup differs from
the single-tree setup? Only that it does not have such a single top-level sheet
that references all other. The reason to keep this possibility open is the
project file. A project file can list all the top level sheets (roots), having to have
a redundant or extra top level file just for this list would be annoying. On
the other hand, the project file can describe non-cschem-aspects of the project,
which a schematics sheet can't, so it's not possible to omit the project file
as an optional feature.


<h2> 6.2. Hierarchical example for all network parameter types </h2>

<p>{imp6:4}
Assume there is a project with two sections: a sensitive analog circuitry
(called "<code>an</code>") and a high frequency logical control circuitry (called
"<code>dig</code>"). The analog circuit contains 4 copies of the same subcircuit
("<code>amp</code>") and some glue. The whole design is a module that could be used in
a bigger circuit.

<p>{imp6:5}
This section gives one of the many possible ways to implement this with cschem,
described in a top-down way.

<p>{imp6:6}
Because of its simplicity, the single-tree topology is chosen. A top sheet
is created with two symbols: one for "an" and one for "dig", with
symbol attribute cschem/child/name set to "<code>an</code>" and "<code>dig</code>" respectively.
<p>
<img src="06_example1.svg">

<p>{imp6:7}
There will be a few control signals that need to travel between the two;
these are added as terminals to both symbols and they are connected with
wires-nets on the top sheet. <code>an</code> and <code>dig</code> also need to take power,
"<code>Vcc</code>" and "<code>GND</code>" terminals are added on both. The top sheet is a good place to
connect the Vcc of the two worlds with ferrite beads and capacitors added
to avoid digital noise ending up in "<code>an</code>", separating the networks to 
avcc/dvcc and agnd/dgnd.

<p>{imp6:8}
We will have a subtree-local "<code>enable</code>" signal that should be used by both
"<code>an</code>" and "<code>dig</code>" to enable the circuitry. Instead of passing this through terminals,
we will define it as a subtree-local circuit, "<code>netname=v/enable</code>" on the top
sheet.

<p>{imp6:9}
Since the whole project would be a reusable part within a bigger project, the
top page should have terminals to get networks from a parent later. These
terminals shall be named, placed on the sheet and connected to the a network.
Our network "<code>v/enable</code>" will be connected to a sheet terminal called "<code>enable</code>".

<p>{imp6:10}
For "<code>dig</code>" we create a single sheet. It should have sheet-level terminals
matching the ones we used on the top sheet for the "<code>dig</code>" symbol. We do
not have a terminal for the enable signal, tho: whenever we need to connect
a symbol terminal to it, we can just add a "<code>connect=^enable</code>", which means
"connect to our parent's enable signal". The rest of the signals and
connections can be realized normally, as if this was the only sheet; named
networks should have the "<code>./</code>" prefix in their netname to avoid binding to global
nets.

<p>{imp6:11}
For "<code>an</code>", we follow the same procedure, except it won't have much of its own
circuitry will be 4 copies of the same symbol referencing to "<code>amp</code>". Most probably
"<code>an</code>" will never need to use the "<code>enable</code>" signal.

<p>{imp6:12}
We create the "<code>amp</code>" sheet, the same way as we did create the digital
sheet. We again can use the "<code>connect=^enable</code>" trick to connect terminals to
the enable signal defined on our top sheet.

<p>{imp6:13}
Finally, there should be a global chassis ground. Every module that has
any electronics in it shall be connected to the chassis ground. This is
realized by a global network chgnd, assumed to "just exist". It would be
normally created by the user of our subtree. Connection to the chassis
ground is done by "<code>connect=chgnd</code>" (which does <b>not</b> enforce "<code>chgnd</code>"
to be global, but allows that).

<p>{imp6:14}
The final "flow of nets" is as shown on the figure below. Blue objects are
sheet-local; red arrows represent a connection through terminals on both ends;
green lines are subtree-local network bindings, without the use of terminals.
Purple lines are global network bindings.
<p>
<img src="06_example2.svg">

<h2> 6.2. Considerations for the example </h2>

<p>{imp6:15}
We did not enforce "<code>chgnd</code>" to be global, but referenced it without a
prefix: lookup scope is "<code>auto</code>". This means a search is started from
bottom up, and the first subtree-local "<code>chgnd</code>" can be used, or as a
fallback, the global one will be used (or even created if nobody else did yet).
It's a good practice to do this: we can not be sure what hierarchy we'd have
above us, and it may be that the user will want to quarantine us chgnd-wise,
by creating a "<code>v/chgnd</code>" a few levels up. This way the "<code>chgnd</code>" network can be
split up by subtree.

<p>{imp6:16}
Referencing the enable network from amp as "<code>^/enable</code>" is safe: even if
the hierarchy above us have "<code>v/enable</code>" defined, the search will always
stop at the innermost hit, so "<code>amp</code>" will always bind to the "<code>v/enable</code>" of
"<code>an</code>".

<p>{imp6:17}
This is a typical use of subtree-local networks: instead of complicating the
API of both "<code>an</code>" and "<code>amp</code>" by more ports for networks that will be shared
by all instances anyway. It's sort of a global network, without "infecting"
other parts of the hierarchy. The actual way it's bound to ext_enable can
be a simple "<code>connect=v/enable</code>" on the sheet level (input) terminal for
the "<code>enable</code>" network on the top sheet. This would both create "<code>v/enable</code>"
and connect it to the input.

<p>{imp6:18}
Reusing "<code>amp</code>" in another project without "<code>an</code>" is easy. The API of
"<code>an</code>" is defined as:
<ul>
	<li> the symbol that references "<code>an</code>" must have 3 ports (agnd, avcc, ctrl) 
	<li> one of the parents of "<code>an</code>" in the new hierarchy must provide a "<code>v/enable</code>" network
	<li> there should be a global "<code>chgnd</code>" network, created anywhere (but it's not fatal if there is none)
</ul>

<p>{imp6:19}
In a hierarchical setup global nets like chgnd is dangerous and probably
better to avoid. For example if no other sheet creates a chgnd, multiple instances
of "<code>an</code>" will still create one and get connected through it in a way that this
network is then never really grounded. The obvious reason chgnd is included
in the above example is to demonstrate how global nets work. The reason
the concept of global nets exists in cschem is to support the flat topology.

<h2> 6.3. How the netlist would be generated </h2>

<p>{imp6:20}
In case of flat netlist syntax: the hierarchy exists only in the concrete
model - once the project is compiled into an abstract model, that model is flat.
Generating the flat netlist from the flat abstract model should be trivial,
especially that names are already unique and flat in the abstract model.

<p>{imp6:21}
In case of hierarchic netlist: there are strong cross-referencing between
the flat abstract model and the hierarchic concrete model. The exporter
plugin would start working from the abstract model and by looking at
the cross references it would be able to understand the original hierarchy.
