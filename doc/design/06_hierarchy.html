<html>
<body>
<h1> 6. Hierarchy </h1>

<p>{des6:0}
The abstract model is flat, but the source (the concrete model) from which
the abstract model is generated may be hierarchic. Since each object of the
flat abstract model remembers which concrete model object(s) it was derived
from, the output can again be hierarchical.

<p>{des6:1}
In that flat abstract model, there is only one namespace for components
and one namespace for networks. The component namespace consists of
component names (and uids), and the network namespace consists of network 
names (and uids).

<p>{des6:2}
Since networks are often referenced by name from the schematics, an auxiliary
global namespace is provided for networks that are used in the concrete model,
where they are identified by their netname and <a href="#des6:8">scope</a>.

<h2> 6.1. Topology </h2>

<p>{des6:3}
A multi page project is called hierarchical if at least one sheet  <a
href="#des6:15">references to another sheet</a>. The system of references
forms one or more loop-free trees, each with a single root sheet. The
list of root sheets is considered the "top level list of root sheets" or in
short, "root sheets" (or "top level sheets") for the project.

<p>{des6:4}
Hierarchy affects how networks are created and referenced.
When a network is referenced by netname, the reference is either:
<ul>
	<li> without a prefix, in which case an <a href="#des6:14">auto search</a>
	     is performed within the hierarchy to find the closest network of
	     matching netname
	<li> or with a prefix, in which case a <a href="#des6:11">controlled search</a> 
	     is performed
	     within the hierarchy to find the closest network of that name
</ul>

<h3> 6.1.1. multi-page flat project</h2>

<p>{des6:5}
If a project contains multiple schematics pages without hierarchical
cross-references, its topology is called a multi-page flat project. In
other words the top level consists of multiple root sheets but no sheet has
references to other sheets. It is possible to connect networks between
the pages using global nets.

<h3> 6.1.2. single-tree hierarchy </h2>

<p>{des6:6}
There is a single root sheet and all other sheets are accessible through
sheet references. In other words, the top level consists of only one
sheet and that sheet has references to other sheets. This setup can
exploit the subtree based netname references. The root sheet is very often
a map of the first level sheets and the connections between them.


<h3> 6.1.3. mixed: multi-tree hierarchy </h2>
<p>{des6:7}
There are multiple root sheets, some of them with sheet references. In
other words, the top level consists of a list of trees. This setup can
exploit the subtree based netname references and global nets.  The root
sheets are often a map of the first level sheets and the connections
between them, but there's no explicit map of the root sheet
interconnects, only the implicit binding of global networks.
Root sheets are typically listed in the project file.


<h2> 6.2. Netname scoping </h2>

<p>{des6:8} <a id="des6:8">
Network names (netnames) determine the scope (visibility) of the network
within the tree of sheets in the concrete model. The scope of a network
is always one of the scopes from the table below.

<table border=1 cellspacing=0>
	<tr><th>scope         <th> syntax                  <th> description
	<tr><td>global        <td><code> /netname   </code><td> visible from anywhere in the hierarchy
	<tr><td>subtree-local <td><code> v/netname  </code><td> visible only from within a subtree of the hierarchy
	<tr><td>sheet-local   <td><code> ./netname  </code><td> visible only from within the same sheet
	<tr><td>auto          <td><code> netname    </code><td> bind to an existing sheet-local net if available, else to a subtree-local net, else to a global net, else create a new global net
	<tr><td>subtree-auto  <td><code> ^/netname  </code><td> similar to auto, but is looking only for subtree-local (<code>v/</code>) networks - no locals and no globals
</table>


<p>{des6:9}
<b>Global nets</b> are accessible from any point from the hierarchy. If a subtree-local
or sheet-local network has the same name as a global net, it is shadowing the
global net: the auto references will prefer those local networks. However,
the global network can always be referenced using the / prefix, which overrides
the auto search.

<p>{des6:10}
Global networks are created when first referenced (with the explicit <code>/</code>
prefix syntax or with the auto or subtree-auto syntax).

<p>{des6:11} <a id="des6:11">
A <b>subtree local net</b> can be regarded as a "group local" network, is
visible under a subtree. It is intended to provide a mechanism for creating
a "global net" locked into a local tree of sheets, allowing the tree to
be reused as part of a larger project without introducing interfering global
networks.

<p>{des6:12}
Defining a new subtree-local network is done by using the netname prefix
<code>v/</code>; this anchors the netname to the current sheet. Auto netnames from
this sheet and sheets below this sheet will find this network if looking
for a sheet-local network failed. To prefer the nearest subtree-local
network over local networks, the <b>subtree-auto</b> <code>^/</code> prefix can be
used: this will start walking up in the hierarchy to find the closest
subtree-local net of matching netname. If that fails and the root sheet
is reached, the compilation fails (no abstract model is produced).

<p>{des6:13}
A <b>sheet local net</b> is visible only within the same sheet. It is
intended to provide a mechanism for creating a local network in a sheet
that is going to be reused as a part of a hierarchy, without the local
netname interfering with global netnames.

<p>{des6:14}<a id="des6:14">
The most common netname is the <b>auto</b> netname, without prefix. This
reference will do an upward tree search for the closest suitable netname:
<ul>
	<li> 1. if there is a sheet-local net with the same netname, use that
	     and stop the search
	<li> 2. visit each parent recursively until (including) the root sheet;
	     if there is a matching subtree-local net defined in that sheet
	     using the <code>v/</code> prefix, use that and stop the search
	<li> 3. look for an existing global net with matching netname and use that
	     if found, stop the search
	<li> 4. create a new global net using the netname
</ul>

<h2> 6.3. Hierarchical reference </h2>

<p>{des6:15}<a id="des6:15">
A concrete schematics sheet may reference another sheet via a symbol.
The symbol represents a new instance of the referenced sheet.

<p>{des6:16}
The current sheet with the symbol is called the parent sheet and the
referenced sheet "behind the symbol" is called the child sheet. The
parent-child relation is the one represented by the hierarchical tree of
the schematics sheets.

<p>{des6:17}
The name of the sheet is specified by one of the following attributes
(only one of these shall be specified in a symbol):

<table border=1 cellspacing=0>
	<tr><th>attribute key                  <th> value syntax <th> meaning
	<tr><td><code>cschem/child/uid</code>  <td> uid          <td> first the project then the hlibrary is searched for a sheet with matching uid
	<tr><td><code>cschem/child/name</code> <td> string       <td> first the project then the the hlibrary is searched for a sheet with matching file name; if contains any slash character, it's a path within the hlibrary relative to the current sheet's path within the hlibrary
	<tr><td><code>cschem/child/path</code> <td> string       <td> path relative to the directory the current sheet is last saved in
</table>

<p>{des6:18}
Configuration setting <i>hlibrary</i> is a list of paths where schematics
sheets for use in hierarchy are stored on the file system. It is similar, but
orthogonal to the symbol library paths (but upon the user's decision there
might be overlap between the two).

<p>{des6:29}
An implementatin that supports project files typically keeps a list of all
root sheets in the project file and another (auxiliary, aux) list of all
other referenced from the root sheets. When a new reference is made (new
symbol is placed with cschem/child/* attribute), the aux list of the project
file is extended with the new file. Thus the project file always contains a
full list of sheet files needed for reproducing the hierarchy.


<h2> 6.3.1. Network connections - network parameters </h2>

<p>{des6:19}
Networks between the current schematics and the referenced schematics are
connected in explicit and implicit ways. Implicit (non-graphical) connections
are done by <a id="des6:8">referencing a netname</a>,
using the <a id="02_data.html#des2:37">"<code>connect=</code>" attribute.</a>

<p>{des6:20}<a id="des6:20">
The explicit way of connecting networks is using the terminals
of the symbol. On the parent sheet, symbol terminals and bus-terminals
can be connected to local or global networks. On the child sheet, these
connections are realized by terminals and bus-terminals placed
<a href="02_data.html#des2:76">directly on the sheet, outside of symbols</a>.

<p>{des6:21}
Explicit passing of a network using terminals helps keeping the namespaces
clean: the short, virtual "network segment" between the parent and the
child has no global name, only sheet-local representations as terminals on
both sides of the relation. It can also be regarded as a "network parameter"
for the subcircuit instance.

<p>{des6:22}
The number, the type (terminal vs. bus-terminal) and the <i>name</i> attribute
of the ports of the symbol and the child sheet must match. Any mismatch results
in a compilation error (no abstract model is created).

<h2> 6.3.2. Attributes - attribute parameters </h2>

<p>{des6:23}
If the parent symbol contains attributes whose key start with the
<code>cschem/param/</code> prefix, those attributes are copied into the child sheet
instance, with the same key and value, as sheet attributes.

<p>{des6:24}
This mechanism is provided as a safe, explicit way for passing attributes
as arguments. Because of the key prefix limit, the parent can not
accidentally overwrite child sheet attributes. The API (the list
of <code>cschem/param/*</code> keys the child sheet will understand) can be specified
by the user and made known for both the implementor of the child sheet
and the parent sheet.

<p>{des6:25}
In a typical application, on a child sheet, symbol, terminal and other
object attributes will symlink to "<code>sheet::cschem/param/</code>" keys to access
the parameters specified by the parent.

<h2> 6.4. Other considerations </h2>

<p>{des6:26}
Unnamed wire-nets are always treated as being local (as if they had a
<code>./</code> prefix in their netname) as they shall never connect to a global network
by name.

<p>{des6:27} <a id="des6:27">
Abstract model netname generation: since the netname in the abstract model
must be project-wise unique, it can not be the same name used in a hierarchical
schematics. The exact details of the name translation is up to the plugin
that implements the translation. A typical example would be:
"<code>refdes/refdes/refdes/netname</code>" where <code>refdes</code> is the hierarchy down-reference
symbol's refdes attribute and <code>netname</code> is the netname attribute of the network
with the scope prefix removed. If the user doesn't chose a plugin, the default
behavior is to use the network's uid as abstract netname. Normally the
renaming does not affect global nets, only subtree-local and sheet-local nets.

<p>{des6:28} <a id="des6:28">
Similar to netnames, abstract component names may be required to be unique
by the exporter plugin. In this case either the exporter plugin or an
intermediate plugin is used to attach human readable unique names deduced
from the hierarchy (e.g. "<code>refdes/refdes/refdes</code>"). Just like with the netnames,
this name translation is optional.

