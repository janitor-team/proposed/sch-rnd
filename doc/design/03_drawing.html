<html>
<body>
<h1> 3. graphical schematics sheets - drawing primitives </h1>

<p>{des3:0}
This chapter describes all drawing primitives that can be used on
a graphical schematics sheet as well as the data structure of the
graphical schematics sheet.

<p>{des3:1} A drawing object is a drawing primitive, a group or a sheet.

<p>{des3:2} A drawing object is fully specified by:
<ul>
	<li> the drawing object type, which is one of the types defined in this document (e.g. line or arc or group)
	<li> properties
	<li> attributes
</ul>

<p>{des3:3} Properties are fields that are typically not stored or
edited by the user in plain text form, and/or are produced and consumed
by the code and are not interesting for the user. Attributes are typically
textual data that are easy to edit as text.

<p>{des3:4} All properties are present in the memory and file format.
When a property needs to be "turned off" (e.g. a group should not be
rotated), the property still has its slot in the file format and in the
memory, but the value is set to a special "unspecified" value.

<p>{des3:5} A valid cschem implementation must implement the functionality
of all mandatory properties. If an implementation does not implement the
functionality of an optional property, the implementation must be able
to load, store and save the value of that property. Whether the
functionality of a property
must be implemented or not can be found in the <i>mandatory</i> column
of the property tables.

<p>{des3:6} All attributes are optional.

<p>{des3:7} Drawing objects are stored in a tree structure, with the top level
single root being the sheet and leaves being atoms.

<p>{des3:8} The following figure demonstrates the tree structure
(parent -&gt; child). Rectangular objects have attributes.

<p><img src="03_stack.svg">


<p><table border=1 cellspacing=0>
<tr><th colspan=6 bgcolor="#aaaaaa"> {des3:9} drawing object types
<tr><th> term      <th> type     <th> properties <th> attributes <th> children         <th> summary
<tr><td> line      <td> atom     <td> yes        <td> no         <td> none             <td> straight line segment
<tr><td> arc       <td> atom     <td> yes        <td> no         <td> none             <td> circular arc
<tr><td> spline    <td> atom     <td> yes        <td> no         <td> none             <td> (spline)
<tr><td> text      <td> atom     <td> yes        <td> no         <td> none             <td> multiline text
<tr><td> polygon   <td> atom     <td> yes        <td> no         <td> none             <td> polygon with curved edges
<tr><td> connection<td> atom     <td> yes        <td> no         <td> none             <td> logical connection
<tr><td> bitmap    <td> atom     <td> yes        <td> no         <td> none             <td> embedded bitmap
<tr><td> group     <td> composite<td> yes        <td> yes        <td> atoms, composites<td> group of atoms and composites
<tr><td> group_ref <td> composite<td> yes        <td> yes        <td> atoms, composites<td> reference to another group
<tr><td> sheet     <td> sheet    <td> yes        <td> yes        <td> atoms, composites<td> root of the tree
</table>

<h2> 3.1. Common drawing object properties </h2>

<h3> 3.1.1. Local identifier </h3>

<p>{des3:10} An <i>oid</i> is a signed 32 bit integer. Any time a new
drawing object is placed on a sheet or within a group, the new drawing object's <i>oid</i> is set to
the group's or sheet's <i>next_oid</i> and the group's or sheet's <i>next_oid</i> is incremented.

<p>{des3:11} The <i>oid</i> value 0 means "invalid" or "unused" or "unspecified".
Any non-zero <i>oid</i> is a valid drawing object ID. The same <i>oid</i> can not
be attached to two different drawing objects within a group or sheet.

<p>{des3:71} Only positive <i>oid</i> values shall be used in files or
communication, negative values are reserved for application-internal use.

<p>{des3:12} The <i>oid</i> is used for referencing drawing primitives
within the group or sheet. It is meant for internal references only: external objects,
such as other sheets, shall not use <i>oid</i> to reference into the sheet.

<p>{des3:13} If <i>next_oid</i> overflows, <i>oid</i>s are allocated using
a slower mechanism that still result in unique numbers. If there are 2^32-1
drawing objects on the sheet, no more drawing objects shall be added.

<p>{des3:72} An <i>oid-path</i> is a slash separated list of <i>oid</i>s.
It is a top-down description of an object path: it contains zero or more
<i>composite</i> object <i>oid</i>s then descending through groups until
reaching the final object that's being addressed.

<p>{des3:73} An <i>oid-path</i> is always an absolute path, root
being the sheet. An <i>oid-path</i> is invalid if any prefix path of it
refers to a non-existing object. The leading slash is mandatory.

<h3> 3.1.2. Common graphical features </h3>

<p>{des3:14} <i>color</i>: 24 bit #rrggbb (2 digit numbers written as hex,
00 means darkest, ff means brightest). There is no alpha, there is no
drawing object level translucency.

<p>{des3:15} Most drawing objects are drawn with a <i>pen</i>.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:16} pen properties
<tr><th> property     <th> mandatory <th> description
<tr><td> name         <td> yes       <td> string: name assigned by the user; used for addressing pens; unique within the group
<tr><td> tip shape    <td> yes       <td> circle (also called round) or square; when square, an edge of the square is always aligned with the stroke direction
<tr><td> tip size     <td> yes       <td> circle diameter or square edge length in coordinates
<tr><td> <i>color</i> <td> yes       <td> &nbsp;
<tr><td> font_height  <td> yes       <td> font height in coords for text objects (string)
<tr><td> font_family  <td> no        <td> hint: font family name for text objects (string)
<tr><td> font_style   <td> no        <td> hint: font style for text objects (string; e.g. bold, italic)
<tr><td> dash         <td> no        <td> dash pattern
<tr><td> dash_period  <td> no        <td> length in coords a full dash pattern cycle
</table>

<p>{des3:17} When using a pen, the base geomerty is specified as the single, 0
wide centerline. For a round tip pen, this means the actual rendered pixels
extend to exactly ((tip size)/2) pixels in all directions from any point of
the base geometry. For example if the base geometry is a 10 units
long horizontal line and the tip size is 4, the total length of the rendered
line is 2+10+2=14 units.

<p>{des3:18} Dash pattern: an unsigned 16 bit integer, a bit pattern, each
bit represents 1/16 dash_period long stroke, 1 means draw, 0 means invisible.
If dash value is 0x0000 or 0xFFFF or dash is not specified, solid stroke is drawn.
If dash_period is not specified or 0, it is (tip size)*128.

<p id="des3:74">{des3:74} Pens are defined per group. When an object refers to a pen,
it uses the <i>name</i> of the pen. The pen is searched first in the group of
the object; if not found, in the parent group, then in the grandparent group,
etc. If no pen with the given oid (or name) is found when reaching the root, a
warning is issued and a safe (implementation-specific) default pen is used.

<h3> 3.1.3. Coordinate system </h3>

<p>{des3:19} Coordinates on the sheet are stored as unit-less integer x;y.
Negative coordinates are supported. The drawing has no special spot, the
user is free to use any coordinate values. The point at 0;0 is not
special either, but it is recommended that the first drawing object of a new
design is placed near to 0;0 to allow the design to expand in any
direction.

<p>{des3:20} Cschem implementations are expected to handle at least 32 bit
wide, signed integer coordinates. When a drawing is presented in normal
orientation, the X axis is horizontal, increasing to the right, the Y
axis is vertical, increasing upward.

<p>{des3:21} Angles are specified in degrees between [0 .. 360) for
absolute values and between [-360 .. +360] for relative values. Degree 0
is at x&gt;0;y=0; positive angle is toward CCW. Angles are stored as
floating point numbers, with precision at least matching the binary32
(single precision) of IEEE 754.

<p>{des3:22} The coordinate system is demonstrated by the following figure:
<p><img src="03_coords.png">

<h3> 3.1.4. Common properties </h3>
<p>{des3:80} Every drawing and group object has these properties with the same
meanining. These common properties are not listed in each object type's
property list in the following sections.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:81} common properties
<tr><th> property   <th> value         <th> description
<tr><td> lock       <th> boolean       <th> if true, the object is locked (should not be selected or edited individually); if not specified, defaults to <b>false</b>
<tr><td> floater    <th> boolean       <th> if true, bypass the "group lock"; non-floater objects are group-locked by default (can't be selected or edited individually) if they are part of a group other than sheet's direct; if not specified, defaults to <b>false</b>
</table>



<h2> 3.2. Atoms </h2>

<h3> 3.2.1. Line (segment) </h3>
<p>{des3:23} A straight line segment represented by a single contour line and
no fill and no attributes and no children.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:24} line properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td> &nbsp;
<tr><td> p1         <td> yes       <td>x;y coords of the starting point
<tr><td> p2         <td> yes       <td>x;y coords of the ending point
<tr><td> stroke     <td> yes       <td><i>pen</i> to use for drawing the line
</table>

<p>{des3:25} Stroke may be unspecified if the line is part of a polygon
outline.

<h3> 3.2.2. Arc (segment) </h3>
<p>{des3:26} An arc segment represented by a single contour line and
no fill and no attributes and no children.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:27} arc properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td>&nbsp;
<tr><td> cx, cy     <td> yes       <td>x;y coords of the center point
<tr><td> r          <td> yes       <td>radius in coord units
<tr><td> sang       <td> yes       <td>start angle [0..360)
<tr><td> dang       <td> yes       <td>delta angle [+-360]
<tr><td> svalid     <td> no        <td>boolean to indicate whether start-point coord is valid
<tr><td> sx, sy     <td> no        <td>exact x;y coords of the start-point
<tr><td> evalid     <td> no        <td>boolean to indicate whether end-point coord is valid
<tr><td> ex, ey     <td> no        <td>exact x;y coords of the end-point
<tr><td> stroke     <td> yes       <td><i>pen</i> to use for drawing the line
</table>

<p>{des3:28} The exact endpoint x;y; coords are specified as an optional helper. In
case the endpoint may matter for visually connecting drawing objects, the
implementation may emit and load the endpoints. Either none of the endpoints
are specified or both endpoints are specified.

<p>{des3:29} Optional: an implementation loading the drawing object may use the
endpoint to detect or even correct rounding errors either in the loader
implementation or in the emitter implementation. If the calculated
endpoint differs from the stored endpoint by more than 1/4 of the tip
size, the implementation shall recalculate other parameters of the arc to
match the endpoints.

<p>{des3:30} If an implementation doesn't implement support for the endpoints,
it must invalidate the endpoint values upon any coordinate change to the arc,
but must preserve the endpoint properties upon other changes or no change.


<p>{des3:31} Stroke may be unspecified if the arc is part of a polygon
outline.

<h3> 3.2.3. Spline </h3>
<p>{des3:32} Not supported in the first version.

<h3> 3.2.4. Text </h3>
<p>{des3:33} The text object has properties and no attributes and no children.

<p>{des3:34} It represents a multiline text block arranged in a bounding box.
When x2 and y2 are specified, pen font height is taken only as a hint:
the renderer may chose a different font and size but has to find the
closest possible match for fitting the unrotated text in the bounding box.
When x2;y2 are not specified, the lower left corner of the rendered text is
placed on x1,y1 and text size is chosen for the pen font_height property.
Rotation is performed after the above bounding box or pen font height rendering.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:35} text properties
<tr><th> property  <th> mandatory <th> description
<tr><td> <i>oid</i><td> yes       <td> &nbsp;
<tr><td> x1, y1    <td> yes       <td> x;y coords of bounding box bottom-left (smaller coords)
<tr><td> x2, y2    <td> no        <td> x;y coords of unrotated bounding box top-right (larger coords)
<tr><td> rot       <td> no        <td> rotation
<tr><td> mirx      <td> no        <td> boolean; mirror X coords (mirror over y axis); default is false if not specified; affects origin vs. bbox relation, does not affect pixels fo rendered text
<tr><td> miry      <td> no        <td> boolean; mirror Y coords (mirror over x axis); default is false if not specified; affects origin vs. bbox relation, does not affect pixels fo rendered text
<tr><td> halign    <td> no        <td> horizontal alignment, per text line: start, center, end, word-justified, justified (default: start); affects only bbox specified text and multi-line text; start means toward the text object's origin
<tr><td> text      <td> yes       <td> string
<tr><td> stroke    <td> yes       <td> <i>pen</i> used to determine color and font properties
<tr><td> dyntext   <td> yes       <td> boolean; when true, perform dyntext substitution
</table>

<p>{des3:79} Rotation is specified in degrees, taken as 0 if the field is not
specified. Zero means horizontal text, positive degree is CCW rotation around
x1, y1.

<p>{des3:36} Horizontal alignment is considered only in two cases. First,
in singleline bbox-specified text that is narrower (horizontally) than the box;
second, in multiline text where
the width of the bounding box matches the width of the widest text line. This
property determines how to handle the narrower lines. Word-justified means
spacing should be applied in between words only; justified means the implementation
is free to use spacing between characters too.

<p>{des3:37} The text field is plain 7 bit ASCII. The implementation may
<b>optionally</b> interpret "&amp;hex;" Unicode sequences - users shall not
expect this to universally work across implementations; portable schematics
shall not use them. Line boundaries indicated by single \n characters.

<p id="des3:75">{des3:75} If dyntext is true, any %pattern% is substituted in text
before rendering. The syntax of pattern is:
<ul>
	<li> a P.<i>key</i> is a property value of the text object
	<li> a ../P.<i>key</i> is a property value of the parent (concrete object) of the text object
	<li> a ../A.<i>key</i> is an attribute value of the parent (concrete object) of the text object
	<li> a ../p.<i>key</i> is a property value of the parent (abstract object) of the text object
	<li> a ../a.<i>key</i> is an attribute value of the parent (abstract object) of the text object
	<li> <i>filename</i> is substituted with the file name of the current sheet without path
	<li> project.<i>field</i> is substituted with an implementation specific project file's named field (e.g. project.name)
</ul>

<p>{des3:76} Multiple ../ can be used to traverse the object tree up; the
root node's parent is itself. Addressing can be done only upward, it is
not possible to address sibling nodes. Non-existing properties or attributes
are substituted with empty string.

<p>{des3:77} A typical example is the "refdes text" which is a text object
within a group role=symbol, with dyntext true, text field containing:
%../a.name%.

<p>{des3:82} In case the addressed attribute is an array: if it has only
one element, it is substituted; else &lt;array&gt; is
printed.

<h3> 3.2.5. Polygon </h3>

<p>{des3:38} A polygon is represented by a complex outline, an optional
fill, and no attributes and no children.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:39} polygon properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td> &nbsp;
<tr><td> outline    <td> yes       <td> list of atoms, see below; these atoms are the children of the polygon in the data tree
<tr><td> stroke     <td> no        <td> <i>pen</i> or unspecified if no stroke is required
<tr><td> fill       <td> no        <td> <i>pen</i> or unspecified if no fill is required
</table>

<p>{des3:40} The outline is an ordered list of lines, arcs and splines that specify the contour. The list is specified in CCW on both drawing object level and
endpoint level. If there's a gap between the endpoints of two
consecutive drawing objects, a straight line is assumed for the missing segment.

<p>{des3:41} To avoid degenerate cases, the contour list has further
restrictions:
<ul>
	<li> must consist at least 3 drawing objects
	<li> must not contain lines or arcs that start and end in the same point
	<li> arcs delta angles must be less than or equal to 180 degrees
	<li> the final contour line, including the assumed missing line segments in gaps, must not intersect or touch itself
</ul>

<p>{des3:42} Individual stroke of each outline object is ignored and the outline is
interpreted as a theoretical, 0 width contour. This theoretical contour
is drawn with the polygon object's stroke (when specified).

<p>{des3:43} If the fill property is specified, the polygon is first filled
with the color of the fill <i>pen</i>. If the <i>stroke</i> property of the polygon is
specified, this <i>pen</i> is used to draw the outline contour of the polygon
(rendered after the fill).

<h3> 3.2.6. Bitmap </h3>
<p>{des3:69} A rectangular, scaled bitmap image.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:70} line properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td> &nbsp;
<tr><td> p1         <td> yes       <td> x;y coords of the top left corner of the bounding box
<tr><td> p2         <td> yes       <td> x;y coords of the bottom right corner of the bounding box
<tr><td> transp     <td> yes       <td> if set, the value of the pixel that should be considered transparent; the value is in the same pixel format as the pnm
<tr><td> pnm        <td> yes       <td> a Portable Anymap in P1, P2, P3, P4, P5 or P6 format
</table>


<h3> 3.2.7. Connection </h3>
<p>{des3:44} The graphical sheet <b>does not imply</b> connections based
on geometrical features, e.g. when the endpoint of a "wire" happens to
be on the endpoint of a "pin". Instead, all connections are created and
stored explicitly by connection objects.

<p>{des3:45} The connection object is invisible and refers
to other objects. It has properties but no attributes and no children.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:46} connection properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td> &nbsp;
<tr><td> conn       <td> yes       <td> list of connected drawing objects (see below)
<tr><td> gfx        <td> yes       <td> oid-path if the drawing object that's the graphics of the connection, or invalid oid
</table>

<p>{des3:47} The connection always connects groups, because only groups
can represent abstract objects. The conn field is a list of <i>oid-path</i>s
of drawing objects (typically atoms) that are directly contributing to the
connection. The groups connected are the direct parent of the objects listed.
Such parent group is always a group or a group_ref with one of the following
roles:
<ul>
	<li> a terminal (role=terminal or bus-terminal)
	<li> a network (role=wire-net or bus-net)
	<li> a hub (role=hub-point)
</ul>

<p>{des3:48} The conn list must list drawing objects so that at least two
different (parent) groups are referred.

<p>{des3:49} If the gfx oid-path is not invalid, it refers to a drawing object
(e.g. an atom or a group) that graphically represents the connection.

<p>{des3:50} The most common use case is marking the graphically overlapping
parts of two or more groups a logical connection. The GUI detects the new
connection from user draw actions and creates (or extends an existing)
connection object. The typical gfx of a connection is a small dot drawn
as a zero length (round cap) line with pen size at least twice of the usual
wire style's.

<p>{des3:51} Since the connection is a logical concept, it can
be realized between two groups that do not have graphically overlapping parts.
However, using connections that way is not intuitive for the user of a
graphical editor, and cschem implementations generally should refrain from
doing that. Overlapping objects (e.g. crossing wirenets) don't necessarily
connect (no connection object is made for disconnected crossings).

<p>{des3:52} Conceptually a connection is similar to a junction within a
wirenet group but it is between two groups (but shall not connect two
wirenets - connected wirenet groups shall be simply merged).

<h2> 3.3. Composite </h2>

<h3> 3.3.1. The group object </h3>
<p>{des3:53} <a id="des3:53">
Group objects consist of properties, attributes and a list
of members (children).

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:54} group properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td> &nbsp;
<tr><td> <i>next_oid</i><td> yes   <td> signed 32 bit integer (see above at common drawing object properties)
<tr><td> memb       <td> yes       <td> list of 1 or more member objects; these drawing objects are the children of the group node
<tr><td> rot        <td> yes       <td> rotation angle around x;y;  can be 0 for no rotation
<tr><td> x, y       <td> yes       <td> x;y offset; can be 0;0 for no action
<tr><td> mirx       <td> no        <td> boolean; mirror X coords (mirror over y axis); default is false if not specified
<tr><td> miry       <td> no        <td> boolean; mirror Y coords (mirror over x axis); default is false if not specified
<tr><td> loclib_name<td> no        <td> arbitrary textual name generated for local lib entries (some groups under the <i>indirect</i> root group); this field is always empty for groups under the <i>direct</i> root group
<tr><td> uuid       <td> no        <td> universally unique ID for referencing the group from external sources
<tr><td> src_uuid   <td> no        <td> if the group was copied from an existing group, universally unique ID of the source group it was copied from
</table>

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:55} group attributes
<tr><th> attribute  <th> value          <th> description
<tr><td> <i>role</i> <th> one of 3.3.3. <th> specify what concrete object the group represents
</table>

<p id="des3:56">{des3:56} The list of members consist of 1 or more atoms or composites. If
the rotation angle is specified, first the coordinates of members are
rotated around the rotation center. If any of the mir* fields are true,
mirroring is performed then.  If the offset is specified, the
coordinates of members are shifted by the offset before the rendering.

<h3> 3.3.2. The group_ref object </h3>

<p>{des3:57} <a id="des3:57">
A group reference is a group instance without locally defined
members, referencing a group that is stored elsewhere. A group_ref object
has properties and attributes but has no children.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:58} group_ref properties
<tr><th> property   <th> mandatory <th> description
<tr><td> <i>oid</i> <td> yes       <td> &nbsp;
<tr><td> ref        <td> yes       <td> reference to an existing group
<tr><td> rot        <td> yes       <td> rotation angle around x;y; can be 0 for no rotation
<tr><td> x, y       <td> yes       <td> x;y offset; can be 0;0 for no action
<tr><td> mirx       <td> no        <td> boolean; mirror X coords (mirror over y axis); default is false if not specified
<tr><td> miry       <td> no        <td> boolean; mirror Y coords (mirror over x axis); default is false if not specified
<tr><td> child_xform<td> no        <td> see <a href="#des3:83">below</a>
</table>

<p>{des3:59} The same attributes are available as for the group object.
The group_ref and referenced group attributes are merged only
<a href="04_attrib.html#des4:31">while compiling the abstract model</a>.

<p>{des3:60} The ref field contains a reference oid-path.

<p id="des3:83">{des3:83} The child_xform field is an unordered list of child
transformations. Each element of the list contains a oid-path of a child
object (relative to the referenced group) and one or more transformation
command.

<p>{des3:84}
Each oid-path can be present only once on the list and each transformation
can be present only once in a given entry.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:85} child_xform transformation commands
<tr><th> name   <th> value type <th> description
<tr><td> movex  <td> coord      <td> child is moved (translated) in the X direction
<tr><td> movey  <td> coord      <td> child is moved (translated) in the Y direction
<tr><td> rot    <td> angle (deg)<td> child is rotated
<tr><td> mirx   <td> boolean    <td> if true, child is mirrored left/right
<tr><td> miry   <td> boolean    <td> if true, child is mirrored up/down
<!--<tr><td> remove <td> boolean    <td> if true, child is removed -->
</table>

<p>{des3:86}
Order of transformations on each child object:
<ul>
	<li> first the group reference's child object is instantiated (copied from the referee)
	<li> then parent group transformations are applied (including the transformations of the group_ref)
	<li> then the child_xform commands are executed (in the same order as <a href="#des3:56">group transformations are applied</a>)
</ul>

<h3> 3.3.3. The role attribute </h3>

<p>{des3:61} A group or group_ref is a drawing object, that can be the
source of a concrete object. Each concrete object has only one source and
each group (or group_ref) can instantiate only one concrete object. (Any
multi-source merging, e.g. multiple symbols donating one element is done
in the concrete -&gt; abstract step.)

<p>{des3:62} If a group (or group_ref) does not define a concrete object,
it is a purely graphical feature that does not affect the concrete model,
the abstract model or any output derived from these models - but still
can affect graphical output.

<p>{des3:63} Whether a group (or group_ref) is a source of a concrete object,
and if so, the actual <a href="02_data.html#des2:70"> type of the concrete object</a> is determined by the
value of the "role" attribute of the group (or group_ref). The value
must be one of those listed in the table below.

<p><table border=1 cellspacing=0>
<tr><th colspan=2 bgcolor="#aaaaaa"> {des3:64} role attribute values
<tr><th> name             <th> documentation
	<tr><td> bus-net        <td> <a href="02_data.html#des2:45"> des2:45 </a>
	<tr><td> bus-terminal   <td> <a href="02_data.html#des2:25"> des2:25 </a>
	<tr><td> hub-point      <td> <a href="02_data.html#des2:57"> des2:57 </a>
	<tr><td> symbol         <td> <a href="02_data.html#des2:18"> des2:18 </a>
	<tr><td> terminal       <td> <a href="02_data.html#des2:25"> des2:24 </a>
	<tr><td> wire-net       <td> <a id="role_wire_net"> <a href="02_data.html#des2:13"> des2:13 </a>
	<tr><td> junction       <td> <a href="02_data.html#des2:81"> des2:81 </a>
</table>

<p>{des3:78} An empty or missing role attribute means the group is a logical
grouping and does not directly create a concrete object. Such logical grouping
can be used by the user or the GUI to group objects together. For example
the user may draw arrows using lines and polygons to represent some logical
relation between areas of a sheet; to be able to easier move/copy/delete an
arrow, the user may use a custom group hosting the arrow.

<p>{des3:65} Note: a junction connecting different groups can be created only by a connection object.

<h2> 3.4. Sheet </h2>

<p id="des3:66">{des3:66} A (graphical schematics) sheet has two child root
groups. The <i>direct</i> group lists
all objects directly visible on the sheet, while the <i>indirect</i> group
serves as a "local library", hosting invisible group objects that
can be referenced from group_ref's from <i>direct</i> or used by plugins (e.g.
devmap).

<p>{des3:87} The first level  under the <i>indirect</i> group is of "library"
groups. Each "library" group has a "purpose"
attribute that indentifies what kind of contents are collected there:
which type of library the group hosts, which plugin(s) can edit it.
For example the local symbol library is stored in the purpose=symlib group
and devmap uses the purpose=devmap group to store locally saved devmaps.
Library groups have no role attribute.

<p>{des3:88} Furthermore a sheet has properties and attributes. The oid of the direct group
is 2 and the oid of the indirect group is 1.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des3:67} sheet properties
<tr><th> property         <th> mandatory <th> description
<tr><td> objects          <td> yes       <td> list of groups and atoms hosted on the sheet; these drawing objects are the children of the sheet
<tr><td> <i>next_oid</i>  <td> yes       <td> signed 32 bit integer (see above at common drawing object properties)
<tr><td> oid              <td> yes       <td> the sheet's own oid (to be used in parent references)
<tr><td> indirect         <td> yes       <td> "local lib" group for invisible objects, oid=1
<tr><td> direct           <td> yes       <td> group for visible objects, oid=2
</table>

<p>{des3:68} sheet uuid is the uuid of the direct group.
