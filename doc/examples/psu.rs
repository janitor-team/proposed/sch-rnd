ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=iNOQfJpO6hT/HFDFGjoAAAGW;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGX;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGY;
				x=44000; y=96000; mirx=1;
				li:objects {
					ha:text.1 { x1=0; y1=-6000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGZ;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGa;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(2,1)
					name=CONN1
					role=symbol
				}
			}
			ha:group.4 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGb;
				x=92000; y=92000;
				li:objects {
					ha:text.1 { x1=0; y1=16000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=16000; }
							ha:line { x1=0; y1=16000; x2=24000; y2=16000; }
							ha:line { x1=24000; y1=16000; x2=24000; y2=0; }
							ha:line { x1=24000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGc;
						x=0; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.pinnum%; floater=1; }
							ha:text.3 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=in
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGd;
						x=24000; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.pinnum%; floater=1; }
							ha:text.3 { x1=2000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=out
							pinnum=3
							role=terminal
						}
					}
					ha:group.5 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGe;
						x=12000; y=0; rot=-90.000000; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.pinnum%; floater=1; }
							ha:text.3 { x1=2000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=gnd
							pinnum=2
							role=terminal
						}
					}
				}
				ha:attrib {
					footprint=TO220
					name=U1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					symbol_generator=boxsym-rnd
				}
			}
			ha:group.8 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGf;
				x=52000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGg;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.10 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGh;
				li:objects {
					ha:line.1 { x1=48000; y1=96000; x2=52000; y2=96000; stroke=wire; }
					ha:line.2 { x1=52000; y1=96000; x2=52000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/10/1
					/2/2/2/1
				}
			}
			ha:connection.12 {
				li:conn {
					/2/10/2
					/2/8/1/1
				}
			}
			ha:group.16 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGi;
				x=80000; y=96000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGj;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGk;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=20000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=16000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=1206
					name=C2
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=100n
				}
			}
			ha:group.17 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGl;
				x=64000; y=96000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGm;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGn;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:text.3 { x1=20000; y1=-10000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=16000; y1=-6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
					ha:arc.8 { cx=34000; cy=0; r=23000; sang=167.500000; dang=25.000000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=-3000; x2=8000; y2=-3000; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-4000; x2=7000; y2=-2000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=rcy(300)
					name=C1
					li:portmap {
						{N->pcb/pinnum=1}
						{P->pcb/pinnum=2}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=10u
				}
			}
			ha:group.19 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGo;
				x=156000; y=96000;
				li:objects {
					ha:text.1 { x1=0; y1=-6000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGp;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGq;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(2,1)
					name=CONN2
					role=symbol
				}
			}
			ha:group.20 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGr;
				x=148000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGs;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.21 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGt;
				li:objects {
					ha:line.1 { x1=152000; y1=96000; x2=148000; y2=96000; stroke=wire; }
					ha:line.2 { x1=148000; y1=96000; x2=148000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.22 {
				li:conn {
					/2/21/1
					/2/19/2/1
				}
			}
			ha:connection.23 {
				li:conn {
					/2/21/2
					/2/20/1/1
				}
			}
			ha:group.24 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGu;
				li:objects {
					ha:line.1 { x1=48000; y1=100000; x2=88000; y2=100000; stroke=wire; }
					ha:line.2 { x1=64000; y1=96000; x2=64000; y2=100000; stroke=wire; }
					ha:line.3 { x1=64000; y1=100000; x2=64000; y2=100000; stroke=junction; }
					ha:line.4 { x1=80000; y1=96000; x2=80000; y2=100000; stroke=wire; }
					ha:line.5 { x1=80000; y1=100000; x2=80000; y2=100000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.25 {
				li:conn {
					/2/24/1
					/2/2/3/1
				}
			}
			ha:connection.26 {
				li:conn {
					/2/24/1
					/2/4/3/1
				}
			}
			ha:group.27 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGv;
				li:objects {
					ha:line.1 { x1=120000; y1=100000; x2=152000; y2=100000; stroke=wire; }
					ha:line.4 { x1=128000; y1=96000; x2=128000; y2=100000; stroke=wire; }
					ha:line.5 { x1=128000; y1=100000; x2=128000; y2=100000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.28 {
				li:conn {
					/2/27/1
					/2/4/4/1
				}
			}
			ha:connection.29 {
				li:conn {
					/2/27/1
					/2/19/3/1
				}
			}
			ha:connection.30 {
				li:conn {
					/2/24/2
					/2/17/2/1
				}
			}
			ha:connection.31 {
				li:conn {
					/2/24/4
					/2/16/2/1
				}
			}
			ha:group.32 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGw;
				x=128000; y=96000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGx;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGy;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=20000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=16000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=1206
					name=C3
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=100n
				}
			}
			ha:group.34 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGz;
				x=128000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAG0;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.35 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG1;
				x=80000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAG2;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.36 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG3;
				x=64000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAG4;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.37 {
				li:conn {
					/2/27/4
					/2/32/2/1
				}
			}
			ha:group.38 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG5;
				li:objects {
					ha:line.1 { x1=128000; y1=76000; x2=128000; y2=64000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.39 {
				li:conn {
					/2/38/1
					/2/34/1/1
				}
			}
			ha:connection.40 {
				li:conn {
					/2/38/1
					/2/32/1/1
				}
			}
			ha:group.41 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG6;
				li:objects {
					ha:line.1 { x1=80000; y1=76000; x2=80000; y2=64000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.42 {
				li:conn {
					/2/41/1
					/2/16/1/1
				}
			}
			ha:connection.43 {
				li:conn {
					/2/41/1
					/2/35/1/1
				}
			}
			ha:group.44 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG7;
				li:objects {
					ha:line.1 { x1=64000; y1=76000; x2=64000; y2=64000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.45 {
				li:conn {
					/2/44/1
					/2/17/1/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/44/1
					/2/36/1/1
				}
			}
			ha:group.47 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG8;
				x=104000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAG9;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.48 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAG+;
				li:objects {
					ha:line.1 { x1=104000; y1=88000; x2=104000; y2=64000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.49 {
				li:conn {
					/2/48/1
					/2/4/5/1
				}
			}
			ha:connection.50 {
				li:conn {
					/2/48/1
					/2/47/1/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={simple PSU board: LDO breakout (example)}
		}
	}
}
