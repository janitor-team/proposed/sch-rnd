ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=iNOQfJpO6hT/HFDFGjoAAAFn;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFo;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFp;
				x=104000; y=112000;
				li:objects {
					ha:text.1 { x1=-8000; y1=28000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=16000; }
							ha:line { x1=0; y1=16000; x2=16000; y2=16000; }
							ha:line { x1=16000; y1=16000; x2=16000; y2=0; }
							ha:line { x1=16000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFq;
						x=0; y=12000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=A
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFr;
						x=0; y=4000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=B
							pinnum=2
							role=terminal
						}
					}
					ha:group.5 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFs;
						x=16000; y=8000; mirx=1;
						li:objects {
							ha:arc.1 { cx=-500; cy=0; r=500; sang=0.000000; dang=360.000000; stroke=term-decor; }
							ha:polygon.2 {
								li:outline {
									ha:line { x1=0; y1=0; x2=1000; y2=1000; }
									ha:line { x1=1000; y1=1000; x2=1000; y2=-1000; }
									ha:line { x1=1000; y1=-1000; x2=0; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.3 { x1=-1000; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.4 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.5 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=Z
							pinnum=3
							role=terminal
						}
					}
					ha:text.6 { x1=-8000; y1=24000; dyntext=1; stroke=sym-secondary; text={slot=%../A.-slot%}; floater=1; }
					ha:text.7 { x1=-8000; y1=20000; dyntext=1; stroke=sym-secondary; text=%../A.device%; floater=1; }
				}
				ha:attrib {
					device=7400
					name=U1
					li:portmap {
						{1/A -> pcb/pinnum=1}
						{2/A -> pcb/pinnum=4}
						{3/A -> pcb/pinnum=9}
						{4/A -> pcb/pinnum=12}
						{1/B -> pcb/pinnum=2}
						{2/B -> pcb/pinnum=5}
						{3/B -> pcb/pinnum=10}
						{4/B -> pcb/pinnum=13}
						{1/Z -> pcb/pinnum=3}
						{2/Z -> pcb/pinnum=6}
						{3/Z -> pcb/pinnum=8}
						{4/Z -> pcb/pinnum=11}
					}
					role=symbol
					-slot=1
					symbol_generator=boxsym-rnd
				}
			}
			ha:group.3 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFt;
				x=136000; y=160000;
				li:objects {
					ha:text.1 { x1=9000; y1=10000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=24000; }
							ha:line { x1=0; y1=24000; x2=8000; y2=24000; }
							ha:line { x1=8000; y1=24000; x2=8000; y2=0; }
							ha:line { x1=8000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFu;
						x=4000; y=24000; rot=90.000000; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.3 { x1=0; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=Vcc
							pinnum=14
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFv;
						x=4000; y=0; rot=-90.000000; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.3 { x1=0; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=gnd
							pinnum=7
							role=terminal
						}
					}
				}
				ha:attrib {
					name=U1
					role=symbol
					symbol_generator=boxsym-rnd
				}
			}
			ha:group.4 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFw;
				x=104000; y=64000;
				li:objects {
					ha:text.1 { x1=-8000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=16000; }
							ha:line { x1=0; y1=16000; x2=16000; y2=16000; }
							ha:line { x1=16000; y1=16000; x2=16000; y2=0; }
							ha:line { x1=16000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFx;
						x=0; y=12000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=A
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFy;
						x=0; y=4000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=B
							pinnum=2
							role=terminal
						}
					}
					ha:group.5 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFz;
						x=16000; y=8000; mirx=1;
						li:objects {
							ha:arc.1 { cx=-500; cy=0; r=500; sang=0.000000; dang=360.000000; stroke=term-decor; }
							ha:polygon.2 {
								li:outline {
									ha:line { x1=0; y1=0; x2=1000; y2=1000; }
									ha:line { x1=1000; y1=1000; x2=1000; y2=-1000; }
									ha:line { x1=1000; y1=-1000; x2=0; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.3 { x1=-1000; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.4 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.5 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=Z
							pinnum=3
							role=terminal
						}
					}
					ha:text.6 { x1=-8000; y1=-8000; dyntext=1; stroke=sym-secondary; text={slot=%../A.-slot%}; floater=1; }
					ha:text.7 { x1=-8000; y1=-12000; dyntext=1; stroke=sym-secondary; text=%../A.device%; floater=1; }
				}
				ha:attrib {
					device=7400
					name=U1
					role=symbol
					-slot=2
					symbol_generator=boxsym-rnd
				}
			}
			ha:group.5 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAF0;
				x=160000; y=108000;
				li:objects {
					ha:text.1 { x1=-8000; y1=28000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=16000; }
							ha:line { x1=0; y1=16000; x2=16000; y2=16000; }
							ha:line { x1=16000; y1=16000; x2=16000; y2=0; }
							ha:line { x1=16000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAF1;
						x=0; y=12000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=A
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAF2;
						x=0; y=4000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=B
							pinnum=2
							role=terminal
						}
					}
					ha:group.5 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAF3;
						x=16000; y=8000; mirx=1;
						li:objects {
							ha:arc.1 { cx=-500; cy=0; r=500; sang=0.000000; dang=360.000000; stroke=term-decor; }
							ha:polygon.2 {
								li:outline {
									ha:line { x1=0; y1=0; x2=1000; y2=1000; }
									ha:line { x1=1000; y1=1000; x2=1000; y2=-1000; }
									ha:line { x1=1000; y1=-1000; x2=0; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.3 { x1=-1000; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.4 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.5 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=Z
							pinnum=3
							role=terminal
						}
					}
					ha:text.6 { x1=-8000; y1=24000; dyntext=1; stroke=sym-secondary; text={slot=%../A.-slot%}; floater=1; }
					ha:text.7 { x1=-8000; y1=20000; dyntext=1; stroke=sym-secondary; text=%../A.device%; floater=1; }
				}
				ha:attrib {
					device=7400
					name=U1
					role=symbol
					-slot=3
					symbol_generator=boxsym-rnd
				}
			}
			ha:group.6 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAF4;
				x=160000; y=68000;
				li:objects {
					ha:text.1 { x1=-8000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=16000; }
							ha:line { x1=0; y1=16000; x2=16000; y2=16000; }
							ha:line { x1=16000; y1=16000; x2=16000; y2=0; }
							ha:line { x1=16000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAF5;
						x=0; y=12000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=A
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAF6;
						x=0; y=4000;
						li:objects {
							ha:polygon.1 {
								li:outline {
									ha:line { x1=1000; y1=0; x2=0; y2=1000; }
									ha:line { x1=0; y1=1000; x2=0; y2=-1000; }
									ha:line { x1=0; y1=-1000; x2=1000; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.2 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.3 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.4 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=B
							pinnum=2
							role=terminal
						}
					}
					ha:group.5 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAF7;
						x=16000; y=8000; mirx=1;
						li:objects {
							ha:arc.1 { cx=-500; cy=0; r=500; sang=0.000000; dang=360.000000; stroke=term-decor; }
							ha:polygon.2 {
								li:outline {
									ha:line { x1=0; y1=0; x2=1000; y2=1000; }
									ha:line { x1=1000; y1=1000; x2=1000; y2=-1000; }
									ha:line { x1=1000; y1=-1000; x2=0; y2=0; }
								}
								stroke=sym-decor;
							}
							ha:line.3 { x1=-1000; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.4 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
							ha:text.5 { x1=1000; y1=-2000; dyntext=1; stroke=term-secondary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=Z
							pinnum=3
							role=terminal
						}
					}
					ha:text.6 { x1=-8000; y1=-8000; dyntext=1; stroke=sym-secondary; text={slot=%../A.-slot%}; floater=1; }
					ha:text.7 { x1=-8000; y1=-12000; dyntext=1; stroke=sym-secondary; text=%../A.device%; floater=1; }
				}
				ha:attrib {
					device=7400
					name=U1
					role=symbol
					-slot=4
					symbol_generator=boxsym-rnd
				}
			}
			ha:group.7 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAF8;
				li:objects {
					ha:line.1 { x1=124000; y1=72000; x2=156000; y2=72000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.8 {
				li:conn {
					/2/7/1
					/2/6/4/2
				}
			}
			ha:connection.9 {
				li:conn {
					/2/7/1
					/2/4/5/3
				}
			}
			ha:group.10 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAF9;
				li:objects {
					ha:line.1 { x1=124000; y1=120000; x2=156000; y2=120000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/10/1
					/2/2/5/3
				}
			}
			ha:connection.12 {
				li:conn {
					/2/10/1
					/2/5/3/2
				}
			}
			ha:group.13 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAF+;
				li:objects {
					ha:line.1 { x1=100000; y1=116000; x2=88000; y2=116000; stroke=wire; }
					ha:line.3 { x1=88000; y1=76000; x2=88000; y2=116000; stroke=wire; }
					ha:line.4 { x1=88000; y1=76000; x2=100000; y2=76000; stroke=wire; }
					ha:line.5 { x1=60000; y1=96000; x2=88000; y2=96000; stroke=wire; }
					ha:line.6 { x1=88000; y1=96000; x2=88000; y2=96000; stroke=junction; }
					ha:text.7 { x1=80000; y1=96000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=clk
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.14 {
				li:conn {
					/2/13/1
					/2/2/4/2
				}
			}
			ha:connection.15 {
				li:conn {
					/2/13/4
					/2/4/3/2
				}
			}
			ha:group.20 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAF/;
				li:objects {
					ha:line.2 { x1=192000; y1=76000; x2=192000; y2=92000; stroke=wire; }
					ha:line.3 { x1=192000; y1=92000; x2=144000; y2=100000; stroke=wire; }
					ha:line.4 { x1=156000; y1=112000; x2=144000; y2=112000; stroke=wire; }
					ha:line.5 { x1=144000; y1=100000; x2=144000; y2=112000; stroke=wire; }
					ha:line.6 { x1=180000; y1=76000; x2=216000; y2=76000; stroke=wire; }
					ha:line.7 { x1=192000; y1=76000; x2=192000; y2=76000; stroke=junction; }
					ha:line.8 { x1=216000; y1=76000; x2=216000; y2=96000; stroke=wire; }
					ha:line.9 { x1=216000; y1=96000; x2=220000; y2=96000; stroke=wire; }
					ha:text.10 { x1=204000; y1=77000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=Q'
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.21 {
				li:conn {
					/2/6/5/3
					/2/20/6
				}
			}
			ha:group.22 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGA;
				li:objects {
					ha:line.4 { x1=192000; y1=100000; x2=192000; y2=116000; stroke=wire; }
					ha:line.5 { x1=192000; y1=100000; x2=144000; y2=92000; stroke=wire; }
					ha:line.6 { x1=156000; y1=80000; x2=144000; y2=80000; stroke=wire; }
					ha:line.7 { x1=144000; y1=80000; x2=144000; y2=92000; stroke=wire; }
					ha:line.9 { x1=192000; y1=116000; x2=192000; y2=116000; stroke=junction; }
					ha:line.10 { x1=180000; y1=116000; x2=216000; y2=116000; stroke=wire; }
					ha:line.11 { x1=216000; y1=116000; x2=216000; y2=100000; stroke=wire; }
					ha:line.12 { x1=216000; y1=100000; x2=220000; y2=100000; stroke=wire; }
					ha:text.13 { x1=204000; y1=117000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=Q
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.23 {
				li:conn {
					/2/5/5/3
					/2/22/10
				}
			}
			ha:connection.24 {
				li:conn {
					/2/22/6
					/2/6/3/2
				}
			}
			ha:connection.25 {
				li:conn {
					/2/20/4
					/2/5/4/2
				}
			}
			ha:group.28 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGB;
				li:objects {
					ha:line.1 { x1=100000; y1=68000; x2=68000; y2=68000; stroke=wire; }
					ha:line.2 { x1=68000; y1=68000; x2=68000; y2=92000; stroke=wire; }
					ha:line.3 { x1=68000; y1=92000; x2=60000; y2=92000; stroke=wire; }
					ha:text.4 { x1=80000; y1=68000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=R
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.29 {
				li:conn {
					/2/28/1
					/2/4/4/2
				}
			}
			ha:group.30 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGC;
				x=140000; y=148000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGD;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.31 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGE;
				x=140000; y=192000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGF;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					li:connect {
						{1:Vcc}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.38 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGG;
				x=120000; y=168000; mirx=1;
				li:objects {
					ha:text.1 { x1=6000; y1=12000; rot=270.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGH;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGI;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN3
					role=symbol
				}
			}
			ha:group.39 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGJ;
				x=224000; y=96000;
				li:objects {
					ha:text.1 { x1=10000; y1=-8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGL;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN2
					role=symbol
				}
			}
			ha:group.40 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGM;
				x=56000; y=92000; mirx=1;
				li:objects {
					ha:text.1 { x1=6000; y1=12000; rot=270.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGN;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGO;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGP;
						x=0; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN1
					role=symbol
				}
			}
			ha:connection.41 {
				li:conn {
					/2/20/9
					/2/39/2/1
				}
			}
			ha:connection.42 {
				li:conn {
					/2/39/3/1
					/2/22/12
				}
			}
			ha:connection.43 {
				li:conn {
					/2/28/3
					/2/40/2/1
				}
			}
			ha:group.44 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGQ;
				li:objects {
					ha:line.1 { x1=60000; y1=100000; x2=68000; y2=100000; stroke=wire; }
					ha:line.2 { x1=100000; y1=124000; x2=68000; y2=124000; stroke=wire; }
					ha:line.3 { x1=68000; y1=100000; x2=68000; y2=124000; stroke=wire; }
					ha:text.4 { x1=80000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=S
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.45 {
				li:conn {
					/2/44/1
					/2/40/4/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/44/2
					/2/2/3/2
				}
			}
			ha:connection.47 {
				li:conn {
					/2/13/5
					/2/40/3/1
				}
			}
			ha:group.72 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGR;
				x=164000; y=180000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGS;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAGT;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					name=C1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=100n
				}
			}
			ha:group.77 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGU;
				li:objects {
					ha:line.1 { x1=129000; y1=153000; x2=140000; y2=153000; stroke=wire; }
					ha:line.2 { x1=140000; y1=153000; x2=140000; y2=154000; stroke=wire; }
					ha:line.3 { x1=140000; y1=148000; x2=140000; y2=156000; stroke=wire; }
					ha:line.4 { x1=140000; y1=153000; x2=164000; y2=153000; stroke=wire; }
					ha:line.5 { x1=129000; y1=168000; x2=129000; y2=153000; stroke=wire; }
					ha:line.6 { x1=164000; y1=153000; x2=164000; y2=160000; stroke=wire; }
					ha:line.7 { x1=124000; y1=168000; x2=129000; y2=168000; stroke=wire; }
					ha:line.8 { x1=140000; y1=153000; x2=140000; y2=153000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.78 {
				li:conn {
					/2/77/3
					/2/30/1/1
				}
			}
			ha:connection.79 {
				li:conn {
					/2/77/3
					/2/3/4/1
				}
			}
			ha:connection.80 {
				li:conn {
					/2/77/6
					/2/72/1/1
				}
			}
			ha:connection.81 {
				li:conn {
					/2/77/7
					/2/38/2/1
				}
			}
			ha:group.83 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAGV;
				li:objects {
					ha:line.1 { x1=140000; y1=189000; x2=140000; y2=189000; stroke=junction; }
					ha:line.2 { x1=129000; y1=172000; x2=129000; y2=189000; stroke=wire; }
					ha:line.3 { x1=140000; y1=188000; x2=140000; y2=192000; stroke=wire; }
					ha:line.4 { x1=124000; y1=172000; x2=129000; y2=172000; stroke=wire; }
					ha:line.5 { x1=129000; y1=189000; x2=164000; y2=189000; stroke=wire; }
					ha:line.6 { x1=164000; y1=189000; x2=164000; y2=180000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.84 {
				li:conn {
					/2/83/3
					/2/3/3/1
				}
			}
			ha:connection.85 {
				li:conn {
					/2/83/3
					/2/31/1/1
				}
			}
			ha:connection.86 {
				li:conn {
					/2/83/4
					/2/38/3/1
				}
			}
			ha:connection.88 {
				li:conn {
					/2/83/6
					/2/72/2/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title=inhomogeneous slot example
		}
	}
}
