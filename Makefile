all:
	cd src && $(MAKE) all
	cd util && $(MAKE) all
	cd tests && $(MAKE) all && cd ..

dep:
	cd src && $(MAKE) dep

clean:
	cd src && $(MAKE) clean
	cd src_3rd && $(MAKE) clean
	cd util && $(MAKE) clean
	cd tests && $(MAKE) clean && cd ..

distclean:
	cd src && $(MAKE) distclean
	cd util && $(MAKE) distclean
	cd scconfig && $(MAKE) distclean
	cd tests && $(MAKE) distclean && cd ..

install:
	cd src && $(MAKE) install
	cd doc && $(MAKE) install
	cd library && $(MAKE) install
	cd util && $(MAKE) install
	cd src_3rd && $(MAKE) install
	cd font && $(MAKE) install

linstall:
	cd src && $(MAKE) linstall
	cd doc && $(MAKE) linstall
	cd library && $(MAKE) linstall
	cd util && $(MAKE) linstall
	cd src_3rd && $(MAKE) install
	cd font && $(MAKE) install

uninstall:
	cd src && $(MAKE) uninstall
	cd doc && $(MAKE) uninstall
	cd library && $(MAKE) uninstall
	cd util && $(MAKE) uninstall
	cd src_3rd && $(MAKE) install
	cd font && $(MAKE) install

test:
	cd tests && $(MAKE) test && cd ..
