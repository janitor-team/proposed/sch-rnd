csch_sheet_t *sch_rnd_sheet_new(csch_project_t *prj); /* doesn't emit postproc event */
csch_sheet_t *sch_rnd_sheet_new4revert(csch_sheet_t *sheet);
void sch_rnd_sheet_postproc(csch_sheet_t *sheet);

rnd_coord_t sch_rnd_sheet_attr_crd(csch_sheet_t *sheet, const char *key, rnd_coord_t defval);

