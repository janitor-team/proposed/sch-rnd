#include <genht/htsp.h>

/* Make s the current sheet (in the GUI). Returns previous current sheet. */
csch_sheet_t *sch_rnd_multi_switch_to(csch_sheet_t *s);

/* Change current sheet to curr+step in the linear sheet list;
   if curr is NULL, use the GUI-current sheet */
void sch_rnd_multi_switch_to_delta(csch_sheet_t *curr, int step);

/* Special case: first switch after loading all pages from the command line */
void sch_rnd_multi_switch_to_initial(csch_sheet_t *s);


csch_sheet_t *sch_rnd_multi_load(const char *fn, const char *fmt, int *is_project);
csch_sheet_t *sch_rnd_multi_new_empty(const char *fn);

/* Assuming s is not the current sheet, unload it, removing from the list of
   sheets. If s is NULL, unload currently active sheet. */
void sch_rnd_multi_unload(csch_sheet_t *s);

void sch_rnd_multi_init(void);
void sch_rnd_multi_uninit(void);
