#ifndef SCH_RND_SEARCH_H
#define SCH_RND_SEARCH_H

#include <libcschem/config.h>
#include <librnd/hid/hid.h>
#include <librnd/hid/grid.h>
#include <libcschem/concrete.h>


#define SCH_RND_SLOP_ 5
#define sch_rnd_slop (SCH_RND_SLOP_ * P2C(rnd_pixel_slop))

/* Return the preferred visible object at x;y or NULL if nothing found;
   the unsel version ignores selected objects */
csch_chdr_t *sch_rnd_search_obj_at(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y, csch_coord_t radius);
csch_chdr_t *sch_rnd_search_obj_at_unsel(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y, csch_coord_t radius);


/* Return the preferred visible object at x;y for inspection (e.g. right click
   popup) or NULL if nothing found */
csch_chdr_t *sch_rnd_search_first_gui_inspect(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y);


/* Append all visible objects that has a bbox intersecting with box to res */
void sch_rnd_search_obj_box(csch_sheet_t *sheet, vtp0_t *res, csch_rtree_box_t *box);



#endif
