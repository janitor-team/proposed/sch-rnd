/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2019,2020,2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/hid/hid.h>
#include <librnd/hid/hid_inlines.h>
#include <librnd/core/color.h>
#include <librnd/core/vtc0.h>
#include <librnd/core/compat_misc.h>

#include <libcschem/concrete.h>
#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_line.h>
#include <libcschem/cnc_conn.h>
#include <libcschem/cnc_poly.h>
#include <libcschem/cnc_text.h>
#include <libcschem/cnc_arc.h>
#include <libcschem/cnc_pen.h>
#include <libcschem/util_wirenet.h>
#include <libcschem/event.h>

#include <gengeo2d/xform.h>

#include "conf_core.h"
#include "font.h"
#include "draw.h"
#include "search.h"

static const char draw_cookie[] = "sch-rnd/draw.c";

typedef struct {
	csch_sheet_t *sheet;
	rnd_hid_gc_t gc;
	vtp0_t text_update;
	rnd_xform_t *xform;
} draw_ctx_t;

TODO("code dup with pcb-rnd")
static void pcb_draw_dashed_line(rnd_hid_gc_t GC, rnd_coord_t x1, rnd_coord_t y1, rnd_coord_t x2, rnd_coord_t y2, unsigned int segs, rnd_bool_t cheap)
{
/* TODO: we need a real geo lib... using double here is plain wrong */
	double dx = x2-x1, dy = y2-y1;
	double len_mnt = RND_ABS(dx) + RND_ABS(dy);
	int n;
	rnd_coord_t minlen = rnd_render->coord_per_pix * 8;

	/* Ignore info->xform->bloat because a dashed line is always thin */

	if (len_mnt < minlen*2) {
		/* line too short, just draw it */
		rnd_render->draw_line(GC, x1, y1, x2, y2);
		return;
	}

	segs = (segs << 1) + 1; /* must be odd */

	if (cheap) {
		if ((segs > 3) && (len_mnt < minlen * segs))
			segs = 3;
		else if ((segs > 5) && (len_mnt < minlen * 2 * segs))
			segs = 5;
	}

	/* first seg is drawn from x1, y1 with no rounding error due to n-1 == 0 */
	for(n = 1; n < segs; n+=2)
		rnd_render->draw_line(GC,
			x1 + (dx * (double)(n-1) / (double)segs), y1 + (dy * (double)(n-1) / (double)segs),
			x1 + (dx * (double)n / (double)segs), y1 + (dy * (double)n / (double)segs));


	/* make sure the last segment is drawn properly to x2 and y2, don't leave
	   room for rounding errors */
	rnd_render->draw_line(GC,
		x2 - (dx / (double)segs), y2 - (dy / (double)segs),
		x2, y2);
}

csch_inline rnd_cap_style_t pen_cap(csch_cpen_t *pen)
{
	switch(pen->shape) {
		case CSCH_PSHP_ROUND: return rnd_cap_round;
		case CSCH_PSHP_SQUARE: return rnd_cap_square;
	}

	/* fallback for invalid */
	return rnd_cap_round;
}

static void lighten_color(const rnd_color_t *orig, rnd_color_t *dst, double factor)
{
	rnd_color_load_int(dst, MIN(255, orig->r * factor), MIN(255, orig->g * factor), MIN(255, orig->b * factor), 255);
}


csch_inline const rnd_color_t *sch_rnd_stroke_color(draw_ctx_t *ctx, const csch_chdr_t *obj, const csch_cpen_t *stroke, int selected)
{
	const rnd_color_t *res;
	int no_render_select = (ctx->xform != NULL) && (ctx->xform->no_render_select);
	int no_render_hilight = (ctx->xform != NULL) && (ctx->xform->no_render_hilight);

	if (obj->hilight && !no_render_hilight)
		return &conf_core.appearance.color.hilight_stroke;

	if (no_render_select)
		selected = 0;

	res = selected ? &conf_core.appearance.color.selected_stroke : &stroke->color;
	if ((ctx->xform != NULL) && (ctx->xform->faded)) {
		static rnd_color_t tmp;
		lighten_color(res, &tmp, 0.5);
		res = &tmp;
	}
	return res;
}

csch_inline const rnd_color_t *sch_rnd_fill_color(draw_ctx_t *ctx, const csch_chdr_t *obj, const csch_cpen_t *fill, int selected)
{
	const rnd_color_t *res;
	if (obj->hilight)
		return &conf_core.appearance.color.hilight_stroke;
	res = selected ? &conf_core.appearance.color.selected_fill : &fill->color;
	if ((ctx->xform != NULL) && (ctx->xform->faded)) {
		static rnd_color_t tmp;
		lighten_color(res, &tmp, 0.5);
		res = &tmp;
	}
	return res;
}

csch_inline void draw_layer_line(draw_ctx_t *ctx, csch_line_t *l, csch_cpen_t *pen, int selected)
{
	rnd_render->set_color(ctx->gc, sch_rnd_stroke_color(ctx, &l->hdr, pen, selected));
	rnd_hid_set_line_cap(ctx->gc, pen_cap(pen));
	rnd_hid_set_line_width(ctx->gc, C2P(pen->size));
	rnd_render->draw_line(ctx->gc, C2P(l->inst.c.p1.x), C2P(l->inst.c.p1.y), C2P(l->inst.c.p2.x), C2P(l->inst.c.p2.y));
}

csch_inline void draw_layer_conn(draw_ctx_t *ctx, csch_conn_t *c)
{
	long n, *crd;

	if (c->coords.used == 0)
		return;

	rnd_render->set_color(ctx->gc, &conf_core.appearance.color.connection_good);
	rnd_hid_set_line_cap(ctx->gc, rnd_cap_round);
	rnd_hid_set_line_width(ctx->gc, -2);

	for(n = 0, crd = c->coords.array; n < c->coords.used; n += 4, crd += 4) {
		long slop = sch_rnd_slop;
		long dx = crd[0] - crd[2], dy = crd[1] - crd[3];
		dx = RND_ABS(dx); dy = RND_ABS(dy);
	
		if (dx + dy <= slop)
			rnd_render->draw_arc(ctx->gc, C2P((crd[0] + crd[2])/2), C2P((crd[1] + crd[3])/2), C2P(256), C2P(256), 0, 360);
		else
			rnd_render->draw_line(ctx->gc, C2P(crd[0]), C2P(crd[1]), C2P(crd[2]), C2P(crd[3]));
	}
}

csch_inline void draw_vectors(draw_ctx_t *ctx, g2d_vect_t base, g2d_vect_t origin, g2d_xform_t mx)
{
		g2d_vect_t u[6];
		int n;

		u[0] = g2d_vect(1000, 0);
		u[1] = g2d_vect(900, +100);
		u[2] = g2d_vect(900, -100);
		u[3] = g2d_vect(0, 1000);
		u[4] = g2d_vect(+100, 900);
		u[5] = g2d_vect(-100, 900);
		for(n = 0; n < 6; n++) {
			u[n].x += base.x;
			u[n].y += base.y;
			u[n] = g2d_xform_vect2vect(mx, u[n]);
		}
		rnd_render->draw_line(ctx->gc, C2P(origin.x), C2P(origin.y), C2P(u[0].x), C2P(u[0].y));
		rnd_render->draw_line(ctx->gc, C2P(origin.x), C2P(origin.y), C2P(u[3].x), C2P(u[3].y));
		rnd_render->draw_line(ctx->gc, C2P(u[0].x), C2P(u[0].y), C2P(u[1].x), C2P(u[1].y));
		rnd_render->draw_line(ctx->gc, C2P(u[0].x), C2P(u[0].y), C2P(u[2].x), C2P(u[2].y));
		rnd_render->draw_line(ctx->gc, C2P(u[3].x), C2P(u[3].y), C2P(u[4].x), C2P(u[4].y));
		rnd_render->draw_line(ctx->gc, C2P(u[3].x), C2P(u[3].y), C2P(u[5].x), C2P(u[5].y));
}

csch_inline void print_mx(const char *name, g2d_xform_t mx)
{
	int n;
	rnd_trace("MX %s\n", name);
	for(n = 0; n < 9; n++) {
		rnd_trace(" %.2f", mx.v[n]);
		if ((n % 3) == 2)
			rnd_trace("\n");
	}
	rnd_trace("\n");
}

csch_inline void draw_layer_text(draw_ctx_t *ctx, csch_text_t *t, csch_cpen_t *pen, int selected)
{
	int use_local = (ctx->xform != NULL) && ctx->xform->use_local_vis;
	int vis_meta = use_local ? ctx->xform->local_vis[CSCH_DSPLY_TEXT_META] : csch_layer_vis[CSCH_DSPLY_TEXT_META];

	if (vis_meta) {
		rnd_render->set_color(ctx->gc, sch_rnd_stroke_color(ctx, &t->hdr, pen, selected));
		rnd_hid_set_line_cap(ctx->gc, pen_cap(pen));
		rnd_hid_set_line_width(ctx->gc, -1);

		rnd_render->draw_line(ctx->gc, C2P(t->inst1.x), C2P(t->inst1.y), C2P(t->inst15.x), C2P(t->inst15.y));
		rnd_render->draw_line(ctx->gc, C2P(t->inst1.x), C2P(t->inst1.y), C2P(t->inst25.x), C2P(t->inst25.y));
		rnd_render->draw_line(ctx->gc, C2P(t->inst2.x), C2P(t->inst2.y), C2P(t->inst15.x), C2P(t->inst15.y));
		rnd_render->draw_line(ctx->gc, C2P(t->inst2.x), C2P(t->inst2.y), C2P(t->inst25.x), C2P(t->inst25.y));
	}
	if (conf_core.rc.debug.draw_text_xform) {
		rnd_render->set_color(ctx->gc, sch_rnd_stroke_color(ctx, &t->hdr, pen, selected));
		rnd_hid_set_line_cap(ctx->gc, pen_cap(pen));
		rnd_hid_set_line_width(ctx->gc, -1);
		draw_vectors(ctx, t->spec1, t->inst1, t->hdr.parent->xform.mx);

#if 0
		{
		g2d_xform_t imx;
		print_mx("orig:", t->hdr.parent->xform.mx);
		rnd_render->set_color(ctx->gc, rnd_color_red);
		csch_cgrp_inverse_matrix(&imx, t->hdr.parent);
		imx.v[2] = imx.v[2] < 0 ? -1000 : 1000;
		imx.v[5] = imx.v[5] < 0 ? -1000 : 1000;
		draw_vectors(ctx, t->inst1, t->inst1, imx);
		print_mx("inv:", imx);
		}
#endif
	}


	rnd_render->set_color(ctx->gc, sch_rnd_stroke_color(ctx, &t->hdr, pen, selected));
	sch_rnd_font_text_render(ctx->sheet, ctx->gc, t, pen);

	if (vis_meta) {
		double vx, vy, len;

		rnd_render->set_color(ctx->gc, rnd_color_red);
		rnd_hid_set_line_cap(ctx->gc, pen_cap(pen));
		rnd_hid_set_line_width(ctx->gc, -3);

		vx = t->inst15.x - t->inst1.x;
		vy = t->inst15.y - t->inst1.y;
		len = sqrt(vx*vx+vy*vy);
		if (len > 0) {
			vx /= len; vy /= len;
			rnd_render->draw_line(ctx->gc, C2P(t->inst1.x), C2P(t->inst1.y), C2P((csch_coord_t)(t->inst1.x + vx*3000)), C2P((csch_coord_t)(t->inst1.y + vy*3000)));
		}

		rnd_render->set_color(ctx->gc, rnd_color_magenta);
		vx = t->inst25.x - t->inst1.x;
		vy = t->inst25.y - t->inst1.y;
		len = sqrt(vx*vx+vy*vy);
		if (len > 0) {
			vx /= len; vy /= len;
			rnd_render->draw_line(ctx->gc, C2P(t->inst1.x), C2P(t->inst1.y), C2P((csch_coord_t)(t->inst1.x + vx*3000)), C2P((csch_coord_t)(t->inst1.y + vy*3000)));
		}
	}

}

vtc0_t draw_poly_x, draw_poly_y;

static void draw_poly_reset(void)
{
	draw_poly_x.used = draw_poly_y.used = 0;
}

static void draw_poly_append(rnd_coord_t x, rnd_coord_t y)
{
	/* do not add duplicate points */
	if (draw_poly_x.used > 0) {
		if ((draw_poly_x.array[draw_poly_x.used-1] == x) && (draw_poly_y.array[draw_poly_x.used-1] == y))
			return;
	}

	vtc0_append(&draw_poly_x, x);
	vtc0_append(&draw_poly_y, y);
}

static void draw_poly_uninit(void)
{
	vtc0_uninit(&draw_poly_x);
	vtc0_uninit(&draw_poly_y);
}

csch_inline void draw_poly_arc(csch_arc_t *arc)
{
	double sa = arc->inst.c.start, da = arc->inst.c.delta;
	long int n, steps;
	double a, step;

	if (rnd_render != NULL) {
		double cpp = rnd_render->coord_per_pix;

		if (cpp <= 0)
			cpp = RND_MIL_TO_COORD(1000.0)/600.0; /* assume 600 DPI, for the exporters */

		steps = (double)C2P(arc->inst.c.r) * (double)da / cpp / 10.0;
		if (steps < 0)
			steps= - steps;
		if (steps < 3)
			steps = 3;
		else if (steps > 1000)
			steps = 1000;
	}
	else
		steps = 3;


	step = da/steps;
	for(a = sa, n = 0; n < steps; a += step, n++) {
		csch_coord_t x, y;
		x = rnd_round(arc->inst.c.c.x + arc->inst.c.r * cos(a));
		y = rnd_round(arc->inst.c.c.y + arc->inst.c.r * sin(a));
		draw_poly_append(C2P(x), C2P(y));
	}
}

csch_inline void draw_layer_poly(draw_ctx_t *ctx, csch_cpoly_t *p, csch_cpen_t *stroke, int selected)
{
	long n;
	csch_coutline_t *o;


	if (p->has_fill) {
		rnd_render->set_color(ctx->gc, sch_rnd_fill_color(ctx, &p->hdr, csch_fill_fallback(p->hdr.sheet, p, ctx->xform == NULL ? NULL : ctx->xform->fallback_pen), selected));
		draw_poly_reset();
		for(n = 0, o = p->outline.array; n < p->outline.used; n++, o++) {
			switch(o->hdr.type) {
				case CSCH_CTYPE_LINE:
					draw_poly_append(C2P(o->line.inst.c.p1.x), C2P(o->line.inst.c.p1.y));
					draw_poly_append(C2P(o->line.inst.c.p2.x), C2P(o->line.inst.c.p2.y));
					break;
				case CSCH_CTYPE_ARC:
					draw_poly_arc(&o->arc);
					break;
				default:
					/* invalid object in contour, already warned */
					break;
			}
		}
		rnd_render->fill_polygon(ctx->gc, draw_poly_x.used, draw_poly_x.array, draw_poly_y.array);
	}

	if (p->has_stroke) {
		rnd_render->set_color(ctx->gc, sch_rnd_stroke_color(ctx, &p->hdr, stroke, selected));
		rnd_hid_set_line_cap(ctx->gc, pen_cap(stroke));
		rnd_hid_set_line_width(ctx->gc, C2P(stroke->size));

		for(n = 0, o = p->outline.array; n < p->outline.used; n++, o++) {
			switch(o->hdr.type) {
				case CSCH_CTYPE_LINE:
					rnd_render->draw_line(ctx->gc, C2P(o->line.inst.c.p1.x), C2P(o->line.inst.c.p1.y), C2P(o->line.inst.c.p2.x), C2P(o->line.inst.c.p2.y));
					break;
				case CSCH_CTYPE_ARC:
					rnd_render->draw_arc(ctx->gc, C2P(o->arc.inst.c.c.x), C2P(o->arc.inst.c.c.y), C2P(o->arc.inst.c.r), C2P(o->arc.inst.c.r), (180.0 - o->arc.inst.c.start * RND_RAD_TO_DEG), -(o->arc.inst.c.delta * RND_RAD_TO_DEG));
					break;
				default:
					/* invalid, do not draw */
					break;
			}
		}
	}
}

csch_inline void draw_layer_arc(draw_ctx_t *ctx, csch_arc_t *a, csch_cpen_t *pen, int selected)
{
	rnd_render->set_color(ctx->gc, sch_rnd_stroke_color(ctx, &a->hdr, pen, selected));
	rnd_hid_set_line_cap(ctx->gc, pen_cap(pen));
	rnd_hid_set_line_width(ctx->gc, C2P(pen->size));
	rnd_render->draw_arc(ctx->gc, C2P(a->inst.c.c.x), C2P(a->inst.c.c.y), C2P(a->inst.c.r), C2P(a->inst.c.r), (180.0 - a->inst.c.start * RND_RAD_TO_DEG), -(a->inst.c.delta * RND_RAD_TO_DEG));

	if (conf_core.rc.debug.draw_arc_bbox) {
		rnd_hid_set_line_width(ctx->gc, -1);
		rnd_render->draw_line(ctx->gc, C2P(a->hdr.bbox.x1), C2P(a->hdr.bbox.y1), C2P(a->hdr.bbox.x2), C2P(a->hdr.bbox.y1));
		rnd_render->draw_line(ctx->gc, C2P(a->hdr.bbox.x1), C2P(a->hdr.bbox.y1), C2P(a->hdr.bbox.x1), C2P(a->hdr.bbox.y2));
		rnd_render->draw_line(ctx->gc, C2P(a->hdr.bbox.x2), C2P(a->hdr.bbox.y2), C2P(a->hdr.bbox.x2), C2P(a->hdr.bbox.y1));
		rnd_render->draw_line(ctx->gc, C2P(a->hdr.bbox.x2), C2P(a->hdr.bbox.y2), C2P(a->hdr.bbox.x1), C2P(a->hdr.bbox.y2));
	}
}

static csch_rtree_dir_t predraw_layer_obj(void *ctx_, void *obj_, const csch_rtree_box_t *box)
{
	draw_ctx_t *ctx = (draw_ctx_t *)ctx_;
	csch_chdr_t *obj = obj_;

	switch(obj->type) {
		case CSCH_CTYPE_TEXT: /* need to update font in a separate pass because it changes the rtree we are searching */
			{
				csch_text_t *t = (csch_text_t *)obj;
				if ((rnd_render->draw_pixmap != NULL) && (t->pixmap == NULL))
					vtp0_append(&ctx->text_update, t);
			}
			break;
		default: break;
	}

	return csch_RTREE_DIR_FOUND_CONT;

}

static void draw_grp_meta(draw_ctx_t *ctx, csch_cgrp_t *grp)
{
	int use_local = (ctx->xform != NULL) && ctx->xform->use_local_vis;
	int vis_meta_sym = use_local ? ctx->xform->local_vis[CSCH_DSPLY_SYMBOL_META] : csch_layer_vis[CSCH_DSPLY_SYMBOL_META];

	if (vis_meta_sym && (grp->role == CSCH_ROLE_SYMBOL) && (grp->hdr.bbox.x1 < grp->hdr.bbox.x2)) {
		int embed = grp->hdr.type == CSCH_CTYPE_GRP;
		rnd_render->set_color(ctx->gc, (embed ? &conf_core.appearance.color.symbol_meta_embed : &conf_core.appearance.color.symbol_meta_loclib));
		rnd_hid_set_line_cap(ctx->gc, rnd_cap_round);
		rnd_hid_set_line_width(ctx->gc, -1);

		pcb_draw_dashed_line(ctx->gc, C2P(grp->hdr.bbox.x1), C2P(grp->hdr.bbox.y1), C2P(grp->hdr.bbox.x2), C2P(grp->hdr.bbox.y1), 5, rnd_true);
		pcb_draw_dashed_line(ctx->gc, C2P(grp->hdr.bbox.x2), C2P(grp->hdr.bbox.y1), C2P(grp->hdr.bbox.x2), C2P(grp->hdr.bbox.y2), 5, rnd_true);
		pcb_draw_dashed_line(ctx->gc, C2P(grp->hdr.bbox.x2), C2P(grp->hdr.bbox.y2), C2P(grp->hdr.bbox.x1), C2P(grp->hdr.bbox.y2), 5, rnd_true);
		pcb_draw_dashed_line(ctx->gc, C2P(grp->hdr.bbox.x1), C2P(grp->hdr.bbox.y2), C2P(grp->hdr.bbox.x1), C2P(grp->hdr.bbox.y1), 5, rnd_true);

		/* draw E or L for embed or local */
		rnd_hid_set_line_width(ctx->gc, -2);
		rnd_render->draw_line(ctx->gc, C2P(grp->hdr.bbox.x1 + 1000), C2P(grp->hdr.bbox.y2 - 1000), C2P(grp->hdr.bbox.x1 + 1000), C2P(grp->hdr.bbox.y2 - 3000));
		rnd_render->draw_line(ctx->gc, C2P(grp->hdr.bbox.x1 + 1000), C2P(grp->hdr.bbox.y2 - 3000), C2P(grp->hdr.bbox.x1 + 2000), C2P(grp->hdr.bbox.y2 - 3000));
		if (embed) {
			rnd_render->draw_line(ctx->gc, C2P(grp->hdr.bbox.x1 + 1000), C2P(grp->hdr.bbox.y2 - 1000), C2P(grp->hdr.bbox.x1 + 2000), C2P(grp->hdr.bbox.y2 - 1000));
			rnd_render->draw_line(ctx->gc, C2P(grp->hdr.bbox.x1 + 1000), C2P(grp->hdr.bbox.y2 - 2000), C2P(grp->hdr.bbox.x1 + 1500), C2P(grp->hdr.bbox.y2 - 2000));
		}

	}
}

static csch_rtree_dir_t draw_layer_obj(void *ctx_, void *obj_, const csch_rtree_box_t *box)
{
	draw_ctx_t *ctx = (draw_ctx_t *)ctx_;
	csch_chdr_t *obj = obj_;
	csch_cpen_t *stroke = csch_stroke_fallback_(obj, ctx->xform == NULL ? NULL : ctx->xform->fallback_pen);
	int sel = csch_chdr_is_selected(obj);

	switch(obj->type) {
		case CSCH_CTYPE_LINE: draw_layer_line(ctx, (csch_line_t *)obj, stroke, sel); break;
		case CSCH_CTYPE_POLY: draw_layer_poly(ctx, (csch_cpoly_t *)obj, stroke, sel); break;
		case CSCH_CTYPE_ARC:  draw_layer_arc(ctx, (csch_arc_t *)obj, stroke, sel); break;
		case CSCH_CTYPE_TEXT: draw_layer_text(ctx, (csch_text_t *)obj, stroke, sel); break;
		case CSCH_CTYPE_CONN: draw_layer_conn(ctx, (csch_conn_t *)obj); break;
		case CSCH_CTYPE_BITMAP: /* not supported yet */ break;

		case CSCH_CTYPE_GRP:
		case CSCH_CTYPE_GRP_REF:
			/* no need to draw groups since we are working from rtree, but need to draw meta */
			draw_grp_meta(ctx, (csch_cgrp_t *)obj);
			break;

		case CSCH_CTYPE_PEN:
		case CSCH_CTYPE_invalid:
		case CSCH_CTYPE_max:
			/* no graphical representation */
			break;
	}

	return csch_RTREE_DIR_FOUND_CONT;
}

static csch_rtree_dir_t draw_layer_obj_nojunc(void *ctx_, void *obj_, const csch_rtree_box_t *box)
{
	if (!csch_obj_is_junction(obj_))
		return draw_layer_obj(ctx_, obj_, box);
	return csch_RTREE_DIR_NOT_FOUND_CONT;
}

static csch_rtree_dir_t draw_layer_obj_junc(void *ctx_, void *obj_, const csch_rtree_box_t *box)
{
	if (csch_obj_is_junction(obj_))
		draw_layer_obj(ctx_, obj_, box);
	return csch_RTREE_DIR_NOT_FOUND_CONT;
}


static void draw_layer(csch_sheet_t *sheet, csch_displayer_t layer, const csch_rtree_box_t *bbox, rnd_hid_gc_t gc, rnd_xform_t *xform)
{
	draw_ctx_t ctx = {0};
	long n;

	if ((layer == CSCH_DSPLY_SYMBOL_META) || (layer == CSCH_DSPLY_TEXT_META))
		return;

	ctx.sheet = sheet;
	ctx.gc = gc;
	ctx.xform = xform;

	csch_rtree_search_obj(&sheet->dsply[layer], bbox, predraw_layer_obj, &ctx);

	for(n = 0; n < ctx.text_update.used; n++) {
		csch_chdr_t *obj = ctx.text_update.array[n];
		sch_rnd_font_text_update(sheet, (csch_text_t *)obj, csch_stroke_fallback_(obj, ctx.xform == NULL ? NULL : ctx.xform->fallback_pen));
	}
	vtp0_uninit(&ctx.text_update);

	if (layer == CSCH_DSPLY_WIRE) {
		/* have to draw in two steps so junctions are always above wires */
		csch_rtree_search_obj(&sheet->dsply[layer], bbox, draw_layer_obj_nojunc, &ctx);
		csch_rtree_search_obj(&sheet->dsply[layer], bbox, draw_layer_obj_junc, &ctx);
	}
	else
		csch_rtree_search_obj(&sheet->dsply[layer], bbox, draw_layer_obj, &ctx);
}

void sch_rnd_draw_sheet(csch_sheet_t *sheet, rnd_hid_gc_t gc, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller)
{
	int n;
	csch_rtree_box_t bb;

	bb.x1 = P2C(region->view.X1); bb.y1 = P2C(region->view.Y1);
	bb.x2 = P2C(region->view.X2); bb.y2 = P2C(region->view.Y2);

	for(n = 0; n < CSCH_DSPLY_max; n++) {
		int use_local = (xform_caller != NULL) && xform_caller->use_local_vis;
		int vis =  use_local ? xform_caller->local_vis[n] : csch_layer_vis[n];
		if (vis)
			draw_layer(sheet, n, &bb, gc, xform_caller);
	}
}

void sch_rnd_expose_main(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller)
{
	rnd_hid_gc_t gc;
	int want_layer;
	csch_sheet_t *sheet = (csch_sheet_t *)region->design;
	rnd_hid_t *save = rnd_render;
	
	if ((rnd_render != NULL) && (!rnd_render->override_render))
		rnd_render = hid;

	gc = rnd_render->make_gc(rnd_render);

	rnd_render->render_burst(rnd_render, RND_HID_BURST_START, &region->view);

	/* the HID needs a group set, even if we don't do groups */
	if (rnd_render->set_layer_group != NULL)
		want_layer = rnd_render->set_layer_group(rnd_render, region->design, 1, NULL, 0, 0, 0, 0, NULL);
	else
		want_layer = 0;

	if (want_layer) {
		rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_RESET, 1, &region->view);
		rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_POSITIVE, 1, &region->view);

		sch_rnd_draw_sheet(sheet, gc, region, xform_caller);

		rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_FLUSH, 1, &region->view);
		rnd_render->end_layer(rnd_render);
	}
	rnd_render->render_burst(rnd_render, RND_HID_BURST_END, &region->view);

	rnd_render->destroy_gc(gc);
	rnd_render = save;
}


void sch_rnd_expose_preview(rnd_hid_t *hid, rnd_hid_expose_ctx_t *e)
{
	rnd_hid_gc_t gc = rnd_render->make_gc(rnd_render);

	rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_RESET, 1, &e->view);
	rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_POSITIVE, 1, &e->view);
	e->expose_cb(gc, e);
	rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_FLUSH, 1, &e->view);
	rnd_render->end_layer(rnd_render);

	rnd_render->destroy_gc(gc);
}

void sch_rnd_redraw(rnd_design_t *target)
{
	rnd_design_t *curr = rnd_multi_get_current();

	if ((target == NULL) || (target == curr))
		rnd_render->invalidate_all(rnd_render);
}


void sch_rnd_obj_redraw_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_chdr_t *obj = argv[1].d.p;
	assert(argv[1].type == RND_EVARG_PTR);
	if (rnd_render->gui && (rnd_render->invalidate_lr != NULL) && (obj->bbox.x2 > obj->bbox.x1))
		rnd_render->invalidate_lr(rnd_render, C2P(obj->bbox.x1), C2P(obj->bbox.x2), C2P(obj->bbox.y1), C2P(obj->bbox.y2));
}

void sch_rnd_draw_init2(void)
{
	rnd_event_bind(CSCH_EVENT_OBJ_NEEDS_REDRAW, sch_rnd_obj_redraw_ev, NULL, draw_cookie);
}

void sch_rnd_draw_uninit(void)
{
	draw_poly_uninit();
	rnd_event_unbind_allcookie(draw_cookie);
}
