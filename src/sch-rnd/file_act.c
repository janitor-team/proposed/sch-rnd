/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Generic actions usually related to the currently open file(s) */

#include <libcschem/config.h>
#include <libcschem/concrete.h>
#include <libcschem/plug_library.h>
#include <libcschem/event.h>
#include <libcschem/plug_io.h>
#include <librnd/core/actions.h>
#include <librnd/hid/hid.h>
#include <librnd/core/compat_misc.h>

#include "sheet.h"
#include "conf_core.h"
#include "build_run.h"


static const char csch_acts_Quit[] = "Quit()";
static const char csch_acth_Quit[] = "Quits sch-rnd after confirming.";
static fgw_error_t csch_act_Quit(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	csch_sheet_t *sheet = CSCH_ACT_SHEET;
	const char *force = NULL;
	RND_ACT_MAY_CONVARG(1, FGW_STR, Quit, force = argv[1].val.str);

	if ((force != NULL) && (rnd_strcasecmp(force, "force") == 0))
		exit(0);

TODO("need to check all sheets, not just the current one")
	if (!sheet->changed || (rnd_hid_message_box(RND_ACT_DESIGN, "warning", "Close: lose data", "Exiting sch-rnd will lose unsaved sheet modifications.", "cancel", 0, "ok", 1, NULL) == 1))
		sch_rnd_quit_app();

	RND_ACT_IRES(-1);
	return 0;
}

static const char csch_acts_SymlibRehash[] = "SymlibRehash()";
static const char csch_acth_SymlibRehash[] = "Rebuild the in-memory tree of symbol libraries";
static fgw_error_t csch_act_SymlibRehash(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	csch_sheet_t *sheet = CSCH_ACT_SHEET;
	csch_lib_master_t *master = csch_lib_get_master("symbol", 1);

	csch_lib_clear_sheet_lib(sheet, master->uid);
	csch_lib_add_all(sheet, master, &conf_core.rc.library_search_paths);
	rnd_event(&sheet->hidlib, CSCH_EVENT_LIBRARY_CHANGED, NULL);
	RND_ACT_IRES(0);
	return 0;
}

static const char csch_acts_Revert[] = "Revert([sheet|project])";
static const char csch_acth_Revert[] = "Revert to on-disk version of file(s). Default target is sheet.";
fgw_error_t csch_act_Revert(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *op = "sheet";
	csch_sheet_t *sheet = CSCH_ACT_SHEET;

	RND_ACT_MAY_CONVARG(1, FGW_STR, Revert, op = argv[1].val.str);

	if (rnd_strcasecmp(op, "sheet") == 0) {
		csch_revert_sheet(sheet, sch_rnd_sheet_new4revert);
	}
	else if (rnd_strcasecmp(op, "project") == 0) {
		rnd_message(RND_MSG_ERROR, "Revert(project,...) not yet implemented\n");
	}
	else
		RND_ACT_FAIL(Revert);

	RND_ACT_IRES(0);
	return 0;
}

static rnd_action_t file_action_list[] = {
	{"Quit", csch_act_Quit, csch_acth_Quit, csch_acts_Quit},
	{"SymlibRehash", csch_act_SymlibRehash, csch_acth_SymlibRehash, csch_acts_SymlibRehash},
	{"Revert", csch_act_Revert, csch_acth_Revert, csch_acts_Revert}
};

void sch_rnd_file_act_init2(void)
{
	RND_REGISTER_ACTIONS(file_action_list, NULL);
}
