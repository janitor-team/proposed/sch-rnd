ha:rnd-menu-v1 {
	li:mouse {
		li:left {
			li:press            = { Tool(Press) }
			li:press-shift      = { Tool(Press) }
			li:press-ctrl       = { Tool(Press) }
			li:release          = { Tool(Release) }
			li:release-shift    = { Tool(Release) }
			li:release-ctrl     = { Tool(Release) }
		}
		li:middle {
			li:press              = { Pan(1) }
			li:release            = { Pan(0) }
		}
		li:right {
			li:press         = { }
			li:release       = { Tool(Escape); Popup(popup-obj, obj-type) }
			li:shift-release = { Popup(popup-obj-misc) }
			li:press-ctrl    = { Display(CycleCrosshair) }
		}
		li:scroll-up {
			li:press        = { Zoom(0.8) }
			li:press-shift  = { Scroll(up) }
			li:press-ctrl   = { Scroll(left) }
		}
		li:scroll-down {
			li:press       = { Zoom(1.25) }
			li:press-shift = { Scroll(down) }
			li:press-ctrl  = { Scroll(right) }
		}
	}

	# List of tool names for the toolbar.
	# Do not specify keys here, they are specified in the menu; the GUI will
	# not pick up keys from this subtree. There's also no action here: the
	# plugin handling this subtree will always pick the tool.
	li:toolbar_static {
		ha:circle    {tip={draw a circle (atomic, drawing primitive)}}
		ha:wirenet   {tip={draw a line segment for a network (wire-net)}}
		ha:line      {tip={draw a line (atomic, drawing primitive)}}
		ha:text      {tip={draw text on the sheet}}
		ha:rect      {tip={draw a rectangle (4 lines)}}
		ha:buffer    {tip={paste the current buffer on the board}}
		ha:remove    {tip={remove object at location clicked}}
		ha:rotate    {tip={rotate object by 90 degree at location clicked}}
		ha:xmirror   {tip={horizontal mirror (changes x coords)}}
		ha:ymirror   {tip={vertical mirror (changes y coords)}}
		ha:arrow     {tip={switch to arrow mode}}
		ha:lock      {tip={lock or unlock object clicked}}

		ha:move      {tip={drag&drop move objects}}
		ha:copy      {tip={drag&drop copy objects}}
	}

	li:main_menu {
		### File Menu
		ha:File {
			li:submenu {
				ha:New sheet            = { li:a={{<key>f;<key>n}; {Ctrl<Key>n};}; action=New() }
				ha:Revert sheet         = { a={<key>f;<key>r};                     action=Revert(sheet); tip=Revert to the sheet stored on disk }
				ha:Unload sheet         = { a={<key>f;<key>u}; action=Unload(sheet) }
				-
				ha:Load...              = { a={<key>f;<key>o}; action=Load() }
				-
				ha:Save sheet...        = { li:a={{<key>f;<key>s}; {Ctrl<key>s};}; action=Save() }
				ha:Save sheet as...     = { a={<key>f;<key>a}; action=SaveAs() }
				-
				ha:Import {
					li:submenu {
						ha:Load symbol/group to paste-buffer = { li:action={BufferClear(); BufferLoad(Symbol); Tool(buffer); } }
						ha:Load back annotation file         = { li:action={Backann();} }
					}
				}
				-
				ha:Print sheet...       = { a={<key>f;<key>p}; action=Print()}
				ha:Export Sheet...      = { a={<key>f;<key>e}; action=ExportDialog()}
				ha:Export Project...    = { a={<key>f;<key>j}; action=ExportProjectDialog()}
				-
				ha:Preferences...       = { a={<key>i;<key>c;<key>p}; action=preferences}
				ha:Maintenance {
					li:submenu {
						ha:Re-scan the symbol library     = { a={<key>i;<key>c;<key>r}; action=SymlibRehash() }
						ha:Data integrity check           = { a={<key>i;<key>c;<key>i}; action=Integrity() }
						-
						ha:Undo dialog (for debugging)    = { a={<key>u;<key>d;}; action=UndoDialog() }
					}
				}
				-
				ha:Quit                 = { a={<key>f;<key>q};          action=Quit() }
			}
		}

		ha:Edit {
			li:submenu {
				ha:Undo last operation          = { li:a={{<key>u; <Key>u}; {Ctrl<Key>z};};   action=Undo() }
				ha:Redo last undone operation   = { li:a={{<key>u; <Key>r}; {Ctrl<Key>y};};   action=Redo() }
				ha:Clear undo-buffer            = { a={<key>u; <Key>c}; action=Undo(ClearList) }
				-
				ha:Cut selection to buffer      = { li:a={{<key>e; <Key>x}; a=Ctrl<Key>x;};   li:action={ GetXY(Click to set the snap point for this buffer); BufferClear(Clear); BufferCut(MoveSelected); Tool(buffer) } }
				ha:Copy selection to buffer     = { li:a={{<key>e; <key>c}; {Ctrl<Key>c;};};  li:action={ GetXY(Click to set the snap point for this buffer); BufferClear(Clear); BufferCopy(); Unselect(All); Tool(buffer) } }
				ha:Paste buffer to layout       = {    a={Ctrl<Key>v;};                          action=Tool(buffer) }
				-
				ha:Remove object                = { li:a={{<key>e;<key>d};{<Key>Delete};}; li:action={Tool(Save); Tool(remove); GetXY(Click on object to remove); Tool(Press); Tool(Restore)} }
				ha:Whirl object (rotate 90 deg) = { li:a={{<key>e;<key>w};}; action=Rotate90(auto); }
				ha:Mirror object horizontally   = { li:a={{<key>e;<key>h};}; action=Mirror(auto, horizontal); }
				ha:Mirror object vertically     = { li:a={{<key>e;<key>v};}; action=Mirror(auto, vertical); }
				ha:Place template {
					li:submenu {
						ha:place terminal           = { a={<key>p;<key>t}; li:action={GetXy(Click where the new terminal should be placed); PlaceTerminal()}; }
						ha:place attribute text     = { a={<key>p;<key>a}; li:action={GetXy(Click where the new attribute should be placed); PlaceAttrib()}; }
					}
				}
				-
				ha:Change object {
					li:submenu {
						ha:Text string                 = { li:a={{<key>e;<key>t};};  action=EditText(Object) }
						ha:Stroke                      = { li:a={{<key>e;<key>s};}; action={pendialog(object)} }
					}
				}
				ha:Change multiple objects {
					li:submenu {
						ha:Renumber symbols            = { li:a={{<key>e;<key>m;<key>r};};  action=RenumberDialog }
					}
				}
				ha:Object Properties...           = { a={<key>e; <key>p}; action=PropEdit(selection) }
			}
		}

		ha:View {
			li:submenu {
			ha:Grid {
				li:submenu {
					ha:Enable visible grid        = { a={<key>g; <key>v}; checked=editor/draw_grid; action=conf(toggle, editor/draw_grid, design) }
					ha:Enable local grid          = { a={<key>g; <key>l}; checked=editor/local_grid/enable; action=conf(toggle, editor/local_grid/enable, design) }
					ha:Grid size = {
						li:submenu {
							ha:No Grid                = { checked=ChkGridSize(none); action=SetGrid(1); update_on={editor/grid} }
							-
							@grid
							-
							ha:Previous grid          = { li:a={{<key>g; <key>b};{<char>[};}; action=Grid(down) }
							ha:Next grid              = { li:a={{<key>g; <key>f};{<char>]};}; action=Grid(up) }
							ha:Custom grid            = { li:a={{<key>g; <key>c};}; action=Grid(ask) }
							-
							ha:Grid *2                = { a={<key>g; <key>d}; action=SetGrid(*2) }
							ha:Grid /2                = { a={<key>g; <key>h}; action=SetGrid(/2) }
						}
					}
					ha:Grid properties = {
						li:submenu {
							ha:Enable local grid      = { checked=editor/local_grid/enable; action=conf(toggle, editor/local_grid/enable, design) }
							-
							ha:local grid radius 4    = { checked={conf(iseq, editor/local_grid/radius, 4)}; li:action={conf(set, editor/local_grid/radius, 4, design); conf(set, editor/local_grid/enable, 1, design) } update_on={} }
							ha:local grid radius 8    = { checked={conf(iseq, editor/local_grid/radius, 8)}; li:action={conf(set, editor/local_grid/radius, 8, design); conf(set, editor/local_grid/enable, 1, design) } update_on={} }
							ha:local grid radius 16   = { checked={conf(iseq, editor/local_grid/radius, 16)}; li:action={conf(set, editor/local_grid/radius, 16, design); conf(set, editor/local_grid/enable, 1, design) } update_on={} }
							ha:local grid radius 32   = { checked={conf(iseq, editor/local_grid/radius, 32)}; li:action={conf(set, editor/local_grid/radius, 32, design); conf(set, editor/local_grid/enable, 1, design) } update_on={} }
							-
							ha:sparse global grid     = { checked=editor/global_grid/sparse; action=conf(toggle, editor/global_grid/sparse, design) }
							ha:global grid density 4  = { checked={conf(iseq, editor/global_grid/min_dist_px, 4)}; li:action={conf(set, editor/global_grid/min_dist_px, 4, design); conf(set, editor/local_grid/enable, 0, design) } update_on={} }
							ha:global grid density 8  = { checked={conf(iseq, editor/global_grid/min_dist_px, 8)}; li:action={conf(set, editor/global_grid/min_dist_px, 8, design); conf(set, editor/local_grid/enable, 0, design) } update_on={} }
							ha:global grid density 16 = { checked={conf(iseq, editor/global_grid/min_dist_px, 16)}; li:action={conf(set, editor/global_grid/min_dist_px, 16, design); conf(set, editor/local_grid/enable, 0, design) } update_on={} }
						}
					}
					ha:Realign grid               = { a={<key>g; <key>r}; action={ Display(RealignGrid) } }
					}
				}

				ha:Zoom {
					li:submenu {
						ha:Zoom In 20%            = { li:a={{<Key>z;<Key>z;}; {<char>+};} action=Zoom(-1.2) }
						ha:Zoom Out 20%           = { li:a={{<Key>z;<Key>x;}; {<char>-};} action=Zoom(+1.2) }
						ha:Zoom In 2X             = { action=Zoom(-2) }
						ha:Zoom Out 2X            = { action=Zoom(+2) }
						ha:Zoom to 0.1mil/px      = { action={Zoom(=0.1mil)} }
						ha:Zoom to 0.01mm/px      = { action={Zoom(=0.01mm)} }
						ha:Zoom to 1mil/px        = { action={Zoom(=1mil)} }
						ha:Zoom to 0.05mm/px      = { action={Zoom(=0.05mm)} }
						ha:Zoom to 2.5mil/px      = { action={Zoom(=2.5mil)} }
						ha:Zoom to 0.1mm/px       = { action={Zoom(=0.1mm)} }
						ha:Zoom to 10mil/px       = { action={Zoom(=10mil)} }
						ha:Zoom In 20% and center = { li:action={Zoom(-1.2); Center()} }
						ha:Zoom Out 20% and center= { li:action={Zoom(+1.2); Center()} }
						-
						ha:Zoom Extents           = {      li:a={{<Key>z;<Key>e;}; {<key>v;<key>f};}; action=Zoom() }
						ha:Zoom to selection      = {      a={<Key>z;<Key>s;}; action=ZoomTo(selected) }
						ha:Zoom to found          = {      a={<Key>z;<Key>f;}; action=ZoomTo(found) }
						-
						ha:Center cursor          = { a={<key>v; <key>c} action=Center() }
					}
				}
				ha:Current layer {
					li:submenu {
						ha:TODO             { li:a={{<Key>l;<Key>r}; {<Key>l;<Key>-};};         action=Layer(remove) }
					}
				}
				ha:Full screen                     = { checked=editor/fullscreen; a=<char>\\;  action=fullscreen(toggle) }
				ha:Reset {
					li:submenu {
						ha:Reset View                      = { a={<key>v; <key>r; <key>v}; sy:action={/scripts/view_reset}; tip={Reset all visibility related states to make sure all objects are visible} }
						ha:Reset GUI                       = { a={<key>v; <key>r; <key>g}; sy:action={/scripts/gui_reset}; tip={Reset all GUI related states } }
					}
				}
				ha:Sheet {
					li:submenu {
						ha:Previous                        = { li:a={{<key>v; <key>d; <key>p};{<char><};}; action={SwitchRelative(-1)}; tip={Switch to the previous sheet on the list of all-loaded-sheets} }
						ha:Next                            = { li:a={{<key>v; <key>d; <key>n};{<char>\>};}; action={SwitchRelative(+1)}; tip={Switch to the next sheet on the list of all-loaded-sheets} }
					}
				}
			}
		}


		ha:Mode = {
			li:submenu {
				ha:Lines and nets {
					li:submenu {
						ha:Cycle line clip/refraction (toggle) = { li:a={{<key>m; <key>l; <key>f}; {<char>/};}; action=conf(toggle, editor/line_refraction, design) }
						ha:Continuous draw (toggle)            = { li:a={{<key>m; <key>l; <key>c};}; action=conf(toggle, editor/line_cont, design) }
					}
				}
				ha:Drawing {
					li:submenu {
						ha:Lock floaters                    = { a={<key>m; <key>f; <key>l}; checked=editor/lock_floaters; action=ToggleFloaters(lock); }
						ha:Only floaters                    = { a={<key>m; <key>f; <key>o}; checked=editor/only_floaters; action=ToggleFloaters(only); }
					}
				}
				ha:Symbols and libraries {
					li:submenu {
						ha:Enable symbol auto-pasting to local lib = { a={<key>m; <key>s; <key>p}; checked=editor/paste_to_local_lib; action=conf(toggle, editor/paste_to_local_lib, design); tip={When enabled, symbols placed from (external) symbol library are first copied into the local symbol library then group references to the local copy are placed}}
					}
				}
				ha:Tool {
					li:submenu {
						ha:Circle         = { checked=ChkMode(circle);      li:a={{<key>t;<key>c};{<Key>F1};}; action=Tool(circle); update_on={editor/mode} }
						ha:Wirenet        = { checked=ChkMode(wirenet);     li:a={{<key>t;<key>w};{<Key>F2};}; action=Tool(wirenet); update_on={editor/mode} }
						ha:Line           = { checked=ChkMode(line);        li:a={{<key>t;<key>l};{<Key>F3};}; action=Tool(line); update_on={editor/mode} }
						ha:Text           = { checked=ChkMode(text);        li:a={{<key>t;<key>t};{<Key>F4};}; action=Tool(text); update_on={editor/mode} }
						ha:Rectangle      = { checked=ChkMode(rect);        li:a={{<key>t;<key>r};{<Key>F5};}; action=Tool(rect); update_on={editor/mode} }
						ha:Buffer         = { checked=ChkMode(buffer);      li:a={{<key>t;<key>b};{<Key>F7};}; action=Tool(buffer); update_on={editor/mode} }
						ha:Del/remove     = { checked=ChkMode(remove);      li:a={{<key>t;<key>d};{<Key>F8};}; action=Tool(remove); update_on={editor/mode} }
						ha:Whirl (rotate 90 deg) = {checked=ChkMode(rotate);li:a={{<key>t;<key>o};{<Key>F9};}; action=Tool(rotate); update_on={editor/mode} }
						ha:xmirror        = { checked=ChkMode(xmirror);     li:a={{<key>t;<key>x};}; action=Tool(xmirror); update_on={editor/mode} }
						ha:ymirror        = { checked=ChkMode(ymirror);     li:a={{<key>t;<key>y};}; action=Tool(ymirror); update_on={editor/mode} }
						ha:Arrow          = { checked=ChkMode(arrow);       li:a={{<key>t;<key>n};{<Key>F11};}; action=Tool(arrow); update_on={editor/mode} }
						ha:Lock           = { checked=ChkMode(lock);        li:a={{<key>t;<key>k};{<Key>F12};}; action=Tool(lock); update_on={editor/mode} }
						ha:Move           = { checked=ChkMode(move);        li:a={{<key>t;<key>m};}; action=Tool(move); update_on={editor/mode} }
						ha:Copy           = { checked=ChkMode(copy);        li:a={{<key>t;Shift<key>p};}; action=Tool(copy); update_on={editor/mode} }
						-
						ha:Cancel         = { a={<Key>Escape}; action=Tool(escape); }
						ha:Change pen     = { a={<Key>t;<Key>p}; action=Preferences(colors); }
					}
				} # Tool
			}
		}

		ha:Select = {
			li:submenu {
				ha:Select all visible objects      = { a={<key>s; <key>a; <key>a;}; action=Select(All) }
				ha:Unselect all objects            = { a={<key>s; <key>u; <key>a;}; action=Unselect(All) }
				ha:Invert selection                = { a={<key>s; <key>i; }; action=Select(Invert) }
				-
				ha:Advanced search and select      = { a={<key>s; <key>s;} action=SearchDialog() }
				ha:List locked objects             = { a={<key>s; <key>l; <key>k;} action={query(view, "@.locked == 1")} }
				ha:List unnamed symbols            = { a={<key>s; <key>l; <key>u;} action={query(view, '@.a.role == "sym" || @.a.name ~ "[?]$"') } }
				-
				ha:Remove selected objects         = { a={<key>s; <key>r;} action=RemoveSelected() }
				ha:Convert selection to {
					li:submenu {
						ha:Polygon        = { a={<key>s;<key>c;<key>p}; li:action={GetXY(Click to set origin); Convert(selected, polygon);} }
						ha:Symbol         = { a={<key>s;<key>c;<key>s}; li:action={GetXY(Click to set origin); Convert(selected, symbol);} }
						ha:Terminal       = { a={<key>s;<key>c;<key>t}; li:action={GetXY(Click to set origin); Convert(selected, terminal);} }
						ha:Custom group   = { a={<key>s;<key>c;<key>g}; li:action={GetXY(Click to set origin); Convert(selected, group);} }
					}
				}
				ha:Break up selected... {
					li:submenu {
						ha:objects        = { a={<key>s;<key>b;<key>b}; action=Breakup(selected, any, sheet); }
						ha:groups         = { a={<key>s;<key>b;<key>g}; action=Breakup(selected, group, sheet); }
						ha:polygons       = { a={<key>s;<key>b;<key>p}; action=Breakup(selected, poly, sheet); }
					}
				}
				ha:Change selected objects {
					li:submenu {
						ha:Edit properties   = { a={<key>s; <key>p}; action=propedit(selection) }
					}
				}
			}
		} # Select

		ha:Buffer {
			li:submenu {
				ha:Rotate buffer 90 deg CCW (left)  = { a={<key>b;<key>r;<key>l}; action=Rotate90(Buffer, 1); }
				ha:Rotate buffer 90 deg CW (right)  = { a={<key>b;<key>r;<key>r}; action=Rotate90(Buffer, -1); }
				ha:Arbitrarily Rotate Buffer        = { a={<key>b;<key>r;<key>a}; action=Rotate(Buffer, ask); }
				ha:Mirror buffer (left/right)       = { a={<key>b;<key>m;<key>l}; action=Mirror(Buffer, horizontal);}
				ha:Mirror buffer (up/down)          = { a={<key>b;<key>m;<key>u}; action=Mirror(Buffer, vertical); }
#				ha:Scale                            = {  }
#				ha:Normalize                        = { a={<key>b;<key>n}; }
				-
				ha:Clear buffer                    = { a={<key>b;<key>c;<key>c;}; action=BufferClear() }
				ha:Save buffer content to file     = { a={<key>b;<key>f;<key>s;}; action=BufferSave(All) }
				ha:Load buffer content from file   = { a={<key>b;<key>f;<key>l;}; li:action={BufferClear(); BufferLoad(All); Tool(buffer);} }
				-
#				ha:Convert buffer to polygon       = { a={<key>b;<key>c;<key>p}; action=Convert(buffer, polygon); }
#				ha:Convert buffer to symbol        = { a={<key>b;<key>c;<key>s}; action=Convert(buffer, symbol); }
#				ha:Convert buffer to terminal      = { a={<key>b;<key>c;<key>t}; action=Convert(buffer, terminal); }
#				ha:Convert buffer to custom group  = { a={<key>b;<key>c;<key>g}; action=Convert(buffer, group); }
#				ha:Break up buffer                 = { a={<key>b;<key>b;<key>b}; action=Breakup(buffer, any, sheet); }
#				ha:Break up buffer groups          = { a={<key>b;<key>b;<key>g}; action=Breakup(buffer, group, sheet); }
#				ha:Break up buffer polygons        = { a={<key>b;<key>b;<key>p}; action=Breakup(buffer, poly, sheet); }
				-
				ha:Save buffer symbol to file      = { a={<key>b;<key>s;<key>s}; action=BufferSave(Symbol); tip={save one symbol group from the buffer as a group file} }
				ha:Save buffer group to file       = { a={<key>b;<key>s;<key>g}; action=BufferSave(Group); tip={save one group from the buffer as a group file} }
				ha:Load buffer symbol/group from file = { a={<key>b;<key>l;<key>s}; li:action={BufferLoad(Symbol); Tool(buffer);}; tip={load one group from file into the paste buffer } }
				-
				ha:Buffer selection {
					li:submenu {
						ha:Select Buffer \#1 = { checked=BufferChk(1); a={<key>b;<Key>1;} action=BufferSwitch(1); update_on={editor/buffer_number} }
						ha:Select Buffer \#2 = { checked=BufferChk(2); a={<key>b;<Key>2;} action=BufferSwitch(2); update_on={editor/buffer_number} }
						ha:Select Buffer \#3 = { checked=BufferChk(3); a={<key>b;<Key>3;} action=BufferSwitch(3); update_on={editor/buffer_number} }
						ha:Select Buffer \#4 = { checked=BufferChk(4); a={<key>b;<Key>4;} action=BufferSwitch(4); update_on={editor/buffer_number} }
						ha:Select scratchpad = { checked=BufferChk(5); a={<key>b;<Key>5;} action=BufferSwitch(5); update_on={editor/buffer_number} }
					}
				}
			}
		} # Buffer

		ha:Attributes {
			li:submenu {
				ha:Generic group {
					li:submenu {
						ha:Change role        = { a={<key>a;<key>o;}; action=QuickAttr(object, role); tip={Change the role attribute of a group} }
						ha:Change name        = { a={<key>a;<key>n;}; action=AttributeDialog(object, name); tip={Change the name attribute of a group (refdes, pin number, etc)} }
					}
				}
				ha:Symbol {
					li:submenu {
						ha:Change name (refdes) = { a={<key>a;<key>r}; action=AttributeDialog(object, name); tip={Change the name attribute of a group (refdes)} }
						ha:Change value         = { a={<key>a;<key>v}; action=AttributeDialog(object, value); tip={Change the value attribute of a group} }
						ha:Change slot num      = { a={<key>a;<key>s}; action=AttributeDialog(object, -slot); tip={Change the slot number attribute of a group} }
						ha:Change devmap        = { a={<key>a;<key>d;}; action=QuickAttr(object, devmap); tip={Change the devmap attribute of a symbol (device+footprint+pinout mapping) } }
						ha:Change portmap       = { a={<key>a;<key>p;}; action=QuickAttr(object, portmap); tip={Change the portmap attribute of a group} }
						ha:Change connections   = { a={<key>a;<key>c;}; action=QuickAttr(object, connect); tip={Change the symbol terminal to network connection list } }
						ha:Change footprint     = { a={<key>a;<key>f}; action=AttributeDialog(object, footprint); tip={Change the footprint attribute of a group} }
					}
				}
				-
				ha:Change name (refdes) = { a={<key>a;<key>a}; action=AttributeDialog(object, name); tip={Invoke attribute editor} }
			}
		} # Attributes

		ha:Plugins {
			li:submenu {
				ha:feature plugins   = {
					# submenu dedicated for feature plugins to register their menu items
					li:submenu {
						@feature_plugins
					}
				}
				ha:script            = {
					# submenu dedicated for user scripts to register their menu items
					li:submenu {
						@scripts
					}
				}
				ha:Manage plugins... = { a={<Key>p;<Key>m;<Key>p;}; action=ManagePlugins() }
				ha:Manage scripts... = { a={<Key>p;<Key>m;<Key>s;}; action=BrowseScripts() }
			}
		} # Plugins

		ha:Window {
			li:submenu {
				ha:Message Log    = { a={<Key>w;<Key>m}; action=LogDialog() }
				ha:Command Entry  = { li:a={a={<Key>w;<Key>c}; {<char>:};}  action=Command() }
				ha:Tree view      = { a={<Key>w;<Key>t}; action=TreeDialog() }
				ha:Abstract model = { a={<Key>w;<Key>a}; action=AbstractDialog() }
				ha:Library        = { a={<Key>w;<Key>l}; action=LibraryDialog(symbol, sheet) }
				ha:devmaps        = { a={<Key>w;<Key>w;<Key>d}; action=LibraryDialog(devmap, sheet) }
			}
		}

		ha:Help {
			li:submenu {
				ha:About                = { action=About() }
			}
		}
	} #main_menu

	li:popups {
# context sensitive right click: popup per object type under the cursor

		ha:layer {
			li:submenu {
				ha:TODO             { }
			}
		}

		ha:popup-obj-polygon {
			li:submenu {
				ha:edit polygon properties...       { action=propedit(object) }
				ha:polygon in tree view...          { action=TreeDialog(object) }
			}
		}

		ha:popup-obj-arc {
			li:submenu {
				ha:edit arc properties...           { action=propedit(object) }
				ha:arc in tree view...              { action=TreeDialog(object) }
			}
		}

		ha:popup-obj-line {
			li:submenu {
				ha:edit line properties...          { action=propedit(object) }
				ha:line in tree view...             { action=TreeDialog(object) }
			}
		}

		ha:popup-obj-text {
			li:submenu {
				ha:edit text properties...          { action=propedit(object) }
				ha:text in tree view...             { action=TreeDialog(object) }
				ha:edit text string                 { action=EditText(Object) }
			}
		}

		ha:popup-obj-symbol {
			li:submenu {
				ha:edit symbol properties...        { action=propedit(object) }
				ha:edit symbol attributes...        { action=AttributeDialog(last-click) }
				ha:edit symbol pens...              { action=pendialog(object, non-modal) }
				ha:symbol in tree view...           { action=TreeDialog(object) }
				-
				ha:Change name (refdes)             { action=AttributeDialog(object, name); tip={Change the name attribute of a group (refdes)} }
				ha:Change devmap                    { action=QuickAttr(object, devmap); tip={Change the devmap attribute of a symbol (device+footprint+pinout mapping) } }
				-
				ha:update symbol from ext. lib               { }
				ha:move symbol to local lib ref.             { action=SymLocLib(object, toref); }
				ha:convert from local lib ref. to embedded   { action=SymLocLib(object, togrp); }
			}
		}

		ha:popup-obj-terminal {
			li:submenu {
				ha:edit terminal properties...        { action=propedit(object) }
				ha:edit terminal attributes...        { action=AttributeDialog(last-click) }
				ha:edit terminal pens...              { action=pendialog(object, non-modal) }
				ha:terminal in tree view...           { action=TreeDialog(object) }
				ha:set terminal name                  { action={message(ERROR, not yet implemented)}; }
			}
		}

		ha:popup-obj-wire-net {
			li:submenu {
				ha:edit wirenet properties...       { action=propedit(parent) }
				ha:edit net attributes...           { action=AttributeDialog(parent) }
				ha:wirenet object in tree view...   { action=TreeDialog(object) }
			}
		}

		ha:popup-obj-user-grp-unknown {
			li:submenu {
				ha:edit group properties...         { action=propedit(object) }
				ha:edit group attributes...         { action=AttributeDialog(last-click) }
				ha:edit group pens...               { action=pendialog(object, non-modal) }
				ha:group in tree view...            { action=TreeDialog(object) }
			}
		}
	} #popups

	ha:scripts {
		# set all visibility related conf nodes (view/mode/drawing in the menu)
		# to safe defaults that makes sure everything is visible
		mode_reset {
#			conf(set, editor/wireframe_draw, 0, design);
#			conf(set, editor/thin_draw, 0, design);
#			conf(set, editor/thin_draw_poly, 0, design);
			Display(Redraw);
		}

		# set all visibility related states to safe defaults that makes sure
		# everything is visible
		li:view_reset {
			sy:mdr = {/scripts/mode_reset}
#TODO:			LayerVisReset();
			zoom();
		}

		# set all GUI states to safe defaults to get the UI to a known state
		li:gui_reset {
			sy:vr = {/scripts/view_reset}
			fullscreen(off);
			conf(set, editor/draw_grid, 1, design);
			conf(set, editor/local_grid/enable, 0, design);
			Tool(arrow);
#			Display(Redraw);
		}
	} # scripts

} # root
