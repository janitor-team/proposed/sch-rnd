/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#ifndef SCH_RND_OPERATIONS_H
#define SCH_RND_OPERATIONS_H

#include <libcschem/concrete.h>
#include <libcschem/cnc_text.h>

/* Remove first visible object at x;y (typically: click-to-remove tool);
   Returns number of removed objects (0 or 1). */
int sch_rnd_op_remove_xy(csch_sheet_t *sheet, rnd_coord_t x, rnd_coord_t y);

/* Rotate first visible object at x;y (typically: click-to-rotate tool)
   by n steps */
void sch_rnd_op_rotate90_xy(csch_sheet_t *sheet, rnd_coord_t x, rnd_coord_t y, int n);


/* Undoably edit text string; recalculate bounding box if text is
   font-height-specified */
void sch_rnd_op_text_edit(csch_sheet_t *sheet, csch_text_t *text, const char *new_str);

#endif
