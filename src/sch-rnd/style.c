/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <ctype.h>
#include <libcschem/concrete.h>
#include <libcschem/cnc_pen.h>

#include "style.h"

csch_cpen_t *sch_rnd_style_get_pen(csch_sheet_t *sheet, csch_cgrp_t *grp, const rnd_conflist_t *lst, const char **strout)
{
	rnd_conf_listitem_t *ci;
	csch_cpen_t *pen;
	const char *first = NULL;

	for(ci = rnd_conflist_first((rnd_conflist_t *)lst); ci != NULL; ci = rnd_conflist_next(ci)) {
		const char *pn = ci->val.string[0];

		if (first == NULL)
			first = pn;

		if ((pn != NULL) && (*pn != '\0')) {
			pen = csch_pen_get(sheet, grp, pn);
			if (pen != NULL) {
				if (strout != NULL)
					*strout = pn;
				return pen;
			}
		}
	}

	if (strout != NULL)
		*strout = first;
	return &csch_pen_default_unknown;
}

