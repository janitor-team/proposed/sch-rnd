#ifndef SCH_RND_DRAW_H
#define SCH_RND_DRAW_H

#include <libcschem/concrete.h>

/* coordinate <-> "pixel" conversion */
#define C2P(coord) ((rnd_coord_t)((coord) << 10))
#define P2C(coord) ((csch_coord_t)((coord) >> 10))

struct rnd_xform_s {
	unsigned faded:1;
	unsigned use_local_vis:1;
	unsigned no_render_select:1;     /* ignore selection (don't render selected color override) */
	unsigned no_render_hilight:1;    /* ignore object highlight (don't render obj->hilight color override) */
	int local_vis[CSCH_DSPLY_max];
	csch_cgrp_t *fallback_pen; /* if not NULL, use this group for final pen fallback before using invalid pen */
};

/* redraw the entire screen */
void sch_rnd_redraw(rnd_design_t *target);

void sch_rnd_expose_main(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller);
void sch_rnd_expose_preview(rnd_hid_t *hid, rnd_hid_expose_ctx_t *e);


/*** calls useful in preview ***/
void sch_rnd_draw_sheet(csch_sheet_t *sheet, rnd_hid_gc_t gc, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller);


#endif
