/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <librnd/core/config.h>
#include <librnd/core/error.h>
#include "config.h"
#include "conf_core.h"

conf_core_t conf_core;

int sch_rnd_conf_dont_merge_node(const char *path, void *lhtn, int role, int default_prio, int default_policy, rnd_conf_native_t *target)
{
	if ((strncmp(path, "prj/", 4) == 0) && (role != RND_CFR_PROJECT)) {
		rnd_message(RND_MSG_ERROR, "ERROR: ignoring conf node %s on role %s\nMust be in the project file\n(Not accepted from any other config)\n", path, rnd_conf_role_name(role));
		return 1;
	}
	if ((strcmp(path, "rc/font_dirs") == 0) && ((role == RND_CFR_DESIGN) || (role == RND_CFR_PROJECT))) {
		rnd_message(RND_MSG_ERROR, "ERROR: ignoring conf node %s on role %s\nMust be in user or system config\n(Not accepted from design or project)\n", path, rnd_conf_role_name(role));
		/* Rationale:
		   Fonts are mapped on startup, before we read design or projects.
		   Fonts are not meant to be static or fixed in any way, they are
		   not to be shipped with the project. */
		return 1;
	}
	if (strcmp(path, "compile/views") == 0) {
		if ((role == RND_CFR_DESIGN) || (role == RND_CFR_CLI)) {
			rnd_message(RND_MSG_ERROR, "ERROR: ignoring conf node %s on role %s\nMust be in the project file, user or system config\n(Not accepted from design or cli)\n", path, rnd_conf_role_name(role));
			return 1;
		}
	}
	if (strcmp(path, "editor/autocpmp") == 0) {
		if ((role > RND_CFR_USER) && (role != RND_CFR_CLI)) {
			rnd_message(RND_MSG_ERROR, "ERROR: ignoring conf node %s on role %s\nMust be in the user, system or CLI config\n", path, rnd_conf_role_name(role));
			return 1;
		}
	}

	return 0;
}


void sch_rnd_conf_core_postproc(void)
{
	rnd_conf_force_set_str(conf_core.rc.path.prefix, SCH_PREFIX);   rnd_conf_ro("rc/path/prefix");
	rnd_conf_force_set_str(conf_core.rc.path.lib, SCHLIBDIR);       rnd_conf_ro("rc/path/lib");
	rnd_conf_force_set_str(conf_core.rc.path.bin, BINDIR);          rnd_conf_ro("rc/path/bin");
	rnd_conf_force_set_str(conf_core.rc.path.share, SCHSHAREDIR);   rnd_conf_ro("rc/path/share");
}


void conf_core_init()
{
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(conf_core, field,isarray,type_name,cpath,cname,desc,flags);
#include "conf_core_fields.h"
	sch_rnd_conf_core_postproc();
}
