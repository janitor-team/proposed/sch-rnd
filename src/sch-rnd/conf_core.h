#ifndef SCH_RND_CONF_CORE_H
#define SCH_RND_CONF_CORE_H

#include <librnd/core/conf.h>
#include <librnd/core/color.h>

typedef struct {
	const struct rc {
			RND_CFT_REAL file_changed_interval;    /* how often to check if the file has changed on the disk (in seconds); 0 or negative means no check at all */
			RND_CFT_INTEGER backup_interval;       /* time between two backups in seconds; 0 means disabled (no backups) */
			RND_CFT_LIST library_search_paths;     /* ordered list of paths that are each recursively searched for symbol files */

			RND_CFT_STRING emergency_name;         /* file name template for emergency save anonymous .sch files (when sch-rnd crashes); optional field: first %ld --> pid, second %ld --> sheet uid; must be shorter than 240 characters. Don't do emergency save if this item is empty. */
			RND_CFT_STRING backup_name;            /* file name template for periodic backup of board files; optional fields (the usual % substitutions work) */

			RND_CFT_BOOLEAN keep_save_backups;     /* TODO: a copy is made before a save operation overwrites an existing file; if this setting is true, keep the copy even after a successful save */

			RND_CFT_LIST default_sheet_file;       /* ordered list of paths to load default sheet from when a new sheet needs to be created */

			RND_CFT_LIST font_dirs;                /* Unordered list of directories to pick up Ringdove fonts from */

			RND_CFT_BOOLEAN silently_create_on_load; /* TODO: do not generate an error message if the board does not exist on load from command line argument, silently create it in memory */

		struct debug {
			RND_CFT_BOOLEAN draw_text_xform;       /* draw arrows representing xform matrix of text object's parent */
			RND_CFT_BOOLEAN draw_arc_bbox;         /* draw an 1 pixel line on every arc object bounding box */
		} debug;

		/***** automatically set (in postproc) *****/
		struct {
			RND_CFT_STRING prefix;               /* e.g. /usr/local */
			RND_CFT_STRING lib;                  /* e.g. /usr/lib/sch-rnd */
			RND_CFT_STRING bin;                  /* e.g. /usr/bin */
			RND_CFT_STRING share;                /* e.g. /usr/share/sch-rnd */
			RND_CFT_STRING fontdir;              /* rnd fonts are searched in these dirs */

			RND_CFT_STRING design;               /* directory path of the current design, or <invalid> if the current design doesn't have a file name yet */
		} path;

	} rc;
	const struct editor {
		RND_CFT_INTEGER click_time;            /* default time for click expiration, in ms */
		RND_CFT_INTEGER edit_time;             /* default time to wait in dialog boxes after declaring editing finished and auto-applying changes, in ms */
		RND_CFT_INTEGER autocomp_time;         /* default time to wait after last change before running autocompile, in ms; <=0 means immediate; shall be set in the user config (not accepted from project or design) */
		RND_CFT_BOOLEAN autocomp;              /* whether enable autocomp on startup; shall be set in the user config (not accepted from project or design)  */
		RND_CFT_BOOLEAN save_in_tmp;           /* emergency save unsaved sheet data (despite the user clicks don't save) when user quits sch-rnd. Does not affect the on-crash emergency save. */
		RND_CFT_BOOLEAN line_refraction;
		RND_CFT_BOOLEAN line_cont;
		RND_CFT_BOOLEAN lock_floaters;         /* lock down floaters so they can not be moved or selected */
		RND_CFT_BOOLEAN only_floaters;         /* lock down everything else but floaters so only floater objects can be moved or selected */
		RND_CFT_BOOLEAN paste_to_local_lib;    /* automatically paste library entries from buffer to sheet-local library and use references from the sheet instead of heavy copies */
		RND_CFT_INTEGER buffer_number;         /* index of the current buffer */
		struct {
			RND_CFT_BOOLEAN disable_negative;    /* selection box behaviour: disable the negative-direction selection - any selection box will select only what's fully within the box */
			RND_CFT_BOOLEAN symmetric_negative;  /* selection box behaviour: when set, the selection direction is considered negative only if the box has negative size in the X direction */
		} selection;
		struct {
			RND_CFT_LIST tool_circle_stroke;         /* pen preference for the circle tool; pick the first existing pen or if none existing the first item */
			RND_CFT_LIST tool_line_stroke;           /* pen preference for the line tool; pick the first existing pen or if none existing the first item */
			RND_CFT_LIST tool_text_stroke;           /* pen preference for the text tool; pick the first existing pen or if none existing the first item */
			RND_CFT_LIST tool_wire_stroke;           /* pen preference for the wire tool; pick the first existing pen or if none existing the first item */


			RND_CFT_LIST tool_rect_stroke;           /* pen preference for the rectangle tool; pick the first existing pen or if none existing the first item; used only if tool_rect_has_pen is true */
			RND_CFT_BOOLEAN tool_rect_has_stroke;    /* whether to stroke (use the pen to draw the outline) */
			RND_CFT_BOOLEAN tool_rect_has_fill;      /* whether to fill (draw a filled polyogn using the fill color) */
			RND_CFT_LIST tool_rect_fill;             /* polygon fill color; used only if tool_rect_has_fill is true */
		} style;
		struct {
			RND_CFT_STRING family;                   /* if pen font is not found, search for a fallbakc font using this font family */
			RND_CFT_STRING style;                    /* if pen font is not found, search for a fallbakc font using this font style */
		} default_font;
	} editor;
	const struct appearance {
		struct color {
			RND_CFT_COLOR attached;
			RND_CFT_COLOR selected_stroke;
			RND_CFT_COLOR selected_fill;
			RND_CFT_COLOR connection_good;
			RND_CFT_COLOR connection_bad;
			RND_CFT_COLOR hilight_stroke;
			RND_CFT_COLOR symbol_meta_embed;         /* symbol meta bounding box and info text draw color for group objects */
			RND_CFT_COLOR symbol_meta_loclib;        /* symbol meta bounding box and info text draw color for group refs to local lib */
		} color;
	} appearance;
	const struct compile {
		RND_CFT_BOOLEAN multiport_net_merge; /* when multiple ports with the same component:port name connect to different nets: if true, allow merging those nets, else throw an error */
		RND_CFT_HLIST views;
	} compile;
	const struct prj {
		RND_CFT_LIST root_sheets;                /* when non-empty, it lists all root sheets that are part of the project; each root sheet is the root of a hierarchy (may be a single sheet hierarchy, tho) */
		RND_CFT_LIST aux_sheets;                 /* when non-empty, it lists all non-root sheets that can be referenced by root sheets or other aux sheets in a hierarchy */
	} prj;
} conf_core_t;


void conf_core_init();
void sch_rnd_conf_core_postproc(void);

extern conf_core_t conf_core;


int sch_rnd_conf_dont_merge_node(const char *path, void *lhtn, int role, int default_prio, int default_policy, rnd_conf_native_t *target);

#endif
