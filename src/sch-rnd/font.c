/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Font selection */

#include <libcschem/config.h>
#include <stdlib.h>

#include <libcschem/cnc_pen.h>
#include <libcschem/cnc_text.h>
#include <libcschem/rotate.h>

#include <genvector/gds_char.h>
#include <genvector/vts0.h>
#include <librnd/core/safe_fs.h>
#include <librnd/core/rnd_conf.h>
#include <librnd/core/conf.h>
#include <librnd/hid/hid.h>
#include <librnd/core/compat_misc.h>
#include <librnd/hid/pixmap.h>
#include <librnd/core/paths.h>
#include <librnd/src_3rd/genregex/regex_sei.h>

#include "conf_core.h"
#include "draw.h"

#include "font.h"

long sch_rnd_font_score_debug;

#include "font_rnd.c"

void sch_rnd_font_text_update(csch_sheet_t *sheet, csch_text_t *t, csch_cpen_t *pen)
{
	rf_text_update(sheet, t, pen);
}

void sch_rnd_font_text_render(csch_sheet_t *sheet, rnd_hid_gc_t gc, csch_text_t *t, csch_cpen_t *pen)
{
	rf_text_render(sheet, gc, t, pen);
}

static void text_inval_font(csch_sheet_t *sheet, csch_text_t *text)
{
	rf_inval_font(sheet, text);
}

static void text_calc_bbox(csch_sheet_t *sheet, csch_text_t *text)
{
	csch_cpen_t *pen = csch_stroke_fallback_(&text->hdr, NULL);
	sch_rnd_font_text_update(sheet, text, pen);
}

static void pen_inval_font(csch_sheet_t *sheet, csch_cpen_t *pen)
{
	pen->font_handle_valid = 0;
}

void *sch_rnd_font_lookup(const char *name, const char *style)
{
	return rf_font_lookup(NULL, name, style);
}


void sch_rnd_font_init2(void)
{
	sch_rnd_font_rnd_init2();

	csch_cb_text_calc_bbox = text_calc_bbox;
	csch_cb_text_invalidate_font = text_inval_font;
	csch_cb_pen_invalidate_font = pen_inval_font;
}

void sch_rnd_font_uninit(void)
{
	sch_rnd_font_rnd_uninit();

	csch_cb_text_calc_bbox = NULL;
	csch_cb_text_invalidate_font = NULL;
	csch_cb_pen_invalidate_font = NULL;
}
