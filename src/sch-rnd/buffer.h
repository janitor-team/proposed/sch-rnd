#ifndef SCH_RND_BUFFER_H
#define SCH_RND_BUFFER_H

#include <libcschem/concrete.h>

#define SCH_RND_BUFFER_MAX 5
#define SCH_RND_BUFFER_SCRATCH (SCH_RND_BUFFER_MAX-1)

extern csch_sheet_t *sch_rnd_buffer[SCH_RND_BUFFER_MAX];
extern csch_project_t sch_rnd_buffer_prj;

#define SCH_RND_PASTEBUFFER (sch_rnd_buffer[conf_core.editor.buffer_number % SCH_RND_BUFFER_MAX])

void sch_rnd_buffer_paste(csch_sheet_t *sheet, csch_sheet_t *buffer, csch_coord_t dx, csch_coord_t dy);
int sch_rnd_buffer_clear(csch_sheet_t *buffer);

#endif