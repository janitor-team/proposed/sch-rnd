/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <librnd/hid/hid.h>
#include <libcschem/config.h>
#include <libcschem/operation.h>
#include <libcschem/cnc_text.h>
#include <libcschem/cnc_obj.h>

#include "draw.h"
#include "search.h"
#include "font.h"

#include "operation.h"

static int remove_obj(csch_sheet_t *sheet, csch_chdr_t *obj)
{
	obj = csch_cobj_first_unlocked(obj);
	if (obj != NULL)
		csch_op_remove(sheet, obj);
	return 1;
}

int sch_rnd_op_remove_xy(csch_sheet_t *sheet, rnd_coord_t x, rnd_coord_t y)
{
	csch_chdr_t *obj;

	obj = sch_rnd_search_obj_at(sheet, x, y, sch_rnd_slop);
	if (obj == NULL)
		return 0;

	return remove_obj(sheet, obj);
}

static void rotate90_obj(csch_sheet_t *sheet, csch_chdr_t *obj, rnd_coord_t x, rnd_coord_t y, int n)
{
	obj = csch_cobj_first_unlocked(obj);
	if (obj != NULL)
		csch_rotate90(sheet, obj, x, y, n, 1);
}


void sch_rnd_op_rotate90_xy(csch_sheet_t *sheet, rnd_coord_t x, rnd_coord_t y, int n)
{
	csch_chdr_t *obj = sch_rnd_search_obj_at(sheet, x, y, sch_rnd_slop);
	if (obj != NULL)
		rotate90_obj(sheet, obj, x, y, n);
}

void sch_rnd_op_text_edit(csch_sheet_t *sheet, csch_text_t *text, const char *new_str)
{
	int has_bb = text->has_bbox;
	text->has_bbox = 0;
	csch_text_modify(sheet, text, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &new_str, NULL, NULL, 1, 0);
	if (!has_bb) { /* height-defined font: recalculate bbox */
		csch_cobj_update_pen(&text->hdr);
		sch_rnd_font_text_update(sheet, text, text->hdr.stroke);
	}
	text->has_bbox = has_bb;
}

