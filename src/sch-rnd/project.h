void sch_rnd_prj_postproc(csch_project_t *prj);

/* Asks the user and creates the project file for sheet. Returns 0
   if created, -1 if not (cancel or error; errors are reported in
   the message log) */
int sch_rnd_project_create_file_for_sheet_gui(csch_sheet_t *sheet);

/* Create a new empty view and append it in the current project config;
   creates project file if needed */
int sch_rnd_project_append_view(csch_sheet_t *sheet, const char *view_name, int silent);

/* Remove the idxth view from the current project config;
   creates project file if needed */
int sch_rnd_project_del_view(csch_sheet_t *sheet, long idx, int silent);

/* Merge the engine list and save the project file */
void sch_rnd_project_views_save(csch_sheet_t *sheet);
