/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/cnc_text.h>

/* Update the pixmap cache (and recalculate bbox for height-defined font) */
void sch_rnd_font_text_update(csch_sheet_t *sheet, csch_text_t *t, csch_cpen_t *pen);

/* render text object (assume gc and drawing code is already prepared) */
void sch_rnd_font_text_render(csch_sheet_t *sheet, rnd_hid_gc_t gc, csch_text_t *t, csch_cpen_t *pen);


/* Look up best match for name/style or at least return a safe fallback font */
void *sch_rnd_font_lookup(const char *name, const char *style);


void sch_rnd_font_init2(void);
void sch_rnd_font_uninit(void);
