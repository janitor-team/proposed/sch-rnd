#ifndef CSCH_CROSSHAIR_H
#define CSCH_CROSSHAIR_H 1

#include <libcschem/config.h>

#include <librnd/core/hidlib.h>

extern rnd_coord_t sch_rnd_crosshair_x, sch_rnd_crosshair_y; /* screen coords */
extern rnd_hid_gc_t sch_rnd_crosshair_gc;

void sch_rnd_notify_crosshair_change(rnd_design_t *hl, rnd_bool changes_complete);

void sch_rnd_crosshair_gui_init(void);
void sch_rnd_crosshair_gui_uninit(void);

void sch_rnd_hidlib_crosshair_move_to(rnd_design_t *hl, rnd_coord_t abs_x, rnd_coord_t abs_y, int mouse_mot);
void sch_rnd_draw_attached(rnd_design_t *hidlib, rnd_bool inhibit_drawing_mode);

/* Same as rnd_hid_get_coords(), except it works with crosshair coords,
   converted to csch_coord_t */
int sch_rnd_get_coords(const char *msg, csch_coord_t *x, csch_coord_t *y, int force);


#endif
