#include <sch-rnd/draw.h>

/* Used as the appspec arg (except when a single sheet is exported) */
typedef struct sch_rnd_export_appspec_s {
	unsigned exp_prj:1;          /* export a whole project when 1 */
	const char *fn_page_suffix;  /* if not NULL, append page suffix to file name */
} sch_rnd_export_appspec_t;

/* Appspec with default values for the case NULL is passed to an exporter */
extern sch_rnd_export_appspec_t sch_rnd_no_appspec;

void sch_rnd_set_export_layers(rnd_xform_t *xform, const char *layers);

/* Call the libcschem plug_io export mechanism to export a whole project's
   abstract model; If not called on a specific sheet, sheet can be NULL.
   Explicit name is either NULL or the output file name specified by the user. */
int sch_rnd_export_prj_abst(csch_project_t *prj, csch_sheet_t *sheet, int viewid, const char *exp_fmt, const char *explicit_name);

/* An export plugin can call this to export the whole project or the sheet
   (depending on appspec). Project export may call exp_sheet() multiple times.
   Returns 0 if all went well. */
int sch_rnd_export_project_or_sheet(rnd_hid_t *hid, rnd_design_t *design, rnd_hid_attr_val_t *options, sch_rnd_export_appspec_t *appspec, int (*exp_sheet)(rnd_hid_t *, rnd_design_t *, rnd_hid_attr_val_t *, sch_rnd_export_appspec_t *, int *ovr));


/* Returns whether appspec is for a project export; NULL may be passed */
RND_INLINE int sch_rnd_export_appspec_prj(sch_rnd_export_appspec_t *appspec)
{
	return (appspec != NULL) && appspec->exp_prj;
}
