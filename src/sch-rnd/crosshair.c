/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <stdlib.h>

#include <librnd/core/event.h>
#include <librnd/core/rnd_conf.h>
#include <librnd/hid/hid_inlines.h>
#include <librnd/hid/grid.h>
#include <librnd/hid/hid.h>
#include <librnd/hid/tool.h>
#include <librnd/core/actions.h>

#include "conf_core.h"

#include "draw.h"
#include "crosshair.h"

rnd_hid_gc_t sch_rnd_crosshair_gc;
rnd_coord_t sch_rnd_crosshair_x, sch_rnd_crosshair_y; /* screen coords */

void sch_rnd_hidlib_crosshair_move_to(rnd_design_t *hl, rnd_coord_t abs_x, rnd_coord_t abs_y, int mouse_mot)
{
	/* grid fit */
	abs_x = rnd_grid_fit(abs_x, hl->grid, hl->grid_ox);
	abs_y = rnd_grid_fit(abs_y, hl->grid, hl->grid_oy);

	if ((abs_x == sch_rnd_crosshair_x) && (abs_y == sch_rnd_crosshair_y))
		return; /* no change, don't waste CPU */

	/* update the GUI, emit events */
	rnd_hid_notify_crosshair_change(hl, rnd_false);

	sch_rnd_crosshair_x = abs_x;
	sch_rnd_crosshair_y = abs_y;

	rnd_gui->set_crosshair(rnd_gui, abs_x, abs_y, 0);
	rnd_tool_adjust_attached(hl);
	rnd_hid_notify_crosshair_change(hl, rnd_true);

}

void sch_rnd_draw_attached(rnd_design_t *hidlib, rnd_bool inhibit_drawing_mode)
{
	if (!inhibit_drawing_mode) {
		rnd_gui->set_drawing_mode(rnd_gui, RND_HID_COMP_RESET, 1, NULL);
		rnd_gui->set_drawing_mode(rnd_gui, RND_HID_COMP_POSITIVE_XOR, 1, NULL);
	}

	rnd_gui->set_color(sch_rnd_crosshair_gc, &conf_core.appearance.color.attached);
	rnd_hid_set_line_width(sch_rnd_crosshair_gc, -1);
	rnd_hid_set_line_cap(sch_rnd_crosshair_gc, rnd_cap_round);
	rnd_tool_draw_attached(hidlib);

	if (!inhibit_drawing_mode)
		rnd_gui->set_drawing_mode(rnd_gui, RND_HID_COMP_FLUSH, 1, NULL);
}

void sch_rnd_notify_crosshair_change(rnd_design_t *hl, rnd_bool changes_complete)
{
	rnd_hid_notify_crosshair_change(hl, changes_complete);
}

void sch_rnd_crosshair_gui_init(void)
{
	sch_rnd_crosshair_gc = rnd_hid_make_gc();
	rnd_hid_set_draw_xor(sch_rnd_crosshair_gc, 1);

}

void sch_rnd_crosshair_gui_uninit(void)
{
	rnd_hid_destroy_gc(sch_rnd_crosshair_gc);
}

int sch_rnd_get_coords(const char *msg, csch_coord_t *x, csch_coord_t *y, int force)
{
	rnd_coord_t qx, qy;
	int res;

	res = rnd_hid_get_coords(msg, &qx, &qy, force);
	if (res != 0)
		return res;

	*x = P2C(sch_rnd_crosshair_x);
	*y = P2C(sch_rnd_crosshair_y);

	return 0;
}

