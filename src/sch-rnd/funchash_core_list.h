/*
	Central list of function IDs
	The core and core plugins use these from a single, central hash
	This list is used to cpp-generate the F_* constants in enum pcb_function_id_t
*/

action_entry(All)
action_entry(Any)
action_entry(Auto)
action_entry(Buffer)
action_entry(Global)
action_entry(Group)
action_entry(Horizontal)
action_entry(Invert)
action_entry(Idpath)
action_entry(List)
action_entry(Lock)
action_entry(Object)
action_entry(Objarr)
action_entry(Only)
action_entry(Poly)
action_entry(Polygon)
action_entry(Project)
action_entry(Selected)
action_entry(Sheet)
action_entry(Sym)
action_entry(Symbol)
action_entry(Term)
action_entry(Terminal)
action_entry(Vertical)
