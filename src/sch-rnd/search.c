/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2021 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/hid/hid.h>

#include <libcschem/concrete.h>
#include <libcschem/operation.h>
#include <libcschem/cnc_obj.h>
#include <libcschem/search.h>

#include "conf_core.h"
#include "draw.h"

#include "search.h"

static int search_obj_at_filter(csch_search_obj_first_t *ctx, csch_chdr_t *obj)
{
	if (conf_core.editor.lock_floaters && obj->floater)
		return 1;
	if (conf_core.editor.only_floaters && !obj->floater)
		return 1;

	return 0;
}

/* also ignore selected */
static int search_obj_at_filter_unsel(csch_search_obj_first_t *ctx, csch_chdr_t *obj)
{
	return (search_obj_at_filter(ctx, obj) || obj->selected);
}


/* Return the preferred visible object at x;y or NULL if nothing found */
csch_chdr_t *sch_rnd_search_obj_at(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y, csch_coord_t radius)
{
	csch_rtree_box_t box;

	box.x1 = x - radius; box.y1 = y - radius;
	box.x2 = x + radius; box.y2 = y + radius;

	return csch_search_first_gui_filter(sheet, &box, search_obj_at_filter);
}

csch_chdr_t *sch_rnd_search_obj_at_unsel(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y, csch_coord_t radius)
{
	csch_rtree_box_t box;

	box.x1 = x - radius; box.y1 = y - radius;
	box.x2 = x + radius; box.y2 = y + radius;

	return csch_search_first_gui_filter(sheet, &box, search_obj_at_filter_unsel);
}

csch_chdr_t *sch_rnd_search_first_gui_inspect(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y)
{
	csch_rtree_box_t q;
	q.x1 = P2C(x)-sch_rnd_slop; q.x2 = P2C(x)+sch_rnd_slop;
	q.y1 = P2C(y)-sch_rnd_slop; q.y2 = P2C(y)+sch_rnd_slop;

	return csch_search_first_gui_inspect_filter(sheet, &q, search_obj_at_filter);
}


void sch_rnd_search_obj_box(csch_sheet_t *sheet, vtp0_t *res, csch_rtree_box_t *box)
{
	int n;

	for(n = 0; n < CSCH_DSPLY_max; n++) {
		csch_chdr_t *o;
		csch_rtree_it_t it;

		if (!csch_layer_vis[n])
			continue;

		/* prefere/find unlocked object first */
		for(o = csch_rtree_first(&it, &sheet->dsply[n], box); o != NULL; o = csch_rtree_next(&it)) {
			if (csch_cobj_is_locked(o))
				continue;
			if (csch_isc_with_box(o, box))
				vtp0_append(res, o);
		}


		/* fall back to parent of locked objects (e.g. clicked a group-locked
		   object, return group) */
		for(o = csch_rtree_first(&it, &sheet->dsply[n], box); o != NULL; o = csch_rtree_next(&it)) {
			csch_chdr_t *cand = csch_cobj_first_unlocked(o);
			if ((cand != NULL) && csch_isc_with_box(cand, box))
				vtp0_append(res, cand);
		}

	}
}
