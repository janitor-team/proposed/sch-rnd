/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_LIBCSHEM_H
#define CSCH_LIBCSHEM_H

#include <libminuid/libminuid.h>
#include <librnd/core/unit.h>

typedef enum {
	CSCH_PRI_HARDWIRED          = 0,
	CSCH_PRI_USER_GRPREF_HIGH   = 1,
	CSCH_PRI_USER_GRP_HIGH      = 101,
	CSCH_PRI_USER_GRPREF_NORMAL = 201,
	CSCH_PRI_USER_GRP_NORMAL    = 301,
	CSCH_PRI_USER_GRPREF_LOW    = 401,
	CSCH_PRI_USER_GRP_LOW       = 501,
	CSCH_PRI_PLUGIN_HIGH        = 1001,
	CSCH_PRI_PLUGIN_NORMAL      = 11001,
	CSCH_PRI_PLUGIN_LOW         = 21001,
	CSCH_PRI_USER_LOW           = 31001,

	/* maximum values */
	CSCH_PRIMAX_PLUGIN          = 1000  /* can not have more plugins than this, because of priority overlaps */
} csch_pri_t;

int csch_init(void);
void csch_uninit(void);

/* Insert all core actions to the global action hash */
int csch_init_actions(void);

/* Insert cschem-specific types; call after rnd_hidlib_init2() */
int csch_init_units(void);

/* cschem-unit bits */
extern unsigned long csch_unit_k_allow, csch_unit_family_cschem;

extern rnd_unit_t *csch_unit_k;    /* the standard cschem 'k' suffix unit (csch_coord_t * 1000) */
extern rnd_unit_t *csch_unit_base; /* csch_coord_t, no suffix */

/* when multiple ports with the same component:port name connect to different
   nets: if true, allow merging those nets, else throw an error */
extern const int *csch_cfg_multiport_net_merge;

#define CSCH_CFG_(name, defval) (name == NULL ? defval : *name)
#define CSCH_CFG(name, defval) CSCH_CFG_((csch_cfg_ ## name), (defval))

extern minuid_session_t csch_minuid;

#endif
