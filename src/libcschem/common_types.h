#ifndef CSCH_COMMON_TYPES_H
#define CSCH_COMMON_TYPES_H

#include "libcschem/config.h"

typedef struct csch_chdr_s  csch_chdr_t;

typedef csch_sint32_t csch_oid_t;
#define CSCH_INVALID_OID ((csch_oid_t)(0))
#define CSCH_MAX_OID ((csch_oid_t)0x7FFFFFFF)

#define CSCH_COORD_MIN ((csch_coord_t)(-((1UL<<31)-1)))
#define CSCH_COORD_MAX ((csch_coord_t)(+((1UL<<31)-1)))
#define CSCH_COORD_INV CSCH_COORD_MAX

/* from concrete.h */
typedef struct csch_cpen_s csch_cpen_t;
typedef struct csch_sheet_s csch_sheet_t;
typedef struct csch_cgrp_s csch_cgrp_t;

/* from abstract.h */
typedef struct csch_abstract_s csch_abstract_t;
typedef struct csch_ahdr_s csch_ahdr_t;

/* project/view */
typedef struct csch_project_s csch_project_t;
typedef struct csch_view_s csch_view_t;
typedef struct csch_view_eng_s csch_view_eng_t;


#endif
