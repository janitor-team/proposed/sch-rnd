#include <librnd/core/hidlib.h>
#include <librnd/hid/hid_attrib.h>
#include <libcschem/project.h>
#include <libcschem/concrete.h>

#define CSCH_PATH_MAX 8192

/* Derive path name for get_export_options() */
void csch_derive_default_filename(rnd_design_t *dsg, int exp_prj, rnd_hid_attr_val_t *filename_attrib, const char *suffix);

/* Allocate a new output file name; use explicit if available or construct
   the name from hl's filename replacing it's "extension". Optional
   base_suffix and/or base_prefix are prepended and/or appended to the base
   file (basename without ext) name after it is generated. */
char *cschem_export_filename(rnd_design_t *hl, const char *explicit, const char *base_prefix, const char *base_suffix, const char *ext);

/* Compile abstract model for export if view_name is neither NULL nor empty.
   Cookie is an opaque pointer-storage provided by the caller. Call _post after
   the export. */
int cschem_export_compile_pre(csch_project_t *prj, const char *view_name, void **cookie);

/* Restore project state after a compilation-for-export */
void cschem_export_compile_post(csch_project_t *prj, void **cookie);


/*** internal ***/
void csch_sheet_export_name(csch_sheet_t *sheet, int exp_prj, char epath[CSCH_PATH_MAX], long exp_idx);

/* sheet is used if project name is not possible to figure; may be NULL */
void csch_project_export_name(csch_project_t *prj, csch_sheet_t *sheet, char epath[CSCH_PATH_MAX], const char *exp_fmt, const char *view_name);
