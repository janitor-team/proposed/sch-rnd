#ifndef VTCSCH_COUTLINE_H
#define VTCSCH_COUTLINE_H

#include <stdlib.h>
#include <string.h>
#include "libcschem/concrete.h"
#include "libcschem/cnc_arc.h"
#include "libcschem/cnc_line.h"

typedef union csch_coutline_s { /* outline object of a polygon */
	csch_chdr_t hdr;
	csch_line_t line;
	csch_arc_t arc;
} csch_coutline_t;

#define GVT(x) csch_vtcoutline_ ## x

#define GVT_ELEM_TYPE csch_coutline_t
#define GVT_SIZE_TYPE long
#define GVT_DOUBLING_THRS 256
#define GVT_START_SIZE 16
#define GVT_FUNC
#define GVT_SET_NEW_BYTES_TO 0
#include <genvector/genvector_impl.h>
#define GVT_REALLOC(vect, ptr, size)  realloc(ptr, size)
#define GVT_FREE(vect, ptr)           free(ptr)
#include <genvector/genvector_undef.h>
#endif
