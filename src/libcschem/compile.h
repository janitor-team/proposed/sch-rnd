/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_COMPILE_H
#define CSCH_COMPILE_H

#include <libcschem/abstract.h>
#include <libcschem/concrete.h>
#include <libcschem/project.h>

int csch_compile_sheet(csch_abstract_t *dst, int viewid, const csch_sheet_t *src);
int csch_compile_post(csch_project_t *proj, int viewid, csch_abstract_t *dst);

int csch_compile_project(csch_project_t *prj, int viewid, csch_abstract_t *dst, int quiet);


/*** for lib itnernal use ***/
int csch_compile_connect_net_to(csch_anet_t **net, csch_ahdr_t *a, int allow_reconn);

csch_inline void csch_compile_add_source(csch_cgrp_t *src, csch_ahdr_t *abst)
{
	vtl0_append(&src->aid, abst->aid);
	vtp0_append(&abst->srcs, src);
}

#endif
