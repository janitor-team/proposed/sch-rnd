/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_OIDPATH_H
#define CSCH_OIDPATH_H

#include <genvector/gds_char.h>
#include <genlist/gendlist.h>
#include "libcschem/vtoid.h"
#include "libcschem/common_types.h"

typedef struct {
	csch_vtoid_t vt;
	gdl_elem_t link;  /* may be part of an idpath list */
} csch_oidpath_t;

void csch_oidpath_from_obj(csch_oidpath_t *dst, const csch_chdr_t *src);

/* Convert a slash separated list of integers into an oidpath array;
   Normally the path must start with a slash, the relative variant
   allows starting with a number */
int csch_oidpath_parse(csch_oidpath_t *dst, const char *src);
int csch_oidpath_parse_relative(csch_oidpath_t *dst, const char *src);

csch_chdr_t *csch_oidpath_resolve(csch_sheet_t *sheet, const csch_oidpath_t *path);
csch_chdr_t *csch_oidpath_resolve_in(csch_cgrp_t *grp, const csch_oidpath_t *path);
void csch_oidpath_free(csch_oidpath_t *path);

int csch_oidpath_to_str_append(gds_t *dst, const csch_oidpath_t *path);
char *csch_oidpath_to_str(const csch_oidpath_t *path);

/* Returns wheter p1 has the same path as p2; returns 1 on match, 0 on mismatch */
int csch_oidpath_eq(const csch_oidpath_t *p1, const csch_oidpath_t *p2);

csch_inline int csch_oidpath_is_empty(const csch_oidpath_t *path)
{
	return path->vt.used == 0;
}

/* oidpath lists */
#define TDL(x)      csch_oidpath_list_ ## x
#define TDL_LIST_T  csch_oidpath_list_t
#define TDL_ITEM_T  csch_oidpath_t
#define TDL_FIELD   link
#define TDL_SIZE_T  size_t
#define TDL_FUNC

#define csch_oidpath_list_foreach(list, iterator, loop_elem) \
	gdl_foreach_((&((list)->lst)), (iterator), (loop_elem))

#include <genlist/gentdlist_impl.h>
#include <genlist/gentdlist_undef.h>

void csch_oidpath_list_clear(csch_oidpath_list_t *lst);

#endif
