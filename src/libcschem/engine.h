/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#ifndef CSCH_ENGINE_H
#define CSCH_ENGINE_H

#include <librnd/core/config.h>
#include <librnd/core/actions.h>
#include <libcschem/common_types.h>
#include <genht/htss.h>

typedef enum {
	CSCH_ENGHK_TERMINAL_NAME_TO_PORT_NAME,
	CSCH_ENGHK_SYMBOL_NAME_TO_COMPONENT_NAME,
	CSCH_ENGHK_SYMBOL_JOINED_COMPONENT,
	CSCH_ENGHK_COMPILE_COMPONENT1,
	CSCH_ENGHK_COMPILE_COMPONENT2,
	CSCH_ENGHK_COMPILE_PORT,
	CSCH_ENGHK_COMPILE_NET,
	CSCH_ENGHK_max
} csch_eng_hook_t;

struct csch_view_eng_s {
	fgw_obj_t *obj;
	htss_t cfg;

	/* cache */
	fgw_func_t *hook[CSCH_ENGHK_max]; /* look up all hooks once after load so calls don't have to go trhough */
	void *cfg_cache; /* optional: compiled by the engine */
	short int eprio; /* engine priority; final attribute priority should be (CSCH_PRI_PLUGIN_* + eprio) */
	char *options; /* parameter (e.g. script file name) specified on creation */
};

typedef struct csch_hook_call_ctx_s {
	csch_project_t *project;
	const csch_view_eng_t *view_eng;
} csch_hook_call_ctx_t;

csch_view_eng_t *csch_eng_alloc(csch_view_t *view, const char *user_name, const char *eng_name, const char *options);
void csch_eng_free(csch_view_t *view, csch_view_eng_t *eng);

/* temporary until fungw C binding installation is sorted out */
fgw_error_t csch_c_call_script(fgw_arg_t *res, int argc, fgw_arg_t *argv);

/*** calls ***/

/* Call hooks, expect no reply; returns 0 on success, -1 on view or
   argument error */
int csch_eng_call(csch_project_t *proj, int viewid, csch_eng_hook_t hkid, ...);

/* Call hooks to modify a string. Result is in *res after the call and needs
   to be freed using csch_eng_call_strmod() */
void csch_eng_call_strmod(csch_project_t *proj, int viewid, csch_eng_hook_t hook, const char **res, const char *defval, ...);

/* Clean up after csch_eng_call_strmod(). defval must be the same pointer
   that was passed to csch_eng_call_strmod() */
csch_inline void csch_eng_free_strmod(const char **res, const char *defval);


/*** implementation ***/
csch_inline void csch_eng_free_strmod(const char **res, const char *defval)
{
	if (*res != defval) {
		free((char *)(*res));
		*res = NULL;
	}
}

#endif
