#ifndef LIBCSCHEM_UTIL_COMPILE_H
#define LIBCSCHEM_UTIL_COMPILE_H

#include <libcschem/abstract.h>

/* Connect a port to a network. Returns:
   +1: connection is made
    0: already connected (the specific port to the specific net)
   -1: error, no connection made (e.g. netname collision)
   If require_conn is non-zero, throw an error message when there is no
   graphical connection on the port (useful for gnd/vcc/rail symbols).
*/
int csch_cmp_nameconn_port_net(csch_acomp_t *comp, const char *portname, const char *netname, int require_conn);

/* Merge n1 and n2, keeping one of them, making the other empty; return the
   one kept. (Tries to keep the one with name) */
csch_anet_t *csch_cmp_merge_nets(csch_anet_t *n1, csch_anet_t *n2);


#endif
