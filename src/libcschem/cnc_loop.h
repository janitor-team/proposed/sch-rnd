/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018,2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "concrete.h"

/*** API ***/

/* Iterate over sheet objects */
typedef struct csch_grpo_iter_s csch_grpo_iter_t;
csch_inline csch_chdr_t *csch_grpo_first(csch_grpo_iter_t *it, const csch_sheet_t *sheet, const csch_cgrp_t *grp);
csch_inline csch_chdr_t *csch_grpo_next(csch_grpo_iter_t *it);

/*** Implementation ***/

struct csch_grpo_iter_s {
	const csch_sheet_t *sheet;
	const csch_cgrp_t *grp;
	htip_entry_t *e;
};

csch_inline csch_chdr_t *csch_grpo_first(csch_grpo_iter_t *it, const csch_sheet_t *sheet, const csch_cgrp_t *grp)
{
	it->sheet = sheet;
	it->grp = grp;
	it->e = htip_first(&it->grp->id2obj);
	if (it->e == NULL)
		return NULL;

	return it->e->value;
}

csch_inline csch_chdr_t *csch_grpo_next(csch_grpo_iter_t *it)
{
	it->e = htip_next(&it->grp->id2obj, it->e);
	if (it->e == NULL)
		return NULL;

	return it->e->value;
}
