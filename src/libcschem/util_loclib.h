/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Utility functions for maintaining a local (per sheet) library */

#include <libcschem/concrete.h>
#include <libcschem/plug_library.h>

/* Return the group whose purpose matches purpose from under sheet's
   indirect group. If alloc is 1, and the group does not exist, it is
   created using src and *alloced is set to 1.
   Always frees src's fields. */
csch_cgrp_t *csch_loclib_get_root(csch_sheet_t *sheet, csch_lib_master_t *master, csch_source_arg_t *src, int alloc, int *alloced);


/* Resolve library root dir and indirect root group for a local lib master.
   If alloc==1, create it using src when missing. Returns 0 on success. On
   failure there's no root dir in indirect.
   Always frees src's fields. */
int csch_loclib_get_roots(csch_lib_t **root_dir_out, csch_cgrp_t **root_grp_out, csch_lib_master_t *master, csch_sheet_t *sheet, csch_source_arg_t *src, int alloc, int *alloced);
