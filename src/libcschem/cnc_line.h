/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#ifndef CSCH_CONCRETE_LINE_H
#define CSCH_CONCRETE_LINE_H
#include "libcschem/concrete.h"
#include GENGEO2D_TYPECFG
#include "gengeo2d/prim.h"

/* type=CSCH_CTYPE_LINE */

typedef struct csch_line_s {
	csch_chdr_t hdr;
	g2d_sline_t inst; /* as installed: group-transformed version (in absolute sheet coordinates) */
	g2d_cline_t spec; /* as specified: file format version (in relative group coordinates) */
} csch_line_t;

csch_line_t *csch_line_alloc(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_oid_t oid);
csch_line_t *csch_line_dup(csch_sheet_t *sheet, csch_cgrp_t *parent, const csch_line_t *src, int keep_id, int inst2spec);
void csch_line_free(csch_line_t *line);
csch_line_t *csch_line_get(csch_sheet_t *sheet, csch_cgrp_t *grp, csch_oid_t oid);
void csch_line_update(csch_sheet_t *sheet, csch_line_t *line, int do_xform);

unsigned csch_line_hash_(const csch_line_t *line, csch_hash_ignore_t ignore, int in_contour);
unsigned csch_line_hash(const csch_line_t *line, csch_hash_ignore_t ignore);
int csch_line_keyeq_(const csch_line_t *l1, const csch_line_t *l2, csch_hash_ignore_t ignore, int in_contour);
int csch_line_keyeq(const csch_line_t *l1, const csch_line_t *l2, csch_hash_ignore_t ignore);

/* non-standard calls */
void csch_line_update_xform(csch_sheet_t *sheet, csch_line_t *line);
void csch_line_update_bbox(csch_sheet_t *sheet, csch_line_t *line);

/* internal */
int csch_line_get_endxy(const csch_line_t *line, int side, csch_coord_t *x, csch_coord_t *y);

void csch_line_modify(csch_sheet_t *sheet, csch_line_t *line, csch_coord_t *x1, csch_coord_t *y1, csch_coord_t *x2, csch_coord_t *y2, int undoable, int relative);


#endif
