/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "config.h"

#include <genvector/gds_char.h>
#include "project.h"
#include "libcschem.h"
#include <librnd/core/compat_lrealpath.h>
#include <librnd/core/event.h>
#include <librnd/core/compat_fs.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/conf.h>
#include "plug_io.h"
#include "event.h"

static const char project_cookie[] = "libcschem/project";

csch_project_t *csch_project_alloc(void)
{
	csch_project_t *prj;

	prj = calloc(sizeof(csch_project_t), 1);
	/* no need to init fields because of calloc() */

	rnd_conf_load_as(RND_CFR_PROJECT, NULL, 0);

	prj->dummy = 1; /* until it's loaded or saved */
	prj->view_conf_rev = -1;
	return prj;
}

void csch_project_free(csch_project_t *prj)
{
	int n;

	if (prj->abst != NULL) {
		csch_abstract_uninit(prj->abst);
		free(prj->abst);
	}

	for(n = 0; n < vtp0_len(&prj->hdr.designs); n++)
		csch_sheet_free(prj->hdr.designs.array[n]);
	prj->hdr.designs.used = 0; /* rnd_project_uninit() will iterate */
	for(n = 0; n < vtp0_len(&prj->views); n++)
		csch_view_free(prj->views.array[n]);
	vtp0_uninit(&prj->views);

	rnd_project_uninit(&prj->hdr);
	free(prj);
}

csch_view_t *csch_view_create(csch_project_t *prj, const char *name)
{
	csch_view_t *view = calloc(sizeof(csch_view_t), 1);

	fgw_init(&view->fgw_ctx, name);
	view->parent = prj;

	vtp0_append(&prj->views, view);
	return view;
}

void csch_view_free(csch_view_t *view)
{
	long n;

	for(n = 0; n < vtp0_len(&view->engines); n++)
		csch_eng_free(view, view->engines.array[n]);
	vtp0_uninit(&view->engines);

	fgw_uninit(&view->fgw_ctx);
	free(view);
}

void csch_view_remove_all(csch_project_t *prj)
{
	long n;
	for(n = 0; n < prj->views.used; n++)
		csch_view_free(prj->views.array[n]);
	prj->views.used = 0;
}

int csch_view_get_id(csch_project_t *prj, const char *name)
{
	int n;
	for(n = 0; n < prj->views.used; n++) {
		csch_view_t *v = prj->views.array[n];
		if ((v != NULL) && (strcmp(v->fgw_ctx.name, name) == 0))
			return n;
	}
	return -1;
}

csch_view_t *csch_view_get(csch_project_t *prj, const char *name)
{
	int view_id = csch_view_get_id(prj, name);
	if (view_id < 0)
		return NULL;
	return prj->views.array[view_id];
}


static void view_renum(csch_view_t *view)
{
	int n, prio;
	for(n = 0, prio = (view->engines.used - 1) * 20; n < view->engines.used; n++,prio-=20) {
		csch_view_eng_t *eng = view->engines.array[n];
		eng->eprio = prio;
	}
}

int csch_view_eng_append(csch_view_t *view, const char *user_name, const char *eng_name, const char *file_name)
{
	csch_view_eng_t *eng;

	if (view->engines.used >= CSCH_PRIMAX_PLUGIN/2) return -1;

	eng = csch_eng_alloc(view, user_name, eng_name, file_name);
	if (eng == NULL) return -1;

	vtp0_append(&view->engines, eng);
	view_renum(view);
	return 0;
}


int csch_view_eng_insert_before(csch_view_t *view, const char *user_name, const char *eng_name, const char *file_name, int idx)
{
	csch_view_eng_t *eng;

	if (view->engines.used >= CSCH_PRIMAX_PLUGIN) return -1;
	if ((idx < 0) || (idx > view->engines.used)) return -1;

	eng = csch_eng_alloc(view, user_name, eng_name, file_name);
	if (eng == NULL) return -1;

	vtp0_insert_len(&view->engines, idx, (void *)eng, 1);
	view_renum(view);
	return 0;
}


int csch_proj_sheet_update_filename(csch_project_t *prj, csch_sheet_t *sheet)
{
	gds_t tmp;
	char *realfn;

	if (rnd_is_path_abs(sheet->hidlib.loadname)) {
		free(sheet->hidlib.fullpath);
		sheet->hidlib.fullpath = rnd_strdup(sheet->hidlib.loadname);
		rnd_event(&sheet->hidlib, RND_EVENT_DESIGN_FN_CHANGED, NULL);
		return 0;
	}

	if (prj->hdr.fullpath == NULL)
		return -1;
	
	gds_init(&tmp);
	gds_append_str(&tmp, prj->hdr.prjdir);
	gds_append(&tmp, '/');
	gds_append_str(&tmp, sheet->hidlib.loadname);
	realfn = rnd_lrealpath(tmp.array);
	gds_uninit(&tmp);
	if (realfn == NULL)
		return -1;

	free(sheet->hidlib.fullpath);
	sheet->hidlib.fullpath = realfn;
	rnd_event(&sheet->hidlib, RND_EVENT_DESIGN_FN_CHANGED, NULL);
	return 0;
}

int csch_proj_update_filename(csch_project_t *prj)
{
	return rnd_project_update_filename(&prj->hdr);
}

int csch_project_load_sheet(csch_project_t *prj, const char *fn, const char *fmt, csch_sheet_t **sheet_out)
{
	int ain;
	csch_sheet_t *sheet = csch_load_sheet(prj, fn, fmt, &ain);

	if (sheet_out != NULL)
		*sheet_out = sheet;

	if (sheet == NULL) {
		if (fmt != NULL)
			rnd_message(RND_MSG_ERROR, "Failed to load %s (with requested format %s)\n", fn, fmt);
		else
			rnd_message(RND_MSG_ERROR, "Failed to load %s\n", fn);
		return -1;
	}

	if (!ain)
		return rnd_project_append_design(&prj->hdr, &sheet->hidlib);

	return 0;
}

int csch_project_remove_sheet(csch_project_t *prj, csch_sheet_t *sheet)
{
	return rnd_project_remove_design(&prj->hdr, &sheet->hidlib);
}


csch_project_t *csch_load_project_by_sheet_name(const char *sheet_fn, int with_sheets)
{
	const char *try;
	const char *project_fn = rnd_conf_get_project_conf_name(NULL, sheet_fn, &try);
	csch_project_t *prj = csch_load_project(project_fn, "lht", with_sheets);

	if (prj == NULL) {
		rnd_message(RND_MSG_WARNING, "Warning: failed to load project file %s; allocating a dummy project\n", project_fn);
		prj = csch_project_alloc();
	}
	else
		prj->dummy = 0;

	return prj;
}

void csch_project_clean_views(csch_project_t *prj)
{
	long n;
	for(n = 0; n < prj->views.used; n++)
		csch_view_free(prj->views.array[n]);
	prj->views.used = 0;
}

void csch_views_changed(csch_project_t *prj)
{
	long n;

	for(n = 0; n < prj->hdr.designs.used; n++) {
		if (prj->hdr.designs.array[n] != NULL) {
			csch_sheet_t *sheet = prj->hdr.designs.array[n];
			rnd_event(&sheet->hidlib, CSCH_EVENT_PRJ_VIEWS_CHANGED, NULL);
			return;
		}
	}
}

int csch_view_activate(csch_project_t *prj, int view_id)
{
	long n;

	if ((view_id < 0) || (view_id >= prj->views.used))
		return -1;

	if (prj->curr == view_id)
		return 0;

	prj->curr = view_id;

	/* call with the first sheet */
	for(n = 0; n < prj->hdr.designs.used; n++) {
		if (prj->hdr.designs.array[n] != NULL) {
			csch_sheet_t *sheet = prj->hdr.designs.array[n];
			rnd_event(&sheet->hidlib, CSCH_EVENT_PRJ_VIEW_ACTIVATED, NULL);
			return 0;
		}
	}

	return 0;
}


static void csch_project_conf_saved(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	int role = argv[2].d.i;

	if (role == RND_CFR_PROJECT) {
		csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
		csch_project_t *prj = (csch_project_t *)sheet->hidlib.project;

		prj->dummy = 0;
	}
}

void csch_project_init(void)
{
	rnd_event_bind(RND_EVENT_CONF_FILE_SAVE_POST, csch_project_conf_saved, NULL, project_cookie);
}

void csch_project_uninit(void)
{
	rnd_event_unbind_allcookie(project_cookie);
}

