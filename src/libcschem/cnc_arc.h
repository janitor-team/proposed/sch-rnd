/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#ifndef CSCH_CONCRETE_ARC_H
#define CSCH_CONCRETE_ARC_H
#include "libcschem/concrete.h"
#include GENGEO2D_TYPECFG
#include "gengeo2d/prim.h"

/* type=CSCH_CTYPE_ARC */

typedef struct csch_arc_s {
	csch_chdr_t hdr;
	g2d_sarc_t inst; /* as installed: group-transformed version (in absolute sheet coordinates) */
	g2d_carc_t spec; /* as specified: file format version (in relative group coordinates) */
	csch_coord_t spec_sx, spec_sy, spec_ex, spec_ey; /* cached start/end coords */
	unsigned svalid:1;
	unsigned evalid:1;
} csch_arc_t;

csch_arc_t *csch_arc_alloc(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_oid_t oid);
csch_arc_t *csch_arc_dup(csch_sheet_t *sheet, csch_cgrp_t *parent, const csch_arc_t *src, int keep_id);
void csch_arc_free(csch_arc_t *arc);
csch_arc_t *csch_arc_get(csch_sheet_t *sheet, csch_cgrp_t *grp, csch_oid_t oid);
void csch_arc_update(csch_sheet_t *sheet, csch_arc_t *arc, int do_xform);

unsigned csch_arc_hash_(const csch_arc_t *arc, csch_hash_ignore_t ignore, int in_contour);
unsigned csch_arc_hash(const csch_arc_t *arc, csch_hash_ignore_t ignore);
int csch_arc_keyeq_(const csch_arc_t *a1, const csch_arc_t *a2, csch_hash_ignore_t ignore, int in_contour);
int csch_arc_keyeq(const csch_arc_t *a1, const csch_arc_t *a2, csch_hash_ignore_t ignore);

void csch_arc_modify(csch_sheet_t *sheet, csch_arc_t *arc, csch_coord_t *cx, csch_coord_t *cy, csch_coord_t *r, double *start, double *delta, int undoable, int relative);

/* non-standard calls */
void csch_arc_update_xform(csch_sheet_t *sheet, csch_arc_t *arc);
void csch_arc_update_bbox(csch_sheet_t *sheet, csch_arc_t *arc);

/* internal calls */
int csch_arc_get_endxy(const csch_arc_t *arc, int side, csch_coord_t *x, csch_coord_t *y);
void csch_arc_rotate_(csch_arc_t *arc, csch_coord_t rcx, csch_coord_t rcy, double da, double cs, double sn, csch_coord_t *cx_out, csch_coord_t *cy_out, double *start_out);
void csch_arc_rotate90_(csch_arc_t *arc, csch_coord_t rcx, csch_coord_t rcy, int n, csch_coord_t *cx_out, csch_coord_t *cy_out, double *start_out);
void csch_arc_mirror_(csch_arc_t *arc, csch_coord_t mcx, csch_coord_t mcy, int mirx, int miry, csch_coord_t *cx_out, csch_coord_t *cy_out, double *start_out, double *delta_out);


#endif
