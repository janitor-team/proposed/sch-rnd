#include <liblihata/dom.h>
#include <load_cache/load_cache.h>
#include <libcschem/concrete.h>
#include <libcschem/project.h>

lht_doc_t *ldch_lht_get_doc(ldch_file_t *file);
ldch_low_parser_t *ldch_lht_reg_low_parser(ldch_ctx_t *ctx);

int csch_lht_parse_attribs_(csch_sheet_t *sheet, csch_chdr_t *dsth, csch_attribs_t *dsta, lht_node_t *subtree, void (*error)(void *ectx, lht_node_t *n, const char *msg), void *ectx);
int csch_lht_parse_attribs(csch_attribs_t *dst, lht_node_t *subtree, void (*error)(void *ectx, lht_node_t *n, const char *msg), void *ectx);

/* Build a lihata subtree for a view; return NULL on error */
lht_node_t *csch_view2lht(const csch_view_t *view);
