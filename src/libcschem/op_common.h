/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_OP_COMMON_H
#define CSCH_OP_COMMON_H

#include "operation.h"

typedef enum csch_cnc_common_remove_e { /* bitfield flags controlling what the common object removal should remove */
	CSCH_REM_UNDOABLE = 1,                /* allocate undo slot; this is on by default when called from an object's handler */
	CSCH_REM_FROM_RTREE = 2,
	CSCH_REM_FROM_PARENT = 4,
	CSCH_REM_DEL_EMPTY_PARENT = 8
} csch_cnc_common_remove_t;


/* Modular redo implementation for any object */
csch_chdr_t *csch_cnc_common_remove_redo(csch_undo_remove_t *u, csch_cnc_common_remove_t how);
csch_chdr_t *csch_cnc_common_remove_undo(csch_undo_remove_t *u, csch_cnc_common_remove_t how);


/* Allocate an undo slot for removing an object */
csch_undo_remove_t *csch_op_remove_alloc_undo(csch_sheet_t *sheet);

/* Call this (centrally) after a new object has been inserted into sheet; will
   do the undo administration. */
csch_chdr_t *csch_op_inserted(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_chdr_t *obj);

/* either keep orig_oid or allocate a new oid, depending on the keep_id arg */
#define CSCH_KEEP_OID(parent, orig_oid) \
	(keep_id ? orig_oid : csch_oid_new(sheet, parent))

#endif
