#ifndef GENHT_HTPO_H
#define GENHT_HTPO_H

#include "config.h"
#include GENGEO2D_TYPECFG
#include "gengeo2d/prim.h"

typedef struct {
	g2d_coord_t x, y;
} htPo_key_t;

typedef struct {
	void *ptr;
	long i;
	void *wire;
} htPo_value_t;

#define HT(x) htPo_ ## x
#include <genht/ht.h>
#undef HT

int htPo_keyeq(htPo_key_t a, htPo_key_t b);
unsigned int htPo_keyhash(htPo_key_t a);

#endif
