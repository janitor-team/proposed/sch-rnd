/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_UNDO_H
#define CSCH_UNDO_H

#include "concrete.h"

typedef enum csch_undo_ev_e {
	CSCH_UNDO_EV_UNDO,
	CSCH_UNDO_EV_REDO,
	CSCH_UNDO_EV_CLEAR_LIST,
	CSCH_UNDO_EV_TRUNCATE
} csch_undo_ev_t;

int csch_undo(csch_sheet_t *sheet);
int csch_redo(csch_sheet_t *sheet);
void csch_undo_clear_list(csch_sheet_t *sheet, rnd_bool Force);
void csch_undo_inc_serial(csch_sheet_t *sheet);

void csch_undo_act_init(void);
void csch_undo_act_uninit(void);

/* Helper for debugging undo bugs: place a visible mark in the undo list */
#define csch_undo_add_mark(sheet, comment) \
	do { \
		csch_source_arg_t *src = csch_attrib_src_c(NULL, 0, 0, NULL); \
		csch_attr_modify_str(sheet, &sheet->direct, 0, comment, NULL, src, 1); \
	} while(0)


/*** helpers for implementing undo swaps ***/

/* Returns arg if not NULL, else obj->field; field and arg have the same type
   but arg is a pointer. */
#define CSCH_UNDO_MODIFY_ARG(obj, field, arg)  (arg == NULL ? (obj)->field : *arg)
#define CSCH_UNDO_MODIFY(obj, field)           CSCH_UNDO_MODIFY_ARG(obj, field, field)
#define CSCH_UNDO_RMODIFY_ARG(obj, field, arg) (arg == NULL ? (obj)->field : (relative ? (obj)->field + *arg : *arg))
#define CSCH_UNDO_RMODIFY(obj, field)          CSCH_UNDO_RMODIFY_ARG(obj, field, field)

#endif
