/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <librnd/config.h>
#include "event.h"

static const char *csch_evnames[] = {
	"cschev_layervis_changed",
	"cschev_undo_post",
	"cschev_sheet_edit",
	"cschev_sheet_preunload",
	"cschev_sheet_postunload",
	"cschev_sheet_postload",
	"cschev_sheet_postsave",
	"cschev_obj_needs_redraw",
	"cschev_obj_attr_edited",
	"cschev_prj_compiled",
	"cschev_prj_views_changed",
	"cschev_prj_view_activated",
	"cschev_library_changed",
	"cschev_selection_changed",

	"cschev_buffer_copy_custom",
	"cschev_buffer_paste_custom"
};

void csch_event_init_app(void)
{
	rnd_event_app_reg(CSCH_EVENT_last, csch_evnames, sizeof(csch_evnames));
}
