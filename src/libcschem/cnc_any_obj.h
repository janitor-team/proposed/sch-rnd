/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_CNC_ANY_OBJ_H
#define CSCH_CNC_ANY_OBJ_H

#include "concrete.h"

void csch_cobj_free(csch_chdr_t *obj);

/* If inst2spec is true, take target groups transformations and modify dup'd
   object's spec so that it ends up in the same installed coords */
csch_chdr_t *csch_cobj_dup(csch_sheet_t *sheet, csch_cgrp_t *grp, const csch_chdr_t *src, int keep_id, int inst2spec);

void csch_cobj_update(csch_sheet_t *sheet, csch_chdr_t *src, int do_xform);
void csch_cobj_attrib_copy_all(csch_sheet_t *sheet, csch_cgrp_t *dst, const csch_cgrp_t *src);

/* source is free'd */
int csch_cobj_attrib_set(csch_sheet_t *sheet, csch_cgrp_t *obj, int prio, const char *key, const char *val, csch_source_arg_t *source);
int csch_cobj_attrib_seti(csch_sheet_t *sheet, csch_cgrp_t *obj, int prio, const char *key, long idx, const char *val, csch_source_arg_t *source);

unsigned csch_cobj_hash(csch_chdr_t *obj);
unsigned csch_cobj_hash_(csch_chdr_t *obj, csch_hash_ignore_t ignore);
int csch_cobj_keyeq(csch_chdr_t *obj1, csch_chdr_t *obj2);
int csch_cobj_keyeq_(csch_chdr_t *obj1, csch_chdr_t *obj2, csch_hash_ignore_t ignore);


/* Fill in endpoint coords of object in x and y; side: 0 means query start,
   1 means end. Returns 0 on success. Works only on line and arc */
int csch_cobj_get_endxy(const csch_chdr_t *src, int side, csch_coord_t *x, csch_coord_t *y);

/* Returns 1 if obj is in the (in)direct subrtee, else 0 */
int csch_cobj_is_direct(const csch_chdr_t *obj);
int csch_cobj_is_indirect(const csch_chdr_t *obj);

/* Select or unselect object; TODO: undoable? */
void csch_cobj_select(csch_sheet_t *sheet, csch_chdr_t *obj);
void csch_cobj_unselect(csch_sheet_t *sheet, csch_chdr_t *obj);

/* Unselet object and all parent objects */
csch_inline void csch_chdr_unselect_wp(csch_chdr_t *obj)
{
	csch_sheet_t *sheet = obj->sheet;
	for(;obj != NULL; obj = &obj->parent->hdr)
		csch_cobj_unselect(sheet, obj);
}


/* Modify obj's pen name(s) if not NULL; if unodable, add the operation to the
   undo list */
void csch_chdr_pen_name_modify(csch_sheet_t *sheet, csch_chdr_t *obj, csch_comm_str_t *stroke_name, csch_comm_str_t *fill_name, int undoable);

#endif
