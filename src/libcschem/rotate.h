/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_ROTATE_H
#define CSCH_ROTATE_H

#include <librnd/config.h>
#include <librnd/core/compat_misc.h>

csch_inline void csch_rotate90_pt(csch_coord_t *x, csch_coord_t *y, csch_coord_t cx, csch_coord_t cy, int n)
{
	csch_coord_t dx = *x - cx, dy = *y - cy;
	switch(n & 0x3) {
		case 3: *x = cx + dy; *y = cy - dx; break;
		case 2: *x = cx - dx; *y = cy - dy; break;
		case 1: *x = cx - dy; *y = cy + dx; break;
	}
}

csch_inline void csch_rotate_pt(csch_coord_t *x, csch_coord_t *y, csch_coord_t cx, csch_coord_t cy, double cosa, double sina)
{
	csch_coord_t px = *x - cx, py = *y - cy;

	*x = rnd_round(px * cosa + py * sina + cx);
	*y = rnd_round(py * cosa - px * sina + cy);
}

csch_inline void csch_mirror_pt(csch_coord_t *x, csch_coord_t *y, csch_coord_t mcx, csch_coord_t mcy, int mirx, int miry)
{
	if (mirx)
		*x = 2*mcx - *x;
	if (miry)
		*y = 2*mcy - *y;
}

#endif
