/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "config.h"

#include <librnd/config.h>

#include <librnd/core/actions.h>
#include <librnd/core/compat_misc.h>
#include "event.h"
#include "project.h"
#include "compile.h"

#include "project_act.h"

static const char *project_cookie = "project_act";

static const char csch_acts_CompileProject[] = "CompileProject([view_name])";
static const char csch_acth_CompileProject[] = "Compile the abstract model of the current project, using the current view (or the view named)";
fgw_error_t csch_act_CompileProject(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hidlib = RND_ACT_DESIGN;
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_project_t *prj = (csch_project_t *)sheet->hidlib.project;
	int rv, view_id = CSCH_VIEW_DEFAULT;
	const char *view_name = NULL;

	RND_ACT_MAY_CONVARG(1, FGW_STR, CompileProject, view_name = argv[1].val.str);

	if (view_name != NULL) {
		view_id = csch_view_get_id(prj, view_name);
		if (view_id < 0) {
			rnd_message(RND_MSG_ERROR, "Invalid view name: '%s'\n", view_name);
			return FGW_ERR_ARG_CONV;
		}
	}

	if (prj->abst == NULL)
		prj->abst = calloc(sizeof(csch_abstract_t), 1);
	else
		csch_abstract_uninit(prj->abst);

	csch_abstract_init(prj->abst);
	rv = csch_compile_project(prj, view_id, prj->abst, 0);

	RND_ACT_IRES(rv);
	return 0;
}


static rnd_action_t csch_project_act_list[] = {
	{"CompileProject", csch_act_CompileProject, csch_acth_CompileProject, csch_acts_CompileProject},
};

void csch_project_act_init(void)
{
	RND_REGISTER_ACTIONS(csch_project_act_list, project_cookie);
}

void csch_project_act_uninit(void)
{
	rnd_remove_actions_by_cookie(project_cookie);
}

