/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#ifndef CSCH_CONCRETE_BITMAP_H
#define CSCH_CONCRETE_BITMAP_H
#include "libcschem/concrete.h"

typedef csch_uint32_t csch_pixel_t; /* rgb(a) color of pixelated objects */

/* type=CSCH_CTYPE_BITMAP */
typedef struct csch_cbitmap_s {
	csch_chdr_t hdr;
	csch_coord_t inst_x1, inst_y1;
	csch_coord_t spec_x1, spec_y1;
	csch_pixel_t transp;
	int sx, sy; /* size in pixels */
	csch_pixel_t *bitmap;
} csch_cbitmap_t;

csch_cbitmap_t *csch_cbitmap_alloc(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_oid_t oid);
csch_cbitmap_t *csch_cbitmap_dup(csch_sheet_t *sheet, csch_cgrp_t *parent, const csch_cbitmap_t *src, int keep_id);
void csch_cbitmap_free(csch_cbitmap_t *bitmap);
csch_cbitmap_t *csch_cbitmap_get(csch_sheet_t *sheet, csch_cgrp_t *grp, csch_oid_t oid);

#endif
