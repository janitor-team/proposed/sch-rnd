#include "htPo.h"
#define HT(x) htPo_ ## x
static htPo_value_t invalid;
#define HT_INVALID_VALUE invalid
#include <genht/ht.c>
#include <genht/hash.h>

int htPo_keyeq(htPo_key_t a, htPo_key_t b)
{
	return (a.x == b.x) && (a.y == b.y);
}

unsigned int htPo_keyhash(htPo_key_t a)
{
	return longhash(a.x) ^ longhash(a.y);
}

#undef HT
