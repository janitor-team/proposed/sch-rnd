/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_EVENT_H
#define CSCH_EVENT_H

#include <librnd/core/event.h>

/* Prefix:
   [d]: per-design: generated for a specific rnd_design_t
   [a]: per-app: generated once, not targeted or specific for a rnd_design_t
*/
enum {
	CSCH_EVENT_LAYERVIS_CHANGED= RND_EVENT_app,  /* [d] called after the visibility of layers has changed */
	CSCH_EVENT_UNDO_POST,                        /* [d] called after undo */
	CSCH_EVENT_SHEET_EDITED,                     /* [d] called after any sheet edit */
	CSCH_EVENT_SHEET_PREUNLOAD,                  /* [d] called before sheet is unloaded, arg: none (sheet is accessible in hidlib) */
	CSCH_EVENT_SHEET_POSTUNLOAD,                 /* [d] called after sheet is unloaded, arg: none (hidlib in the next sheet we are switching to) */
	CSCH_EVENT_SHEET_POSTLOAD,                   /* [d] called after sheet is loaded, so that the app can do postprocessing; arg: none (sheet is accessible in hidlib) */
	CSCH_EVENT_SHEET_POSTSAVE,                   /* [d] called after sheet is saved; arg: none (sheet is accessible in hidlib) */
	CSCH_EVENT_OBJ_NEEDS_REDRAW,                 /* [d] called when an object needs redraw (object properties or geometry changed); arg: (csch_chdr_t *) */
	CSCH_EVENT_OBJ_ATTR_EDITED,                  /* [d] called after object attributes got edited arg: (csch_chdr_t *) */
	CSCH_EVENT_PRJ_COMPILED,                     /* [d] called after the project has been compiled (abstract model changed); no args (project can be retrieved from hidlib -> sheet) */
	CSCH_EVENT_PRJ_VIEWS_CHANGED,                /* [d] called after project view configuration changed; no args (project can be retrieved from hidlib -> sheet) */
	CSCH_EVENT_PRJ_VIEW_ACTIVATED,               /* [d] called after a new view is set active in a project (project can be retrieved from hidlib -> sheet) */
	CSCH_EVENT_LIBRARY_CHANGED,                  /* [d] called after a new entry is added to the library so that any open library window can be updated on the GUI; no args (sheet can be retrieved from hidlib) */
	CSCH_EVENT_SELECTION_CHANGED,                /* [d] called after object selection changed (sleected/unselected objects) */

	/* generated by the app, not libcschem */
	CSCH_EVENT_BUFFER_COPY_CUSTOM,               /* [d] called before an object is duplicated from sheet to buffer; args: (csch_chdr_t **obj, csch_sheet_t *buffer); *obj is the current object, initially the source object on the sheet; destination buffer the second arg */
	CSCH_EVENT_BUFFER_PASTE_CUSTOM,              /* [d] called before an object is duplicated from buffer to sheet; args: (csch_chdr_t **obj); *obj is the current object, initially the source object in the buffer; destination sheet is in the hidlib arg */

	CSCH_EVENT_last                              /* not a real event */
};

void csch_event_init_app(void);

#endif
