#ifndef LIBCSCHEM_UTIL_GRP
#define LIBCSCHEM_UTIL_GRP

#include "cnc_text.h"

/* Place a new floater for an existing attribute, by attr key
   If hint_xy is not NULL, it contains coords where the GUI action started */
csch_text_t *csch_auto_attr_place(csch_sheet_t *sheet, csch_cgrp_t *grp, const char *key, const char *stroke, const char *atempl, csch_coord_t *hint_xy);

/* Create a new attribute and place a dyntext-floater for it if stroke is
   not NULL; returns the floater created. */
csch_text_t *csch_auto_attr_create(csch_sheet_t *sheet, csch_cgrp_t *grp, const char *key, const char *val, const char *stroke, const char *atempl);

/* Apply conventional side effects of role on grp: create attributes, rename pens */
void csch_cgrp_role_side_effects(csch_sheet_t *sheet, csch_cgrp_t *grp, csch_role_t role);

/* Calls csch_cobj_update_dsply() recursively */
void csch_cgrp_update_dsply_recurse(csch_sheet_t *sheet, csch_cgrp_t *grp);


typedef enum csch_cgrp_replace_how_e { /* bitfield */
	CSCH_REPLGRP_KEEP_DST_ATTR = 1,    /* preserve attributes of the dst group */
	CSCH_REPLGRP_PREFER_DST_ATTR = 2   /* if an attribute is specified both in dst and src, prefer dst's */
} csch_cgrp_replace_how_t;

int csch_cgrp_replace(csch_sheet_t *sheet, csch_cgrp_t *dst, csch_cgrp_t *src, csch_cgrp_replace_how_t how);

#endif
