/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "config.h"

#include <genvector/vts0.h>
#include <genvector/gds_char.h>

#include <librnd/core/global_typedefs.h>

#include <libcschem/plug_library.h>

/* look up library root_path's real path, skipping the ? prefix */
char *csch_lib_fs_realpath(rnd_design_t *hl, char *root_path);

/* map path dir recursively for library entries; check each entry with
   type_cb, if it returns invalid type, omit the entry. */
void csch_lib_fs_map(rnd_design_t *hl, csch_lib_backend_t *be, csch_lib_t *parent, gds_t *path, csch_lib_type_t (*type_cb)(rnd_design_t *, const char *fn));

