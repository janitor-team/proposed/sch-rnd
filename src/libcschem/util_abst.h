#ifndef LIBCSCHEM_UTIL_ANST_H
#define LIBCSCHEM_UTIL_ABST_H

#include <libcschem/concrete.h>
#include <libcschem/abstract.h>

/* Returns whether concrete c is a source of abstract a */
int csch_cgrp_is_in_asrc(const csch_cgrp_t *c, const csch_ahdr_t *a);

#endif
