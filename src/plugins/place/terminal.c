/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_line.h>
#include <libcschem/util_grp.h>
#include <libcschem/operation.h>

static int place_terminal(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_coord_t ox, csch_coord_t oy)
{
	csch_cgrp_t *grp = (csch_cgrp_t *)csch_op_create(sheet, parent, CSCH_CTYPE_GRP);
	csch_line_t *line = (csch_line_t *)csch_op_create(sheet, grp, CSCH_CTYPE_LINE);

	grp->x = ox;
	grp->y = oy;

	line->spec.p1.x = 0; line->spec.p1.y = 0;
	line->spec.p2.x = -1 * 4000; line->spec.p2.y = 0;

	line->hdr.stroke_name = csch_comm_str(sheet, "term-decor", 1);
	csch_line_update(sheet, line, 1);
	csch_cgrp_update(sheet, grp, 1);

	csch_cgrp_role_side_effects(sheet, grp, CSCH_ROLE_TERMINAL);
	csch_cgrp_update(sheet, grp, 1);

	csch_sheet_set_changed(sheet, 1);

	return 0;
}

static const char csch_acts_PlaceTerminal[] = "PlaceTerminal([sheet|buffer])\n";
static const char csch_acth_PlaceTerminal[] = "Place a terminal group template ";
static fgw_error_t csch_act_PlaceTerminal(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	return place_action(res, argc, argv, place_terminal);
}
