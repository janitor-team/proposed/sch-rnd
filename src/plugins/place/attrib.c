/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_pen.h>
#include <libcschem/cnc_text.h>
#include <libcschem/util_grp.h>
#include <libcschem/operation.h>

#include <sch-rnd/search.h>

static int place_attrib(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_coord_t ox, csch_coord_t oy)
{
	csch_text_t *text;
	const char *penname = NULL;
	csch_cpen_t *stroke;

	/* if we are on the sheet, look for a host group */
	if (parent == &sheet->direct) {
		csch_rtree_box_t box;

		box.x1 = ox - sch_rnd_slop; box.y1 = oy - sch_rnd_slop;
		box.x2 = ox + sch_rnd_slop; box.y2 = oy + sch_rnd_slop;

		parent = (csch_cgrp_t *)csch_search_first_mask(sheet, &box, CSCH_CMASK_ANY_GRP);
		if (parent == NULL)
			parent = &sheet->direct;
		assert(csch_obj_is_grp(&parent->hdr));
	}



	text = (csch_text_t *)csch_op_create(sheet, parent, CSCH_CTYPE_TEXT);
	text->spec1.x = ox - parent->x;
	text->spec1.y = oy - parent->y;
	text->hdr.floater = 1;
	text->dyntext = 1;

	switch(parent->role) {
		case CSCH_ROLE_BUS_NET:      penname = "bus"; break;
		case CSCH_ROLE_BUS_TERMINAL: penname = "busterm-secondary"; break;
		case CSCH_ROLE_WIRE_NET:     penname = "wire"; break;
		case CSCH_ROLE_HUB_POINT:    penname = "wire"; break;
		case CSCH_ROLE_SYMBOL:       penname = "sym-secondary"; break;
		case CSCH_ROLE_TERMINAL:     penname = "term-secondary"; break;
		case CSCH_ROLE_JUNCTION:     penname = "junction"; break;

		case CSCH_ROLE_invalid:
		case CSCH_ROLE_empty:
			penname = "sheet-decor";
			break;
	}

	text->text = "%../A.key%";

	stroke = csch_pen_resolve(parent, penname);
	if (stroke == NULL)
		stroke = &csch_pen_default_unknown;
	text->hdr.stroke_name = csch_comm_str(sheet, stroke->name.str, 1);

	csch_text_update(sheet, text, 1);

	csch_sheet_set_changed(sheet, 1);

	return 0;
}

static const char csch_acts_PlaceAttrib[] = "PlaceAttrib([sheet|buffer])\n";
static const char csch_acth_PlaceAttrib[] = "Place an attribute floater dyntext";
static fgw_error_t csch_act_PlaceAttrib(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	return place_action(res, argc, argv, place_attrib);
}
