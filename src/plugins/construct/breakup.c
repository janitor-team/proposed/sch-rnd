/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Break up group into individual objects */

#include <librnd/core/error.h>
#include <libcschem/concrete.h>
#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_poly.h>
#include <libcschem/cnc_any_obj.h>
#include <libcschem/operation.h>

static int sch_rnd_breakup_grp(csch_sheet_t *sheet, csch_cgrp_t *dst, csch_cgrp_t *src, int silent)
{
	long res = 0;
	htip_entry_t *e;

	for(e = htip_first(&src->id2obj); e != NULL; e = htip_next(&src->id2obj, e)) {
		csch_chdr_t *newo = csch_op_copy_into(sheet, dst, e->value);
		if (newo != NULL)
			csch_inst2spec(sheet, newo, e->value, 1);
		else
			res = -1;
	}

	return res;
}

static int sch_rnd_breakup_poly(csch_sheet_t *sheet, csch_cgrp_t *dst, csch_cpoly_t *src, int silent)
{
	int res = 0;
	long n;

	for(n = 0; n < src->outline.used; n++) {
		csch_coutline_t *olo = &src->outline.array[n];
		csch_chdr_t *newo = csch_op_copy_into(sheet, dst, &olo->hdr);
		if (newo == NULL)
			res = -1;
	}

	return res;
}


