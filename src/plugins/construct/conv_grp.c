/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Convert floating objects to group (low level) */

#include <librnd/core/error.h>
#include <libcschem/concrete.h>
#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_any_obj.h>
#include <libcschem/operation.h>

#include <sch-rnd/crosshair.h>

static csch_cgrp_t *sch_rnd_conv_grp(csch_sheet_t *sheet, csch_cgrp_t *parent, vtp0_t *objs, csch_coord_t ox, csch_coord_t oy, int silent)
{
	long n;
	unsigned int warn = 0;
	csch_cgrp_t *grp = (csch_cgrp_t *)csch_op_create(sheet, parent, CSCH_CTYPE_GRP);

	for(n = 0; n < objs->used; n++) {
		csch_chdr_t *newo, *obj = objs->array[n];

		if (!can_convert(sheet, obj, &warn, 1)) {
			vtp0_remove(objs, n, 1);
			n--;
			continue;
		}

		newo = csch_cobj_dup(sheet, grp, obj, 0, 0);
		if ((newo != NULL) && ((ox != 0) || (oy != 0)))
			csch_move(sheet, newo, -ox, -oy, 0);
	}

	grp->x = ox;
	grp->y = oy;

	csch_cgrp_update(sheet, grp, 1);

	return grp;
}


