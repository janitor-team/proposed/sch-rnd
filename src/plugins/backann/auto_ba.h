typedef enum {
	SCH_RND_BAA_ATTR,         /* need to edit an attribute */
	SCH_RND_BAA_DEL,          /* need to remove an object */
	SCH_RND_BAA_BREAK_GRCONN, /* need to break graphical connection, but don't know how exactly */
	SCH_RND_BAA_MAKE_GRCONN   /* need to make a graphical connection, but don't know how exactly */
} sch_rnd_backann_auto_type_t;

/* Changing the attribute of obj to want_val would resolve a backann entry */
typedef struct sch_rnd_backann_auto_attr_s {
	sch_rnd_backann_auto_type_t type;
	csch_cgrp_t *grp;
	int is_str;
	char *key;
	char *want_val;
} sch_rnd_backann_auto_attr_t;

/* Removing the specified obj would resolve a backann entry */
typedef struct sch_rnd_backann_auto_del_s {
	sch_rnd_backann_auto_type_t type;
	csch_chdr_t *obj;
} sch_rnd_backann_auto_del_t;

/* The user needs to break graphical connection between the two objects */
typedef struct sch_rnd_backann_auto_break_grconn_s {
	sch_rnd_backann_auto_type_t type;
	csch_chdr_t *term_obj, *wire_obj;
} sch_rnd_backann_auto_break_grconn_t;

/* The user needs to make a graphical connection between the two objects */
typedef struct sch_rnd_backann_auto_make_grconn_s {
	sch_rnd_backann_auto_type_t type;
	csch_chdr_t *term_obj, *wire_obj;
	csch_coord_t x1, y1, x2, y2; /* suggested wire line endpoints (any-angle); x1;y1 is on the terminal side, x2;y2 is on the wire side */
} sch_rnd_backann_auto_make_grconn_t;


typedef union sch_rnd_backann_auto_s {
	sch_rnd_backann_auto_attr_t         attr;
	sch_rnd_backann_auto_del_t          del;
	sch_rnd_backann_auto_break_grconn_t break_grconn;
	sch_rnd_backann_auto_make_grconn_t  make_grconn;
	struct {
		sch_rnd_backann_auto_type_t type;
	} any;
} sch_rnd_backann_auto_t;

/* Return a malloc()'d description of what needs changed, or NULL if can't
   be auto-fixed */
sch_rnd_backann_auto_t *sch_rnd_backann_auto_entry(sch_rnd_backann_t *ctx, sch_rnd_ba_t *ba);

/* Free au and all its fields */
void sch_rnd_backann_auto_free(sch_rnd_backann_auto_t *au);
