#include <librnd/hid/hid_dad.h>

#include <libcschem/concrete.h>

typedef enum sch_rnd_ba_type_e {
	SCH_RND_BAT_invalid = 0,
	SCH_RND_BAT_NETINFO,
	SCH_RND_BAT_CONN_DEL,
	SCH_RND_BAT_CONN_ADD,
	SCH_RND_BAT_NET_ATTR,
	SCH_RND_BAT_COMP_ATTR,
	SCH_RND_BAT_TERM_ATTR
} sch_rnd_ba_type_t;

typedef enum sch_rnd_ba_status_e {
	SCH_RND_BAS_UNKNOWN,
	SCH_RND_BAS_PENDING,
	SCH_RND_BAS_DONE
} sch_rnd_ba_status_t;

typedef struct sch_rnd_ba_s {
	sch_rnd_ba_type_t type;
	char *raw; /* only raw is allocated, value fields are pointing into raw */
	union {
		struct { char *net, *comp, *term; }  netinfo;
		struct { char *comp, *term, *net; }  conn_del;
		struct { char *comp, *term, *net; }  conn_add;
		struct { char *net, *key, *val; }    net_attr;
		struct { char *comp, *key, *val; }   comp_attr;

		struct { char *comp, *term, *key, *val; } term_attr;

		struct { char *a0, *a1, *a2, *a3; } any4;
	} value;
	sch_rnd_ba_status_t status;
	unsigned optional:1;   /* do not enforce (e.g. netinfo when changed by later entries */
} sch_rnd_ba_t;

/* Elem=sch_rnd_ba_t; init=0 */
#define GVT(x) vtba_ ## x
#define GVT_ELEM_TYPE sch_rnd_ba_t
#define GVT_SIZE_TYPE size_t
#define GVT_DOUBLING_THRS 4096
#define GVT_START_SIZE 32
#define GVT_FUNC
#define GVT_SET_NEW_BYTES_TO 0
#include <genvector/genvector_impl.h>
#include <genvector/genvector_undef.h>


typedef struct sch_rnd_backann_s {
	RND_DAD_DECL_NOINIT(dlg)
	csch_sheet_t *sheet;
	char *fn;
	vtba_t list;

	/* widgets */
	int wlist;
} sch_rnd_backann_t;

extern htpp_t sch_rnd_prj2backann;
extern int sch_rnd_prj2backann_inited;

void sch_rnd_backann_uninit(sch_rnd_backann_t *ctx);

