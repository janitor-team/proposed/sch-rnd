/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - tEDAx netlist export
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <librnd/hid/hid.h>
#include <librnd/hid/hid_attrib.h>
#include <librnd/hid/hid_nogui.h>
#include <librnd/hid/hid_init.h>
#include <libcschem/project.h>
#include <libcschem/util_export.h>
#include <sch-rnd/export.h>

static const char tedax_cookie[] = "tEDAx export hid";

static const rnd_export_opt_t tedax_options[] = {
	{"outfile", "Name of the tEDAx netlist output file",
	 RND_HATT_STRING, 0, 0, {0, 0, 0}, 0},
#define HA_outfile 0

	{"view", "Name of the view to export (use first view when not specified)",
	 RND_HATT_STRING, 0, 0, {0, 0, 0}, 0},
#define HA_view 1
};

#define NUM_OPTIONS (sizeof(tedax_options)/sizeof(tedax_options[0]))

static rnd_hid_attr_val_t tedax_values[NUM_OPTIONS];

static const rnd_export_opt_t *tedax_get_export_options(rnd_hid_t *hid, int *n, rnd_design_t *dsg, void *appspec)
{
	const char *val = tedax_values[HA_outfile].str;

	if ((dsg != NULL) && ((val == NULL) || (*val == '\0')))
		csch_derive_default_filename(dsg, 1, &tedax_values[HA_outfile], ".tdx");

	if (n)
		*n = NUM_OPTIONS;
	return tedax_options;
}

static void tedax_do_export(rnd_hid_t *hid, rnd_design_t *design, rnd_hid_attr_val_t *options, void *appspec)
{
	csch_sheet_t *sheet = (csch_sheet_t *)design;
	int viewid = CSCH_VIEW_DEFAULT;

	if (!options) {
		tedax_get_export_options(hid, 0, design, appspec);
		options = tedax_values;
	}

	if ((options[HA_view].str != NULL) && (options[HA_view].str[0] != '\0')) {
		viewid = csch_view_get_id((csch_project_t *)sheet->hidlib.project, options[HA_view].str);
		if (viewid < 0) {
			rnd_message(RND_MSG_ERROR, "No such view in the project: '%s'\n", options[HA_view].str);
			return;
		}
	}

	sch_rnd_export_prj_abst((csch_project_t *)sheet->hidlib.project, sheet, viewid, "tedax", options[HA_outfile].str);
}

static int tedax_usage(rnd_hid_t *hid, const char *topic)
{
	fprintf(stderr, "\ntEDAx exporter command line arguments:\n\n");
	rnd_hid_usage(tedax_options, sizeof(tedax_options) / sizeof(tedax_options[0]));
	fprintf(stderr, "\nUsage: sch-rnd [generic_options] -x tedax [options] foo.rs\n\n");
	return 0;
}


static int tedax_parse_arguments(rnd_hid_t *hid, int *argc, char ***argv)
{
	rnd_export_register_opts2(hid, tedax_options, sizeof(tedax_options) / sizeof(tedax_options[0]), tedax_cookie, 0);
	return rnd_hid_parse_command_line(argc, argv);
}

rnd_hid_t tedax_hid = {0};
