/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - property editor plugin
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  (Adapted from propsel code in pcb-rnd by the same author)
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* This API builds a prop list by looking at all selected objects on a design. */
void csch_propsel_map_core(csch_propedit_t *ctx);

typedef struct set_ctx_s {
	const char *s; /* only for string */
	csch_coord_t c; /* also int */
	double d;
	csch_comm_str_t comms;
	minuid_bin_t u;
	rnd_bool c_absolute, d_absolute, c_valid, d_valid;
	unsigned toggle:1;        /* when 1, ignore value and attempt to toggle */
	unsigned toggle_create:1; /* when 1, create non-existing attribute on toggle, with value true */

	/* private */
	unsigned is_attr:1;
	csch_sheet_t *sheet;
	csch_cgrp_t *grp;
	const char *name;
	int set_cnt;
} csch_propset_ctx_t;


int csch_propsel_set_str(csch_propedit_t *ctx, const char *prop, const char *value);
int csch_propsel_set(csch_propedit_t *ctx, const char *prop, csch_propset_ctx_t *sctx);
int csch_propsel_toggle(csch_propedit_t *ctx, const char *prop, rnd_bool create);
int csch_propsel_del(csch_propedit_t *ctx, const char *attr_name);

/* Allocate new string and print the value using current unit */
char *csch_propsel_printval(csch_props_t *p, const csch_propval_t *val);

