/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - gEDA file format support
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <genht/htpp.h>
#include <genht/hash.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <librnd/core/error.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/rnd_printf.h>

#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_grp_child.h>
#include <libcschem/cnc_line.h>
#include <libcschem/cnc_text.h>
#include <libcschem/cnc_poly.h>
#include <libcschem/vtoid.h>
#include <plugins/lib_alien/read_helper.h>


#include "read.h"

#define error(node, args) \
	do { \
		if (!ctx->silent) { \
			rnd_message(RND_MSG_ERROR, "tinycad parse error at %s:%ld:\n", ctx->fn, (long)(node)->line); \
			rnd_msg_error args; \
		} \
	} while(0)

typedef struct read_ctx_s {
	const char *fn;
	xmlDoc *doc;
	xmlNode *root;
	csch_alien_read_ctx_t alien;
	csch_cgrp_t *symdef; /* symdef we are reading at the moment */
	double symdef_dx, symdef_dy;
	htpp_t sym2xml;  /* key: (csch_cgrp_t *); value: (xmlNode *) */
	unsigned silent:1;
} read_ctx_t;

#define PROP(node, propname) \
	((const char *)xmlGetProp((node), (const xmlChar *)(propname)))

/* Return 1 if nd->name matches expected */
static int node_name_eq(xmlNode *nd, const char *expected)
{
	return (xmlStrcmp(nd->name, (const unsigned char *)expected) == 0);
}

static int parse_coords(read_ctx_t *ctx, xmlNode *nd, const char *str, double *x, double *y)
{
	char *end;
	*x = strtod(str, &end);
	if (*end != ',') {
		error(nd, ("Missing comma in coords\n"));
		return -1;
	}
	*y = strtod(end+1, &end);
	if (*end != '\0') {
		error(nd, ("Invalid y coord (need numeric)\n"));
		return -1;
	}
	return 0;
}

static const char *parse_text(read_ctx_t *ctx, xmlNode *nd, int mandatory)
{
	if (nd->children == NULL) {
		if (mandatory)
			error(nd, ("Missing text child\n"));
		return NULL;
	}
	return (const char *)nd->children->content;
}

static int parse_bool(read_ctx_t *ctx, xmlNode *nd, const char *inp)
{
	if (inp == NULL) return 0;
	if ((inp[0] == '1') && (inp[1] == '\0')) return 1;
	if ((inp[0] == '0') && (inp[1] == '\0')) return 0;
	error(nd, ("Invalid boolean value %s; expected 0 or 1\n", inp));
	return -1;
}

/* Parse integer, overwrite dst if inp is not NULL */
static int parse_long(read_ctx_t *ctx, xmlNode *nd, const char *inp, long *dst, int mandatory)
{
	char *end;
	long tmp;

	if (inp == NULL) {
		if (mandatory) {
			error(nd, ("missing integer data\n"));
			return -1;
		}
		return 0;
	}

	tmp = strtol(inp, &end, 10);
	if (*end != '\0') {
		if (mandatory)
			error(nd, ("Invalid integer value '%s' (should be decimal)\n", inp));
		return -1;
	}

	*dst = tmp;
	return 0;
}

/* Parse decimal, overwrite dst if inp is not NULL */
static int parse_double(read_ctx_t *ctx, xmlNode *nd, const char *inp, double *dst, int mandatory)
{
	char *end;
	double tmp;

	if (inp == NULL) {
		if (mandatory) {
			error(nd, ("missing decimal data\n"));
			return -1;
		}
		return 0;
	}

	tmp = strtod(inp, &end);
	if (*end != '\0') {
		if (mandatory)
			error(nd, ("Invalid decimal value '%s'\n", inp));
		return -1;
	}

	*dst = tmp;
	return 0;
}

static double rot2deg(read_ctx_t *ctx, xmlNode *nd, int rot)
{
	switch(rot) {
		case 0: return 0;
		case 1: return 180;
		case 2: return 270;
		case 3: return 90;
	}
	error(nd, ("Invalid rotation value %d: should be 0..3\n", rot));
	return rot2deg(ctx, nd, rot % 4);
}

static int dir2dxy(read_ctx_t *ctx, xmlNode *root, int dir, double *dx, double *dy, double len)
{
	switch(dir) {
		case 0: *dy = -len; break;
		case 1: *dy = len; break;
		case 2: *dx = -len; break;
		case 3: *dx = +len; break;
		default:
			error(root, ("Invalid direction %d: must be 0..3\n", dir));
			return -1;
	}
	return 0;
}

static void rot2sym(read_ctx_t *ctx, xmlNode *nd, int rotin, double *rot_out, int *mirx, int *miry, int *dx, int *dy, int *swbb)
{
	switch(rotin) {
		case 0: *mirx = 0; *miry = 0; *rot_out = 0;   *dx = 0;  *dy = 0; *swbb=0; return;
		case 1: *mirx = 0; *miry = 1; *rot_out = 0;   *dx = 0;  *dy = 1; *swbb=0; return;
		case 2: *mirx = 0; *miry = 1; *rot_out = 90;  *dx = 0;  *dy = 0; *swbb=0; return;
		case 3: *mirx = 0; *miry = 0; *rot_out = -90; *dx = -1; *dy = 0; *swbb=1; return;
		case 4: *mirx = 1; *miry = 0; *rot_out = 0;   *dx = -1; *dy = 0; *swbb=0; return;
		case 5: *mirx = 1; *miry = 1; *rot_out = 0;   *dx = -1; *dy = 1; *swbb=0; return;
		case 6: *mirx = 1; *miry = 1; *rot_out = -90; *dx = 0;  *dy = 1; *swbb=1; return;
		case 7: *mirx = 1; *miry = 0; *rot_out = 90;  *dx = -1; *dy = 1; *swbb=1; return;
	}
	*mirx = *miry = 0;
	*rot_out = 0;
	*dx = *dy = 0;
	*swbb = 0;
	error(nd, ("Invalid rotation value %d: should be 0..7\n", rotin));
}


typedef struct {
	const char *nodename;
	int (*cb)(read_ctx_t *ctx, void *dst, xmlNode *root);
} parser_t;

static int parse_all(read_ctx_t *ctx, void *dst, xmlNode *root, const parser_t *parsers)
{
	const parser_t *p;
	xmlNode *n;
	int res;

	for(n = root->children; n != NULL; n = n->next) {
		for(p = parsers; p->nodename != NULL; p++) {
			if (xmlStrcmp(n->name, (const unsigned char *)p->nodename) == 0) {
				res = p->cb(ctx, dst, n);
				if (res != 0)
					return res;
			}
		}
	}

	return 0;
}


static int parse_name(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	/* Nothing to do with this: this is the tab name in tinycad */
	return 0;
}

static int parse_detail_size(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *sw = PROP(root, "width"), *sh = PROP(root, "height");
	char tmp[64];
	long lw, lh;
	csch_coord_t w, h;
	csch_source_arg_t *src;
	csch_sheet_t *sheet = dst;

	if (parse_long(ctx, root, sw, &lw, 1) != 0)    return -1;
	if (parse_long(ctx, root, sh, &lh, 1) != 0)    return -1;

	w = csch_alien_coord(&ctx->alien, lw)/5;
	h = csch_alien_coord(&ctx->alien, lh)/5;

	sprintf(tmp, "%ld", w);
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&sheet->direct.attr, CSCH_ATP_USER_DEFAULT, "drawing_min_width", tmp, src, NULL);

	sprintf(tmp, "%ld", h);
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&sheet->direct.attr, CSCH_ATP_USER_DEFAULT, "drawing_min_height", tmp, src, NULL);

	ctx->alien.oy = lh/5;
	return 0;
}

static int parse_detail_attr(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *key, *val;
	csch_source_arg_t *src;
	csch_sheet_t *sheet = dst;

	switch(root->name[0]) {
		case 'T': key = "title"; break;
		case 'A': key = "maintainer"; break;
		case 'S': key = "page"; break;
		default: return 0; /* shouldn't get here: parse_all ran strcmp() on the name */
	}

	val = parse_text(ctx, root, 0);
	if (val == NULL)
		val = "";

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&sheet->direct.attr, CSCH_ATP_USER_DEFAULT, key, val, src, NULL);
	return 0;
}


static int parse_details(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	static const parser_t parsers[] = {
		{"Size", parse_detail_size},
		{"TITLE", parse_detail_attr},
		{"AUTHOR", parse_detail_attr},
		{"SHEETS", parse_detail_attr},
		/*{"SHOWS", parse_detail_...}, this tells whether the titlebox is shown */
		{NULL, NULL}
	};

	return parse_all(ctx, dst, root, parsers);
}


/*** symdef and sym ***/

static int parse_symdef_ref(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	char *ref;
	csch_source_arg_t *src;

	if (root->children == NULL) {
		error(root, ("Invalid symdef ref name: empty subtree, no text node\n"));
		return -1;
	}
	ref = (char *)root->children->content;
	if ((ref == NULL) || (*ref == '\0')) {
		error(root, ("Invalid symdef ref name: empty string\n"));
		return -1;
	}

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&ctx->symdef->attr, CSCH_ATP_USER_DEFAULT, "name", ref, src, NULL);

	return 0;
}

static int parse_symdef_field(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *key = NULL, *val = NULL;
	double x = 0, y = 0;
	csch_source_arg_t *src;
	xmlNode *n;

	for(n = root->children; n != NULL; n = n->next) {
		if (node_name_eq(n, "NAME"))  key = parse_text(ctx, n, 1);
		if (node_name_eq(n, "VALUE")) val = parse_text(ctx, n, 0);
		if (node_name_eq(n, "POS")) parse_coords(ctx, n, parse_text(ctx, n, 0), &x, &y);
	}

	if ((key == NULL) || (val == NULL) || (*val == '\0'))
		return 0; /* don't create empty attributes */

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&ctx->symdef->attr, CSCH_ATP_USER_DEFAULT, key, val, src, NULL);
	return 0;
}

static int parse_symdef_ref_point(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *pos = PROP(root, "pos");
	double x, y;

	if (parse_coords(ctx, root, pos, &x, &y) != 0)
		return -1;

	ctx->symdef_dx = -x;
	ctx->symdef_dy = -y;
	return 0;
}

static int parse_symdef_pin(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *spos = PROP(root, "pos"), *sdir = PROP(root, "direction");
	const char *slen = PROP(root, "length"), *snum = PROP(root, "number");
	const char *sshow = PROP(root, "show"), *swhich = PROP(root, "which");
	long dir, llen, num;
	int show, which;
	double ox, oy, dx = 0, dy = 0, len;
	csch_cgrp_t *pin;
	csch_source_arg_t *src;

	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)   return -1;
	if (parse_long(ctx, root, sdir, &dir, 1) != 0)      return -1;
	if (parse_long(ctx, root, slen, &llen, 1) != 0)     return -1;
	if (parse_long(ctx, root, snum, &num, 1) != 0)      return -1;
	if (parse_long(ctx, root, swhich, &which, 1) != 0)  return -1;
	if (parse_long(ctx, root, sshow, &show, 1) != 0)    return -1;

	TODO("show controls how the pin number is shown (not bool)\n");

	if (which != 5)
		len = (double)llen/5.0;
	else
		len = 0;

	if (dir2dxy(ctx, root, dir, &dx, &dy, len) != 0)
		return -1;

	ox += ctx->symdef_dx; oy += ctx->symdef_dy;

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	pin = (csch_cgrp_t *)csch_alien_mkpin_line(&ctx->alien, src, ctx->symdef, ox, oy, ox+dx, oy+dy);

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&pin->attr, CSCH_ATP_USER_DEFAULT, "name", snum, src, NULL);

	return 0;
}

static int parse_polygon(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *spos = PROP(root, "pos");
	const char *sfill = PROP(root, "fill"), *spoly = PROP(root, "polygon");
	double ox, oy;
	int fill, poly;
	long len;
	xmlNode *n;
	csch_cgrp_t *grp = dst;

	fill = parse_bool(ctx, root, sfill);
	poly = parse_bool(ctx, root, spoly);
	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)
		return -1;

	if (fill)
		poly = 1; /* required by the format */

	if (grp == ctx->symdef) {
		ox += ctx->symdef_dx;
		oy += ctx->symdef_dy;
	}

	/* verify and count points */
	for(n = root->children, len = 0; n != NULL; n = n->next, len++) {
		if (node_name_eq(n, "text")) {
			/* ignore */
		}
		else if (node_name_eq(n, "POINT")) {
			len++;
		}
		else{
			error(n, ("Invalid node in polygon: must be POINT, not '%s'\n", n->name));
			return -1;
		}
		
	}

	if (len == 1) {
		error(n, ("Invalid polygon: must contain more than one POINTs\n"));
		return -1;
	}

	if (fill) { /* real polygon */
		csch_chdr_t *poly = csch_alien_mkpoly(&ctx->alien, grp, "sym-decor", NULL);
		csch_cpoly_t *cp;
		double lx, ly, x, y;
		int first = 1;
		for(n = root->children; n != NULL; n = n->next) {
			if (node_name_eq(n, "POINT")) {
				const char *spos = PROP(n, "pos");

				if (parse_coords(ctx, n, spos, &x, &y) != 0)
					return -1;

				x += ox; y += oy;

				if (!first && ((x != lx) || (y != ly)))
					csch_alien_append_poly_line(&ctx->alien, poly, lx, ly, x, y);

				lx = x; ly = y;
				first = 0;
			}
		}
		cp = (csch_cpoly_t *)poly;
		cp->has_fill = 1;
		cp->hdr.fill_name = cp->hdr.stroke_name;
	}
	else { /* polyline */
		double lx, ly, fx, fy, x, y;
		int first = 1;
		for(n = root->children; n != NULL; n = n->next) {
			if (node_name_eq(n, "POINT")) {
				const char *spos = PROP(n, "pos");

				if (parse_coords(ctx, n, spos, &x, &y) != 0)
					return -1;

				x += ox; y += oy;

				if (first) {
					fx = x; fy = y;
				}
				else
					csch_alien_mkline(&ctx->alien, grp, lx, ly, x, y, "sym-decor");

				lx = x; ly = y;
				first = 0;
			}
		}
		if (poly) /* close polygon */
			csch_alien_mkline(&ctx->alien, grp, lx, ly, fx, fy, "sym-decor");
	}

	return 0;
}

static int parse_symdef_polygon(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	parse_polygon(ctx, ctx->symdef, root);
}

static int parse_sheet_polygon(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	parse_polygon(ctx, &ctx->alien.sheet->direct, root);
}


static int parse_symdef_rectangle(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *sa = PROP(root, "a"), *sb = PROP(root, "b");
	const char *sfill = PROP(root, "fill");
	double ax, ay, bx, by;
	csch_chdr_t *poly;
	int fill;

	fill = parse_bool(ctx, root, sfill);
	if (fill < 0)                                      return -1;
	if (parse_coords(ctx, root, sa, &ax, &ay) != 0)    return -1;
	if (parse_coords(ctx, root, sb, &bx, &by) != 0)    return -1;

	ax += ctx->symdef_dx; ay += ctx->symdef_dy;
	bx += ctx->symdef_dx; by += ctx->symdef_dy;

	poly = csch_alien_mkpoly(&ctx->alien, ctx->symdef, "sym-decor", (fill ? "sym-decor" : NULL));
	csch_alien_append_poly_line(&ctx->alien, poly, ax, ay, bx, ay);
	csch_alien_append_poly_line(&ctx->alien, poly, bx, ay, bx, by);
	csch_alien_append_poly_line(&ctx->alien, poly, bx, by, ax, by);
	csch_alien_append_poly_line(&ctx->alien, poly, ax, by, ax, ay);

	return 0;
}


static int parse_symdef_drawing(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	int res;
	double save;

	static const parser_t parsers[] = {
		{"PIN", parse_symdef_pin},
		{"POLYGON", parse_symdef_polygon},
		{"RECTANGLE", parse_symdef_rectangle},
		{NULL, NULL}
	};

	save = ctx->alien.oy;
	ctx->alien.oy = 0;
	res = parse_all(ctx, dst, root, parsers);
	ctx->alien.oy = save;

	return res;
}


static int parse_symdef(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	long oid;
	int res;
	const char *sid = PROP(root, "id");
	char *end;
	csch_source_arg_t *src;
	csch_sheet_t *sheet = dst;
	static const parser_t parsers1[] = {
		{"REF_POINT", parse_symdef_ref_point},
		{NULL, NULL}
	};
	static const parser_t parsers2[] = {
		{"REF", parse_symdef_ref},
		{"FIELD", parse_symdef_field},
		{"TinyCAD", parse_symdef_drawing},
		{NULL, NULL}
	};

	oid = strtol(sid, &end, 10);
	if (*end != '\0') {
		error(root, ("Invalid symdef id: must be an integer\n"));
		return -1;
	}
	if (oid <= 0) {
		error(root, ("Invalid symdef id: must be greater than zero\n"));
		return -1;
	}

	ctx->symdef = csch_cgrp_alloc(dst, &sheet->indirect, oid);
	if (ctx->symdef == NULL) {
		error(root, ("Failed to allocate symdef in local lib\n"));
		return -1;
	}

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&ctx->symdef->attr, CSCH_ATP_USER_DEFAULT, "role", "symbol", src, NULL);
	csch_attr_side_effects(ctx->symdef, "role");

	res = parse_all(ctx, dst, root, parsers1); /* need to get the reference first for the offsets */
	res = parse_all(ctx, dst, root, parsers2);
	ctx->symdef = NULL;
	return res;
}

/* Return dyntext in the referee group rgrp that prints attribute key sdesc */
static csch_text_t *grp_ref_attr_text(read_ctx_t *ctx, csch_cgrp_t *rgrp, const char *sdesc, int alloc)
{
	char *tmp = rnd_strdup_printf("%%../A.%s%%", sdesc);
	csch_text_t *text;
	htip_entry_t *e;
	int isref;
	double y = 0.0;

	/* find existing dyntext matching attribute template */
	for(e = htip_first(&rgrp->id2obj); e != NULL; e = htip_next(&rgrp->id2obj, e)) {
		text = e->value;
		if (text->hdr.type == CSCH_CTYPE_TEXT) {
			if (text->dyntext && (strcmp(text->text, tmp) == 0)) {
				free(tmp);
				return text;
			}
			y += 4.0;
		}
	}

	if (!alloc)
		return NULL;

	/* not found, need to create */
	isref = (strcmp(sdesc, "Ref") == 0);
	text = (csch_text_t *)csch_alien_mktext(&ctx->alien, rgrp, 0, y, (isref ? "sym-primary" : "sym-secondary"));
	text->text = tmp;
	text->dyntext = 1;
	text->hdr.floater = 1;

	return text;
}

static int parse_sym_field(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *spos = PROP(root, "pos"), *sshow = PROP(root, "show");
	const char *sdesc = PROP(root, "description"), *sval = PROP(root, "value");
	/* attributes that we can't figure: type */
	double ox, oy;
	long show;
	csch_source_arg_t *src;
	csch_cgrp_t *sym = (csch_cgrp_t *)dst;

	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)   return -1;
	if (parse_long(ctx, root, sshow, &show, 1) != 0)    return -1;

	if ((sdesc == NULL) || (*sdesc == '\0'))
		return 0;

	if ((sval[0] == '.') && (sval[1] == '.') && (sval[2] == '\0') && !show)
		return 0;

	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&sym->attr, CSCH_ATP_USER_DEFAULT, sdesc, sval, src, NULL);

	if (show) {
		csch_text_t *text = grp_ref_attr_text(ctx, sym->data.ref.grp, sdesc, 1);
		csch_child_xform_t *xf = calloc(sizeof(csch_child_xform_t), 1);

		csch_vtoid_append(&xf->path.vt, text->hdr.oid);
		xf->movex = csch_alien_coord(&ctx->alien, ox);
		xf->movey = csch_alien_coord(&ctx->alien, oy);
		vtp0_append(&sym->data.ref.child_xform, xf);
	}

	return 0;
}

static int parse_sym(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *spos = PROP(root, "pos");
	const char *sid = PROP(root, "id");
	const char *sscx = PROP(root, "scale_x"), *sscy = PROP(root, "scale_y");
	const char *srot = PROP(root, "rotate");
	csch_cgrp_t *symdef, *sym;
	csch_coord_t bbw, bbh;
	double ox, oy, scx, scy;
	long id, rot;
	int mx, my, dx, dy, swbb;
	csch_sheet_t *sheet = dst;
	static const parser_t parsers[] = {
		{"FIELD", parse_sym_field},
		{NULL, NULL}
	};


	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)   return -1;
	if (parse_long(ctx, root, sid, &id, 1) != 0)        return -1;
	if (parse_double(ctx, root, sscx, &scx, 1) != 0)    return -1;
	if (parse_double(ctx, root, sscy, &scy, 1) != 0)    return -1;
	if (parse_long(ctx, root, srot, &rot, 1) != 0)      return -1;

	if ((scx != 1.0) || (scy != 1.0)) {
		error(root, ("Scaled symbols are not yet supported\n"));
		return -1;
	}

	symdef = htip_get(&sheet->indirect.id2obj, id);
	if (symdef == NULL) {
		error(root, ("Can not find symbol def id %ld\n", id));
		return -1;
	}

	sym = csch_cgrp_ref_alloc(dst, &sheet->direct, csch_oid_new(dst, &sheet->direct));
	if (sym == NULL) {
		error(root, ("Failed to create blank symbol\n"));
		return -1;
	}

	csch_cgrp_update(sheet, symdef, 1);
	bbw = symdef->hdr.bbox.x2 - symdef->hdr.bbox.x1;
	bbh = symdef->hdr.bbox.y2 - symdef->hdr.bbox.y1;

	rot2sym(ctx, root, rot, &sym->spec_rot, &mx, &my, &dx, &dy, &swbb);
	if (swbb)
		rnd_swap(csch_coord_t, bbw, bbh);

	sym->mirx = mx;
	sym->miry = my;
	sym->data.ref.grp = symdef;

	rnd_trace("symdef bbox: %d*%ld %d*%ld\n", dx, bbw, dy, bbh);

	sym->x = csch_alien_coord_x(&ctx->alien, ox) + dx * bbw;
	sym->y = csch_alien_coord_y(&ctx->alien, oy) + dy * bbh;

	/* do not ref-embed here, let postprocessor do that */

	htpp_set(&ctx->sym2xml, sym, root);

	return parse_all(ctx, sym, root, parsers);
}

/*** wires and other drawing objects ***/

static int parse_wire(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	double x1, y1, x2, y2;
	const char *a = PROP(root, "a"), *b = PROP(root, "b");
	csch_sheet_t *sheet = dst;

	if ((parse_coords(ctx, root, a, &x1, &y1) != 0) || (parse_coords(ctx, root, b, &x2, &y2) != 0))
		return -1;

	csch_alien_mknet(&ctx->alien, &sheet->direct, x1, y1, x2, y2);
	return 0;
}

static void create_net_label(read_ctx_t *ctx, csch_line_t *wire, xmlNode *root, const char *textstr, double ox, double oy, int style, int dir)
{
	csch_sheet_t *sheet = ctx->alien.sheet;
	csch_text_t *text;
	csch_source_arg_t *src;
	csch_cgrp_t *parent;
	csch_line_t *line;
	const char *templ;
	long tw, th;
	double w, h, step;

	if (style != 0) {
		parent = csch_cgrp_alloc(sheet, wire->hdr.parent, csch_oid_new(sheet, wire->hdr.parent));
		parent->hdr.floater = 1;
		templ = "%../../A.name%";
	}
	else {
		parent = wire->hdr.parent;
		templ = "%../A.name%";
	}

	text = (csch_text_t *)csch_alien_mktext(&ctx->alien, parent, ox, oy, "wire");
	text->text = rnd_strdup(templ);
	text->dyntext = 1;
	text->hdr.floater = 1;
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&wire->hdr.parent->attr, CSCH_ATP_USER_DEFAULT, "name", textstr, src, NULL);

	if (style == 0) {
		if ((dir == 1) || (dir == 2))
			text->spec_mirx = 1;
		if (dir == 0)
			text->spec_rot = 90;
		else if (dir == 1)
			text->spec_rot = -90;
		return;
	}

	csch_text_update(sheet, text, 1);
	tw = (text->hdr.bbox.x2 - text->hdr.bbox.x1);
	th = (text->hdr.bbox.y2 - text->hdr.bbox.y1);
	w = (double)tw / ctx->alien.coord_factor;
	h = (double)th / ctx->alien.coord_factor;
	step = h / 2;

	switch(style) {
		case 1: /* input: right arrow */
			text->spec_mirx = 1;
			text->spec1.x -= th/2;
			text->spec1.y -= th/2;
			csch_alien_mkline(&ctx->alien, parent, ox, oy, ox-step, oy-step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox, oy, ox-step, oy+step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step, oy+step, ox-step*1.25-w, oy+step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step, oy-step, ox-step*1.25-w, oy-step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step*1.25-w, oy-step, ox-step*1.25-w, oy+step, "sheet-decor");
			break;
		case 2: /* output: left arrow */
			text->spec_mirx = 1;
			text->spec1.y -= th/2;
			csch_alien_mkline(&ctx->alien, parent, ox, oy-step, ox, oy+step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox, oy-step, ox-w-step, oy-step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox, oy+step, ox-w-step, oy+step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-w-step, oy+step, ox-w-step*2, oy, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-w-step, oy-step, ox-w-step*2, oy, "sheet-decor");
			break;
		case 3: /* io: dual arrow */
			text->spec_mirx = 1;
			text->spec1.x -= th/2;
			text->spec1.y -= th/2;
			csch_alien_mkline(&ctx->alien, parent, ox, oy, ox-step, oy-step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox, oy, ox-step, oy+step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step, oy+step, ox-step*1.25-w, oy+step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step, oy-step, ox-step*1.25-w, oy-step, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step*1.25-w, oy-step, ox-step*2.25-w, oy, "sheet-decor");
			csch_alien_mkline(&ctx->alien, parent, ox-step*1.25-w, oy+step, ox-step*2.25-w, oy, "sheet-decor");
			break;
	}
}

static int parse_label(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *spos = PROP(root, "pos"), *sdir = PROP(root, "direction");
	const char *sstyle = PROP(root, "style");
	const char *textstr;
	csch_line_t *wire;
	csch_text_t *text;
	csch_rtree_it_t it;
	csch_rtree_box_t bbox;
	csch_sheet_t *sheet = dst;
	double ox, oy, rot;
	long dir, style;


	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)   return -1;
	if (parse_long(ctx, root, sdir, &dir, 1) != 0)      return -1;
	if (parse_long(ctx, root, sstyle, &style, 1) != 0)  return -1;
	if ((textstr = parse_text(ctx, root, 1)) == NULL)   return -1;
	rot = rot2deg(ctx, root, dir);

	bbox.x1 = csch_alien_coord_x(&ctx->alien, ox) - 1;
	bbox.y1 = csch_alien_coord_y(&ctx->alien, oy) - 1;
	bbox.x2 = bbox.x1 + 2;
	bbox.y2 = bbox.y1 + 2;
	for(wire = csch_rtree_first(&it, &sheet->dsply[CSCH_DSPLY_WIRE], &bbox); wire != NULL; wire = csch_rtree_next(&it))
		if ((wire->hdr.type == CSCH_CTYPE_LINE) && (wire->hdr.parent->role == CSCH_ROLE_WIRE_NET))
			break;

	if (wire == NULL) { /* create text object */
		text = (csch_text_t *)csch_alien_mktext(&ctx->alien, &sheet->direct, ox, oy, "sheet-decor");
		text->text = rnd_strdup(textstr);
		if ((dir == 1) || (dir == 2))
			text->spec_mirx = 1;
		if (dir == 0)
			text->spec_rot = 90;
		else if (dir == 1)
			text->spec_rot = -90;
	}
	else /* create floater */
		create_net_label(ctx, wire, root, textstr, ox, oy, style, dir);

	return 0;
}

static int parse_text_obj_in(read_ctx_t *ctx, void *dst, xmlNode *root, const char *posname, csch_cgrp_t *parent)
{
	const char *spos = PROP(root, posname), *sdir = PROP(root, "direction");
	const char *textstr;
	csch_text_t *text;
	double ox, oy, rot;
	long dir;

	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)   return -1;
	if (parse_long(ctx, root, sdir, &dir, 1) != 0)      return -1;
	if ((textstr = parse_text(ctx, root, 1)) == NULL)   return -1;
	rot = rot2deg(ctx, root, dir);

	text = (csch_text_t *)csch_alien_mktext(&ctx->alien, parent, ox, oy, "sheet-decor");
	text->text = rnd_strdup(textstr);

	if ((dir == 1) || (dir == 2))
		text->spec_mirx = 1;
	if (dir == 0)
		text->spec_rot = 90;
	else if (dir == 1)
		text->spec_rot = -90;

	return 0;
}

static int parse_text_obj(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	csch_sheet_t *sheet = dst;
	return parse_text_obj_in(ctx, dst, root, "pos", &sheet->direct);
}


static int parse_note(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	csch_cgrp_t *grp;
	const char *sa = PROP(root, "a"), *sb = PROP(root, "b");
	int res;
	double x1, y1, x2, y2;
	csch_sheet_t *sheet = dst;

	if (parse_coords(ctx, root, sa, &x1, &y1) != 0)   return -1;
	if (parse_coords(ctx, root, sb, &x2, &y2) != 0)   return -1;

	grp = csch_cgrp_alloc(dst, &sheet->direct, csch_oid_new(dst, &sheet->direct));
	csch_alien_mkrect(&ctx->alien, grp, x1, y1, x2, y2, "sheet-decor", "note-fill");
	res = parse_text_obj_in(ctx, dst, root, "a", grp);

	TODO("Multiline text is not yet supported");

	return res;
}

static csch_cgrp_t *power_sym(read_ctx_t *ctx, void *dst, xmlNode *root, const char *netname)
{
	csch_cgrp_t *sym;
	csch_source_arg_t *src;
	csch_sheet_t *sheet = dst;
	static const char *forge[] = {
		"delete,forge/tmp",
		"scalar,forge/tmp",
		"sub,^,1:,forge/tmp",
		"suba,$,rail,forge/tmp",
		"array,connect",
		"append,connect,forge/tmp",
		NULL
	};

	sym = csch_cgrp_alloc(dst, &sheet->direct, csch_oid_new(dst, &sheet->direct));
	if (sym == NULL) {
		error(root, ("Failed to allocate symbol for power\n"));
		return NULL;
	}
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&sym->attr, CSCH_ATP_USER_DEFAULT, "role", "symbol", src, NULL);
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set(&sym->attr, CSCH_ATP_USER_DEFAULT, "rail", netname, src, NULL);
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_attrib_set_arr_c(&sym->attr, CSCH_ATP_USER_DEFAULT, "forge", forge, src, NULL);

	return sym;
}

static int parse_power(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	const char *netname = parse_text(ctx, root, 0);
	const char *spos = PROP(root, "pos"), *sdir = PROP(root, "direction");
	const char *swhich = PROP(root, "which");
	double ox, oy, ty;
	const double dx = 1, dy = 1;
	long which, dir;
	csch_cgrp_t *sym;
	csch_source_arg_t *src;
	csch_text_t *text;
	csch_alien_read_ctx_t a0 = {0};
	a0.sheet = ctx->alien.sheet;
	a0.coord_factor = ctx->alien.coord_factor;

	if (parse_coords(ctx, root, spos, &ox, &oy) != 0)   return -1;
	if (parse_long(ctx, root, swhich, &which, 1) != 0)  return -1;
	if (parse_long(ctx, root, sdir, &dir, 1) != 0)      return -1;

	sym = power_sym(ctx, dst, root, netname);
	if (sym == NULL)
		return -1;

	sym->spec_rot = rot2deg(ctx, root, dir);
	sym->x = csch_alien_coord_x(&ctx->alien, ox);
	sym->y = csch_alien_coord_y(&ctx->alien, oy);


	/* graphics: terminal */
	src = csch_attrib_src_c(ctx->fn, root->line, 0, NULL);
	csch_alien_mkpin_line(&a0, src, sym, 0, 0, 0, +dy*3);

	/* graphics: text */
	switch(which) {
		case 0: ty = 6; break;
		case 3: ty = 5; break;
	}
	text = (csch_text_t *)csch_alien_mktext(&a0, sym, -8*dx, ty*dy, "sym-primary");
	text->text = rnd_strdup("%../A.rail%");
	text->dyntext = 1;
	text->has_bbox = 1;
	text->spec2.x = csch_alien_coord_x(&a0, 8*dx);
	text->spec2.y = text->spec1.y+3000;
	text->halign = CSCH_HALIGN_CENTER;
	text->spec_rot = -sym->spec_rot;
	text->spec_mirx = (sym->spec_rot >= 180);

	/* graphics: decoration */
	switch(which) {
		case 0: /* T */
			csch_alien_mkline(&a0, sym, -1*dx, +3*dy, +1*dx, +3*dy, "sym-decor");
			break;
		case 3: /* arrow */
			csch_alien_mkline(&a0, sym, -1*dx, +3*dy, +1*dx, +3*dy, "sym-decor");
			csch_alien_mkline(&a0, sym, -1*dx, +3*dy, 0,     +5*dy, "sym-decor");
			csch_alien_mkline(&a0, sym, +1*dx, +3*dy, 0,     +5*dy, "sym-decor");
			break;
		default:
			error(root, ("Failed to create power: unknown style which=%s\n", swhich));
			return -1;
	}
	return 0;
}

/* Finalize attribute visibility: delete inivisible attribute printouts */
static int sym_attr_vis(read_ctx_t *ctx, csch_cgrp_t *sym)
{
	xmlNode *n, *root = htpp_get(&ctx->sym2xml, sym);
	if (root == NULL) {
		rnd_message(RND_MSG_ERROR, "io_tinycad: internal error: no xml node in sym_attr_vis()\n");
		return -1;
	}

	/* check each FIELD and if it's show=0 and has a floater text, child-remove it */
	for(n = root->children; n != NULL; n = n->next) {
		if (xmlStrcmp(n->name, (const unsigned char *)"FIELD") == 0) {
			const char *sdesc = PROP(n, "description"), *sshow = PROP(n, "show");
			int show = parse_bool(ctx, n, sshow);
			if (!show) {
				csch_text_t *text = grp_ref_attr_text(ctx, sym->data.ref.grp, sdesc, 0);
				if (text != NULL) { /* attr text created, remove it */
					csch_child_xform_t *xf = calloc(sizeof(csch_child_xform_t), 1);
					csch_vtoid_append(&xf->path.vt, text->hdr.oid);
					xf->remove = 1;
					vtp0_append(&sym->data.ref.child_xform, xf);
				}
			}
		}
	}

	return 0;
}

static int translate_attrib_strs(read_ctx_t *ctx, csch_cgrp_t *grp, const char **tab)
{
	for(; *tab != NULL; tab += 2) {
		htsp_entry_t *e = htsp_getentry(&grp->attr, tab[0]);
		if (e != NULL) {
			csch_attrib_t *a = e->value;
			csch_source_arg_t *src = csch_attrib_src_c(NULL, 0, 0, e->key);
			csch_attrib_set(&grp->attr, CSCH_ATP_USER_DEFAULT, tab[1], a->val, src, NULL);

			/* csch_attrib_del(); is not good for this as it keeps the attribute and marks it deleted */
			e = htsp_popentry(&grp->attr, tab[0]);
			csch_attr_free(e->value);
		}
	}
}


static int translate_sym_attribs(read_ctx_t *ctx, csch_cgrp_t *sym)
{
	static const char *tab[] = {
		"Package", "footprint",
		NULL, NULL
	};
	translate_attrib_strs(ctx, sym, tab);
	return 0;
}

static int translate_term_attribs(read_ctx_t *ctx, csch_cgrp_t *sym)
{
	rnd_trace("xlate term\n");
	return 0;
}

static int translate_attribs_recurse(read_ctx_t *ctx, csch_cgrp_t *grp)
{
	htip_entry_t *e;

	if ((grp->role == CSCH_ROLE_SYMBOL) || (grp->hdr.type == CSCH_CTYPE_GRP_REF)) {
		if ((grp->hdr.type == CSCH_CTYPE_GRP_REF) && (sym_attr_vis(ctx, grp) != 0))
			return -1;
		if (translate_sym_attribs(ctx, grp) != 0)
			return -1;
	}
	if (grp->role == CSCH_ROLE_TERMINAL) {
		if (translate_term_attribs(ctx, grp) != 0)
			return -1;
	}

	for(e = htip_first(&grp->id2obj); e != NULL; e = htip_next(&grp->id2obj, e)) {
		csch_cgrp_t *child = e->value;
		if (csch_obj_is_grp(&child->hdr)) {
			if (translate_attribs_recurse(ctx, child) != 0)
				return -1;
		}
	}
	return 0;
}

/* Try to convert attributes from the TinyCAD model to the cschem model */
static int translate_attribs(read_ctx_t *ctx, csch_sheet_t *sheet)
{
	return translate_attribs_recurse(ctx, &sheet->indirect) || translate_attribs_recurse(ctx, &sheet->direct);
}

static int parse_sheet(read_ctx_t *ctx, void *dst, xmlNode *root)
{
	int res;
	static const parser_t parsers0[] = { /* read details first so that page size is known (for the mirroring) */
		{"DETAILS", parse_details},
		{NULL, NULL}
	};
	static const parser_t parsers1[] = {
		{"NAME", parse_name},
		{"SYMBOLDEF", parse_symdef},
		{"SYMBOL", parse_sym},
		{"WIRE", parse_wire},
		{"TEXT", parse_text_obj},
		{"NOTE_TEXT", parse_note},
		{"POWER", parse_power},
		{"POLYGON", parse_sheet_polygon},
		{NULL, NULL}
	};
	static const parser_t parsers2[] = {
		{"LABEL", parse_label}, /* labels are placed on wires, best if wires already exist */
		{NULL, NULL}
	};

	res = parse_all(ctx, dst, root, parsers0) || parse_all(ctx, dst, root, parsers1) || parse_all(ctx, dst, root, parsers2);

	if (res != 0)
		return -1;

	return translate_attribs(ctx, dst);
}

/*** file level parsing and entry points ***/
int io_tinycad_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst)
{
	int res = -1;
	read_ctx_t ctx = {0};
	xmlNode *n;

	ctx.alien.sheet = dst;
	ctx.alien.coord_factor = 1000.0;
	ctx.alien.flip_y = 1;
	ctx.fn = fn;
	ctx.doc = xmlReadFile(fn, NULL, 0);

	if (ctx.doc == NULL) {
		rnd_message(RND_MSG_ERROR, "xml parsing error on file %s\n", fn);
		return -1;
	}

	ctx.root = xmlDocGetRootElement(ctx.doc);
	if (xmlStrcmp(ctx.root->name, (xmlChar *)"TinyCADSheets") != 0) {
		rnd_message(RND_MSG_ERROR, "xml error: root is not <TinyCADSheets>\n");
		xmlFreeDoc(ctx.doc);
		return -1;
	}

	htpp_init(&ctx.sym2xml, ptrhash, ptrkeyeq);
	csch_alien_sheet_setup(&ctx.alien, 1);

	for(n = ctx.root->children; n != NULL; n = n->next) {
		if (xmlStrcmp(n->name, (const unsigned char *)"TinyCAD") == 0) {
			res = parse_sheet(&ctx, dst, n);
			TODO("We are really reading a project");
			break;
		}
	}

	htpp_uninit(&ctx.sym2xml);
	xmlFreeDoc(ctx.doc);
	return res;
}

int io_tinycad_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	char line[512], *s;
	int n;

	if (type != CSCH_IOTYP_SHEET)
		return -1;

	s = fgets(line, sizeof(line), f);
	if ((s == NULL) || (strncmp(s, "<?xml", 4) != 0))
		return -1;

	for(n = 0; n < 32; n++) {
		s = fgets(line, sizeof(line), f);
		if (s == NULL)
			return -1;
		if (strstr(s, "<TinyCADSheets>") != NULL)
			return 0;
	}
	return -1;
}
