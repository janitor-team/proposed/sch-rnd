/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - attr. transf. for PCB workflow
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <stdlib.h>
#include <assert.h>

#include <libfungw/fungw.h>
#include <librnd/core/plugins.h>

#include <libcschem/libcschem.h>
#include <libcschem/abstract.h>
#include <libcschem/engine.h>
#include <libcschem/actions_csch.h>
#include <libcschem/attrib.h>

/*** hooks ***/

fgw_error_t target_none_compile_port(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	fgw_obj_t *obj = argv[0].val.argv0.func->obj;
	csch_view_eng_t *eng = obj->script_data;
	csch_aport_t *port;

	CSCH_HOOK_CONVARG(1, FGW_AOBJ, std_cschem_comp_update, port = fgw_aobj(&argv[1]));
	assert(port->hdr.type == CSCH_ATYPE_PORT);

	if (port->name != NULL) {
		csch_source_arg_t *src = csch_attrib_src_p("target_none", NULL);
		csch_attrib_set(&port->hdr.attr, eng->eprio + CSCH_PRI_PLUGIN_NORMAL, "display/name", port->name, src, NULL);
	}

	return 0;
}

static int on_load(fgw_obj_t *obj, const char *filename, const char *opts)
{
	fgw_func_reg(obj, "compile_port", target_none_compile_port);
	obj->script_data = obj->script_user_call_ctx; /* save eng ptr */
	return 0;
}

static const fgw_eng_t fgw_target_none_eng = {
	"target_none",
	csch_c_call_script,
	NULL,
	on_load,
	NULL, /* on_unload */
};

int pplg_check_ver_target_none(int ver_needed) { return 0; }

void pplg_uninit_target_none(void)
{
}

int pplg_init_target_none(void)
{
	RND_API_CHK_VER;

	fgw_eng_reg(&fgw_target_none_eng);
	return 0;
}

