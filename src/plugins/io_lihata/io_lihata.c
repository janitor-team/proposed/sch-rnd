/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (lihata format support)
 *  Copyright (C) 2018,2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include <librnd/core/plugins.h>
#include "read.h"
#include "write.h"

static csch_plug_io_t lhtv1;

static int io_lihata_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "lihata") && !strstr(fmt, "lht"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 100;
	if (type == CSCH_IOTYP_BUFFER)
		return 100;
	if (type == CSCH_IOTYP_PROJECT)
		return 100;
	return 0;
}

static int io_lihata_save_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "lihata") && !strstr(fmt, "lht"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 100;
	if (type == CSCH_IOTYP_BUFFER)
		return 100;
	if (type == CSCH_IOTYP_PROJECT)
		return 100;
	return 0;
}


int pplg_check_ver_io_lihata(int ver_needed) { return 0; }

void pplg_uninit_io_lihata(void)
{
	csch_plug_io_unregister(&lhtv1);
}

int pplg_init_io_lihata(void)
{
	RND_API_CHK_VER;

	lhtv1.name = "lihata schematics sheet v1";
	lhtv1.load_prio = io_lihata_load_prio;
	lhtv1.save_prio = io_lihata_save_prio;
	lhtv1.load_sheet = io_lihata_load_sheet;
	lhtv1.load_buffer = io_lihata_load_buffer;
	lhtv1.load_project = io_lihata_load_project;
	lhtv1.load_grp = io_lihata_load_grp;
	lhtv1.save_sheet = io_lihata_save_sheet;
	lhtv1.save_buffer = io_lihata_save_buffer;
	lhtv1.save_grp = io_lihata_save_grp;
	lhtv1.test_parse = io_lihata_test_parse;

	lhtv1.ext_save_sheet = "rs";
	lhtv1.ext_save_buffer = "lht";
	lhtv1.ext_save_grp = "ry";
	csch_plug_io_register(&lhtv1);
	return 0;
}

