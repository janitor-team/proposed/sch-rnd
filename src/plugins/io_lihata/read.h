/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (lihata format support)
 *  Copyright (C) 2018,2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <libcschem/concrete.h>
#include <libcschem/plug_io.h>

int io_lihata_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
int io_lihata_load_buffer(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
int io_lihata_load_project(FILE *f, const char *fn, const char *fmt, csch_project_t *dst, int with_sheets);
csch_cgrp_t *io_lihata_load_grp(FILE *f, const char *fn, const char *fmt, csch_sheet_t *sheet);
int io_lihata_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);

