/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard cschem attributes
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <genht/htsp.h>
#include <genht/htsi.h>
#include <genht/hash.h>
#include <genht/ht_utils.h>
#include <genvector/vtp0.h>

#include <libfungw/fungw.h>
#include <libcschem/concrete.h>
#include <libcschem/actions_csch.h>
#include <libcschem/attrib.h>
#include <libcschem/libcschem.h>
#include <librnd/core/actions.h>
#include <librnd/core/plugins.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/conf_multi.h>

#include <sch-rnd/funchash_core.h>

#include "renumber_conf.h"
#include "conf_internal.c"

static conf_renumber_t renumber_conf;

static const char renumber_cookie[] = "renumber";

typedef struct renumber_s {
	csch_sheet_t *sheet;
	vtp0_t objs;         /* all objects (eventually sorted array) */
	htsp_t name2sym;     /* key: (const char *)name, val: (csch_cgrp_t *)sym */
	htsi_t next;         /* next integer per prefix */
	int base;            /* start numbering from */
	const char *sorta;
	unsigned selected:1; /* renumber selected only */
} renumber_t;

#include "renumber_impl.c"
#include "renumber_dlg.c"

const char csch_acts_Renumber[] = "Renumber(selected|all, lr|rl|tb|bt, [base])\n";
const char csch_acth_Renumber[] = "";
fgw_error_t csch_act_Renumber(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hl = RND_ACT_DESIGN;
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	int cmd, rv = -1;
	renumber_t rctx = {0};
	const char *sorta;

	rctx.base = 1;

	RND_ACT_CONVARG(1, FGW_KEYWORD, Renumber, cmd = fgw_keyword(&argv[1]));
	RND_ACT_CONVARG(2, FGW_STR, Renumber, sorta = argv[2].val.str);
	RND_ACT_MAY_CONVARG(3, FGW_INT, Renumber, rctx.base = argv[3].val.nat_int);

	htsp_init(&rctx.name2sym, strhash, strkeyeq);
	htsi_init(&rctx.next, strhash, strkeyeq);
	rctx.sheet = sheet;
	rctx.sorta = sorta;

	switch(cmd) {
		case F_Selected:
			rctx.selected = 1;
			break;
		case F_All:
			break;
		default:
			rnd_message(RND_MSG_ERROR, "Renumber: invalid first argument\n");
			goto error;
	}

	renumber_list_objs(&rctx);
	if (rctx.objs.used == 0) {
		rnd_message(RND_MSG_ERROR, "Renumber: no symbols %s to renumber\n", rctx.selected ? "selected" : "on sheet");
		goto error;
	}

	rv = 0;

	if (renumber_sort(&rctx) != 0) {
		rnd_message(RND_MSG_ERROR, "Renumber: invalid sorting algorithm '%s'\n", rctx.sorta);
		goto error;
	}

	renumber_objs(&rctx);
	csch_sheet_set_changed(sheet, 1);

	error:;
	htsp_uninit(&rctx.name2sym);
	genht_uninit_deep(htsi, &rctx.next, {
		free(htent->key);
	});
	vtp0_uninit(&rctx.objs);

	RND_ACT_IRES(rv);
	return 0;
}

const char csch_acts_RenumberDialog[] = "RenumberDialog()\n";
const char csch_acth_RenumberDialog[] = "";
fgw_error_t csch_act_RenumberDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	sch_rnd_dlg_renumdlg();
	return 0;
}

static rnd_action_t renumber_action_list[] = {
	{"Renumber", csch_act_Renumber, csch_acth_Renumber, csch_acts_Renumber},
	{"RenumberDialog", csch_act_RenumberDialog, csch_acth_RenumberDialog, csch_acts_RenumberDialog}
};

int pplg_check_ver_renumber(int ver_needed) { return 0; }

void pplg_uninit_renumber(void)
{
	rnd_remove_actions_by_cookie(renumber_cookie);
	rnd_conf_plug_unreg("plugins/renumber/", renumber_conf_internal, renumber_cookie);
}

int pplg_init_renumber(void)
{
	RND_API_CHK_VER;

	RND_REGISTER_ACTIONS(renumber_action_list, renumber_cookie);

	rnd_conf_plug_reg(renumber_conf, renumber_conf_internal, renumber_cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(renumber_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "renumber_conf_fields.h"

	return 0;
}

