/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - property editor plugin
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <libcschem/config.h>
#include <librnd/core/actions.h>
#include <librnd/core/plugins.h>
#include <librnd/core/conf.h>
#include <librnd/core/error.h>
#include <libcschem/event.h>
#include <libcschem/undo.h>
#include <sch-rnd/font.h>

extern long sch_rnd_font_score_debug;
static const char csch_acts_FontFind[] = "FontFind(name, style)\n";
static const char csch_acth_FontFind[] = "Finds a font by the same name:style combo pens use";
static fgw_error_t csch_act_FontFind(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *name, *style;
	void *font;

	RND_ACT_CONVARG(1, FGW_STR, FontFind, name = argv[1].val.str);
	RND_ACT_CONVARG(2, FGW_STR, FontFind, style = argv[2].val.str);

	sch_rnd_font_score_debug++;
	font = sch_rnd_font_lookup(name, style);
	rnd_message(RND_MSG_INFO, "FontFind: %p\n", font);
	sch_rnd_font_score_debug--;

	RND_ACT_IRES(0);
	return 0;
}

static const char csch_acts_UndoSplit[] = "UndoSplit()\n";
static const char csch_acth_UndoSplit[] = "Renumber undo serials so they can be undone separately";
static fgw_error_t csch_act_UndoSplit(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hl = RND_ACT_DESIGN;
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	uundo_item_t *i;
	uundo_serial_t ser, base = 0, cnt;

	ser = -1;

	for(i = sheet->undo.head; i != NULL; i = i->next) {
		if (ser != i->serial) {
			base += 10000;
			ser = i->serial;
			cnt = 0;
		}
		i->serial = base + cnt;
		cnt++;
	}
	sheet->undo.serial = base + cnt;

	rnd_event(&sheet->hidlib, CSCH_EVENT_UNDO_POST, "i", CSCH_UNDO_EV_UNDO);

	RND_ACT_IRES(0);
	return 0;
}

static const char csch_acts_d1[] = "d1()\n";
static const char csch_acth_d1[] = "debug action for development";
static fgw_error_t csch_act_d1(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	RND_ACT_IRES(0);
	return 0;
}


rnd_action_t diag_action_list[] = {
	{"FontFind", csch_act_FontFind, csch_acth_FontFind, csch_acts_FontFind},
	{"UndoSplit", csch_act_UndoSplit, csch_acth_UndoSplit, csch_acts_UndoSplit},
	{"d1", csch_act_d1, csch_acth_d1, csch_acts_d1}
};

static const char *diag_cookie = "diag plugin";

int pplg_check_ver_diag(int ver_needed) { return 0; }

void pplg_uninit_diag(void)
{
	rnd_remove_actions_by_cookie(diag_cookie);
}

int pplg_init_diag(void)
{
	RND_API_CHK_VER;

	RND_REGISTER_ACTIONS(diag_action_list, diag_cookie)
	return 0;
}
