/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (gEDA format support)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <librnd/core/plugins.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include "read.h"

static csch_plug_io_t altium;

static int io_altium_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "altium") && !strstr(fmt, "chdoc"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 90;
	return 0;
}

int pplg_check_ver_io_altium(int ver_needed) { return 0; }

void pplg_uninit_io_altium(void)
{
	csch_plug_io_unregister(&altium);
}

int pplg_init_io_altium(void)
{
	RND_API_CHK_VER;

	altium.name = "altium schematics sheet from schdoc (cdf)";
	altium.load_prio = io_altium_load_prio;
	altium.load_sheet = io_altium_load_sheet;
	altium.test_parse = io_altium_test_parse;

	altium.ext_save_sheet = "SchDoc";
	csch_plug_io_register(&altium);


	return 0;
}

