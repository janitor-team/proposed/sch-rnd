#ifndef SCH_RND_STD_DEVMAP_CONF_H
#define SCH_RND_STD_DEVMAP_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_LIST search_paths;     /* ordered list of paths that are each recursively searched for devmap files */
		} std_devmap;
	} plugins;
} conf_std_devmap_t;

#endif
