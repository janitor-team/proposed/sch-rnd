/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard device mapper
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* copied from io_lihata write.c */
static int attr_cmp(const void *v1, const void *v2)
{
	csch_attrib_t * const *a1 = v1, * const *a2 = v2;
	return strcmp((*a1)->key, (*a2)->key);
}

/* copied from io_lihata write.c */
static void lht_print_str(gds_t *tmp, const char *s)
{
	if (s == NULL) {
		gds_append_str(tmp, "{}");
		return;
	}
	if (!lht_need_brace(LHT_TEXT, s, 0)) {
		gds_append_str(tmp, s);
		return;
	}
	gds_append(tmp, '{');
	for(; *s != '\0'; s++) {
		if ((*s == '\\') || (*s == '}'))
			gds_append(tmp, '\\');
		gds_append(tmp, *s);
	}
	gds_append(tmp, '}');
}

static char *print_attribs(csch_attribs_t *attr)
{
	gds_t tmp = {0};
	htsp_entry_t *e;
	vtp0_t ord;
	long n;

	memset(&ord, 0, sizeof(ord));

	/* copied from io_lihata write.c */
	for(e = htsp_first(attr); e != NULL; e = htsp_next(attr, e))
		vtp0_append(&ord, e->value);
	if (ord.used > 0) {
		qsort(ord.array, ord.used, sizeof(void *), attr_cmp);
		for(n = 0; n < ord.used; n++) {
			const csch_attrib_t *a = ord.array[n];
			if (a->val != NULL) {
				if (a->prio != CSCH_ATP_USER_DEFAULT) {
					rnd_append_printf(&tmp, "ha:%s = { value=", a->key);
					lht_print_str(&tmp, a->val);
					rnd_append_printf(&tmp, "; prio=%d; }\n", a->prio);
				}
				else {
					rnd_append_printf(&tmp, "%s=", a->key);
					lht_print_str(&tmp, a->val);
					rnd_append_printf(&tmp, "\n");
				}
			}
			else {
				long n;

				if (a->prio != CSCH_ATP_USER_DEFAULT)
					rnd_append_printf(&tmp, "ha:%s = { li:value = {\n", a->key);
				else
					rnd_append_printf(&tmp, "li:%s {\n", a->key);

				for(n = 0; n < a->arr.used; n++) {
					rnd_append_printf(&tmp, " ");
					lht_print_str(&tmp, a->arr.array[n]);
					rnd_append_printf(&tmp, "\n");
				}
				if (a->prio != CSCH_ATP_USER_DEFAULT) {
					rnd_append_printf(&tmp, "}\n");
					rnd_append_printf(&tmp, "prio=%d; }\n", a->prio);
				}
				else
					rnd_append_printf(&tmp, "}\n");
			}
		}
	}
	vtp0_uninit(&ord);
	return tmp.array;
}

static char *devmap_lht_preview_text(csch_sheet_t *sheet, csch_lib_t *src, const char *parametric)
{
	csch_attribs_t *attr = NULL;
	long oid = src->backend_data.lng[0];

	if (oid == 0) {
		/* external lib */
		csch_hook_call_ctx_t cctx = {0};

		cctx.project = (csch_project_t *)sheet->hidlib.project;
		attr = devmap_get_extlib(global_devmap_ctx, src->realpath, &cctx, 0);
	}
	else {
		/* local lib */
		attr = devmap_get_from_loclib(sheet, src->name);
	}

	if (attr == NULL)
		return rnd_strdup("<not found>");

	return print_attribs(attr);
}

