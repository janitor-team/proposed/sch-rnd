/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/cnc_poly.h>

typedef struct {
	int clicked;
	rnd_coord_t x, y;
	csch_cpen_t *stroke, *fill;
} csch_tool_rect_t;

static csch_tool_rect_t csch_tool_rect;

static void tool_rect_init(void)
{
	csch_tool_rect.clicked = 0;
	csch_tool_rect.stroke = NULL;
	csch_tool_rect.fill = NULL;
}

static void tool_rect_uninit(void)
{
}

static void tool_rect_press(rnd_design_t *hl)
{
}

static void new_line(csch_sheet_t *sheet, csch_cpoly_t *poly, rnd_coord_t x1, rnd_coord_t y1, rnd_coord_t x2, rnd_coord_t y2)
{
	csch_coutline_t *dst = csch_vtcoutline_alloc_append(&poly->outline, 1);
	csch_line_t *line = &dst->line;
	line->hdr.type = CSCH_CTYPE_LINE;
	line->spec.p1.x = x1; line->spec.p1.y = y1;
	line->spec.p2.x = x2; line->spec.p2.y = y2;
}

static void tool_rect_release(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	if (csch_tool_rect.clicked) {
		const char *stroke_name, *fill_name;
		csch_cpoly_t *poly = (csch_cpoly_t *)csch_op_create(sheet, &sheet->direct, CSCH_CTYPE_POLY);
		rnd_coord_t x1, y1, x2, y2;

		csch_tool_rect.stroke = SCH_RND_DIRECT_PEN(sheet, tool_rect_stroke, &stroke_name);
		csch_tool_rect.fill = SCH_RND_DIRECT_PEN(sheet, tool_rect_fill, &fill_name);

		x1 = csch_tool_rect.x;
		y1 = csch_tool_rect.y;
		x2 = P2C(sch_rnd_crosshair_x);
		y2 = P2C(sch_rnd_crosshair_y);

		new_line(sheet, poly, x1, y1, x1, y2);
		new_line(sheet, poly, x1, y2, x2, y2);
		new_line(sheet, poly, x2, y2, x2, y1);
		new_line(sheet, poly, x2, y1, x1, y1);

		poly->hdr.stroke_name = csch_comm_str(sheet, stroke_name, 1);
		poly->has_stroke = conf_core.editor.style.tool_rect_has_stroke;
		poly->hdr.fill_name = csch_comm_str(sheet, fill_name, 1);
		poly->has_fill = conf_core.editor.style.tool_rect_has_fill;

		csch_poly_update(sheet, poly, 1);
		csch_sheet_set_changed(sheet, 1);

		csch_tool_rect.clicked = 0;
	}
	else {
		csch_tool_rect.clicked = 1;
		csch_tool_rect.x = P2C(sch_rnd_crosshair_x);
		csch_tool_rect.y = P2C(sch_rnd_crosshair_y);
	}
}

static void tool_rect_adjust_attached_objects(rnd_design_t *hl)
{
}


static void tool_rect_draw_attached(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

TODO("symedit: when editing symbol, use the symbol deco style");
	if (csch_tool_rect.clicked) {
		rnd_coord_t x1, y1, x2, y2, ps;

		if (csch_tool_rect.stroke == NULL)
			csch_tool_rect.stroke = SCH_RND_DIRECT_PEN(sheet, tool_rect_stroke, NULL);
		if (csch_tool_rect.fill == NULL)
			csch_tool_rect.fill = SCH_RND_DIRECT_PEN(sheet, tool_rect_fill, NULL);

		ps = (csch_tool_rect.stroke == NULL) ? C2P(1000) : C2P(csch_tool_rect.stroke->size);
		x1 = C2P(csch_tool_rect.x);
		y1 = C2P(csch_tool_rect.y);
		x2 = sch_rnd_crosshair_x;
		y2 = sch_rnd_crosshair_y;

		rnd_hid_set_line_cap(sch_rnd_crosshair_gc, rnd_cap_round);
		rnd_hid_set_line_width(sch_rnd_crosshair_gc, ps);

		/* draw contour */
		rnd_render->draw_line(sch_rnd_crosshair_gc, x1, y1, x2, y1);
		rnd_render->draw_line(sch_rnd_crosshair_gc, x2, y1, x2, y2);
		rnd_render->draw_line(sch_rnd_crosshair_gc, x2, y2, x1, y2);
		rnd_render->draw_line(sch_rnd_crosshair_gc, x1, y2, x1, y1);
	}
}

static void tool_rect_escape(rnd_design_t *hl)
{
	if (!csch_tool_rect.clicked)
		rnd_tool_select_by_name(hl, "arrow");
	else
		rnd_tool_select_by_name(hl, "rect");
}


/* XPM */
static const char *rect_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 4 1",
"  c #000000",
". c #7A8584",
"X c #6EA5D7",
"O c None",
/* pixels */
"OOOOOOOOOOOOOOOOOOOOO",
"OOO...............OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO.OOOOOOOOOOOOO.OOO",
"OOO...............OOO",
"OOOOOOOOOOOOOOOOOOOOO",
"    OO    O    O     ",
" OOO O OOOO OOOOOO OO",
" OOO O OOOO OOOOOO OO",
" OOO O   OO OOOOOO OO",
"    OO OOOO OOOOOO OO",
" OO OO OOOO OOOOOO OO",
" OOO O OOOO OOOOOO OO",
" OOO O    O    OOO OO",
"OOOOOOOOOOOOOOOOOOOOO"
};


static rnd_tool_t sch_rnd_tool_rect = {
	"rect", NULL, std_tools_cookie, 100, rect_icon, RND_TOOL_CURSOR_NAMED("ul_angle"), 0,
	tool_rect_init,
	tool_rect_uninit,
	tool_rect_press,
	tool_rect_release,
	tool_rect_adjust_attached_objects,
	tool_rect_draw_attached,
	NULL, /* undo */
	NULL, /* redo */
	tool_rect_escape
};
