/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (copied from camv-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

static void tool_wirenet_init(void)
{
	line_init();
	csch_tool_line.dir = CSCH_TOOL_LINEDIR_HV;
	csch_tool_line.stroke = NULL;
	csch_tool_line.stroke_name = NULL;
}

static void tool_wirenet_uninit(void)
{
	line_uninit();
}

static void tool_wirenet_press(rnd_design_t *hl)
{
}

static void tool_wirenet_release(rnd_design_t *hl)
{
	if (line_release(hl)) {
		csch_sheet_t *sheet = (csch_sheet_t *)hl;

		if (line_is_zero_len()) {
			csch_tool_line.clicked = 0;
			return;
		}

		if (csch_tool_line.stroke == NULL)
			csch_tool_line.stroke = SCH_RND_DIRECT_PEN(sheet, tool_wire_stroke, &csch_tool_line.stroke_name);
		line_create_all(hl, &sheet->direct, 1);
		line_continue(hl);
	}
}

static void tool_wirenet_adjust_attached_objects(rnd_design_t *hl)
{
	line_adjust_attached_objects(hl);
}


static void tool_wirenet_draw_attached(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	if (csch_tool_line.stroke == NULL)
		csch_tool_line.stroke = SCH_RND_DIRECT_PEN(sheet, tool_wire_stroke, &csch_tool_line.stroke_name);
	line_draw_attached(hl, &sheet->direct);
}

static void tool_wirenet_escape(rnd_design_t *hl)
{
	if (!csch_tool_line.clicked)
		rnd_tool_select_by_name(hl, "arrow");
	else
		rnd_tool_select_by_name(hl, "wirenet");
}


/* XPM */
static const char *wire_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 4 1",
"  c #000000",
". c #7A8584",
"X c #6EA5D7",
"O c None",
/* pixels */
"OOOOOOOOOOOOOOOOOOOOO",
"OO...OOOOOOOOOOOOOOOO",
"O.....OOOOOOOOOOOOOOO",
"O..XXXXXXXXXXXXXXXOOO",
"O.....OOOOOOOOOOOXOOO",
"OO...OOOOOOOOOOO.X.OO",
"OOOOOOOOOOOOOOO..X..O",
"OOOOOOOOOOOOOOO..X..O",
"OOOOOOOOOOOOOOO.....O",
"OOOOOOOOOOOOOOOO...OO",
"OOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOO",
" OOO O   O   OOO    O",
" OOO OO OO OO OO OOOO",
" O O OO OO OO OO OOOO",
" O O OO OO OO OO OOOO",
" O O OO OO   OOO   OO",
"O   OOO OO OO OO OOOO",
"O   OOO OO OO OO OOOO",
"O   OO   O OO OO    O",
"OOOOOOOOOOOOOOOOOOOOO"
};


static rnd_tool_t sch_rnd_tool_wirenet = {
	"wirenet", NULL, std_tools_cookie, 100, wire_icon, RND_TOOL_CURSOR_NAMED("pencil"), 0,
	tool_wirenet_init,
	tool_wirenet_uninit,
	tool_wirenet_press,
	tool_wirenet_release,
	tool_wirenet_adjust_attached_objects,
	tool_wirenet_draw_attached,
	NULL, /* undo */
	NULL, /* redo */
	tool_wirenet_escape
};
