/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  (icons copied from pcb-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <libcschem/cnc_obj.h>

#include <librnd/core/actions.h>
#include <librnd/hid/tool.h>
#include <sch-rnd/crosshair.h>
#include <sch-rnd/operation.h>
#include <sch-rnd/conf_core.h>


static void csch_tool_lock_notify_mode(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	csch_rtree_box_t box;
	csch_chdr_t *obj;
	int itmp;

	box.x1 = P2C(sch_rnd_crosshair_x) - sch_rnd_slop; box.y1 = P2C(sch_rnd_crosshair_y) - sch_rnd_slop;
	box.x2 = P2C(sch_rnd_crosshair_x) + sch_rnd_slop; box.y2 = P2C(sch_rnd_crosshair_y) + sch_rnd_slop;

	obj = csch_search_first_locked_gui(sheet, &box);
	if (obj != NULL) {
		itmp = 0;
		csch_commprp_modify(sheet, obj, &itmp, NULL, 1);
		return;
	}

	obj = csch_search_first_gui(sheet, &box);
	if (obj != NULL) {
		itmp = 1;
		csch_commprp_modify(sheet, obj, &itmp, NULL, 1);
		return;
	}
}

/* XPM */
static const char *lock_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 2 1",
"  c black",
"X c None",
/* pixels */
"XXXXXXXX     XXXXXXXX",
"XXXXXXX  XXX  XXXXXXX",
"XXXXXXX XXXXX XXXXXXX",
"XXXXXX  XXXXX  XXXXXX",
"XXXXXX XXXXXXX XXXXXX",
"XXXXXX XXXXXXX XXXXXX",
"XXXX             XXXX",
"XXXX XXXXXXXXXXX XXXX",
"XXXX             XXXX",
"XXXX XXXXXXXXXXX XXXX",
"XXXX             XXXX",
"XXXX XXXXXXXXXXX XXXX",
"XXXX             XXXX",
"XXXXXXXXXXXXXXXXXXXXX",
"XX XXXX  XXX  X XX XX",
"XX XXX XX X XXX XX XX",
"XX XXX XX X XXX X XXX",
"XX XXX XX X XXX  XXXX",
"XX XXX XX X XXX X XXX",
"XX XXX XX X XXX XX XX",
"XX   XX  XXX  X XX XX"
};

#define lockIcon_width 16
#define lockIcon_height 16
static unsigned char lockIcon_bits[] = {
   0x00, 0x00, 0xe0, 0x07, 0x30, 0x0c, 0x10, 0x08, 0x18, 0x18, 0x08, 0x10,
   0x08, 0x00, 0xfc, 0x3f, 0x04, 0x20, 0xfc, 0x3f, 0x04, 0x20, 0xfc, 0x3f,
   0x04, 0x20, 0xfc, 0x3f, 0x04, 0x20, 0xfc, 0x3f};
#define lockMask_width 16
#define lockMask_height 16
static unsigned char lockMask_bits[] = {
   0xf0, 0x0f, 0xf0, 0x0f, 0xf8, 0x1f, 0x38, 0x1c, 0x1c, 0x3c, 0x1c, 0x38,
   0x1c, 0x30, 0xfe, 0x7f, 0xfe, 0x7f, 0xfe, 0x7f, 0xfe, 0x7f, 0xfe, 0x7f,
   0xfe, 0x7f, 0xfe, 0x7f, 0xfe, 0x7f, 0xfe, 0x7f};

rnd_tool_t sch_rnd_tool_lock = {
	"lock", NULL, NULL, 100, lock_icon, RND_TOOL_CURSOR_XBM(lockIcon_bits, lockMask_bits), 0,
	NULL,
	NULL,
	csch_tool_lock_notify_mode,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL  /* escape */
};
