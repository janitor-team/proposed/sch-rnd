/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  (copied from pcb-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */



void sch_rnd_tool_remove_notify_mode(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	sch_rnd_op_remove_xy(sheet, P2C(sch_rnd_crosshair_x), P2C(sch_rnd_crosshair_y));
}

/* XPM */
static const char *remove_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c black",
". c gray100",
"X c None",
/* pixels */
"XXX XXXXXXXXXXXX XXXX",
"XXXX XXX    XXX XXXXX",
"XXXXX X      X XXXXXX",
"XXXXXX XX  XX XXXXXXX",
"XXXXX  XX  XX  XXXXXX",
"XXXXX          XXXXXX",
"XXXXXX        XXXXXXX",
"XXXXXXX  XX  XXXXXXXX",
"XXXXXXX  XX XXXXXXXXX",
"XXXXXX X XX  XXXXXXXX",
"XXXX  XX    X  XXXXXX",
"XX  XXXXX  XXXX  XXXX",
"XXXXXXXXXXXXXXXXXXXXX",
"XX     XX     X XXXXX",
"XXX XXX X XXXXX XXXXX",
"XXX XXX X XXXXX XXXXX",
"XXX XXX X XXXXX XXXXX",
"XXX XXX X    XX XXXXX",
"XXX XXX X XXXXX XXXXX",
"XXX XXX X XXXXX XXXXX",
"XX     XX     X     X"
};


static rnd_tool_t sch_rnd_tool_remove = {
	"remove", NULL, std_tools_cookie, 100, remove_icon, RND_TOOL_CURSOR_NAMED("pirate"), 0,
	NULL,
	NULL,
	sch_rnd_tool_remove_notify_mode,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};
