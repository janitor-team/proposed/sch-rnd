/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (copied from camv-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "tool_movecopy.h"

typedef struct {
	csch_coord_t press_x, press_y, press_chx, press_chy;
	int click;
	double click_timeout;
	csch_chdr_t *click_obj;
} tool_arrow_t;

static tool_arrow_t arrow;

static int arrow_is_drag_n_drop()
{
	return (arrow.click_obj != NULL) && (rnd_dtime() > arrow.click_timeout);
}

static void tool_arrow_init(void)
{
}

static void tool_arrow_uninit(void)
{
}

static void tool_arrow_press(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	arrow.press_x = P2C(sch_rnd_crosshair_x);
	arrow.press_y = P2C(sch_rnd_crosshair_y);
	arrow.press_chx = sch_rnd_crosshair_x;
	arrow.press_chy = sch_rnd_crosshair_y;
	arrow.click = 1;
	arrow.click_timeout = rnd_dtime() + ((double)conf_core.editor.click_time / 1000.0);
	arrow.click_obj = sch_rnd_search_obj_at(sheet, arrow.press_x, arrow.press_y, sch_rnd_slop);
}

static void tool_arrow_release(rnd_design_t *hl)
{
	if (arrow.click) {
		csch_sheet_t *sheet = (csch_sheet_t *)hl;
		csch_coord_t x = P2C(sch_rnd_crosshair_x), y = P2C(sch_rnd_crosshair_y);

		if ((arrow.press_x == x) && (arrow.press_y == y))
			sch_rnd_select_click(sheet, x, y);
		else
			sch_rnd_select_box(sheet, arrow.press_x, arrow.press_y, x, y);

		arrow.click = 0;
	}
}

static void tool_arrow_adjust_attached_objects(rnd_design_t *hl)
{
	if (!arrow.click)
		return;

	if (arrow_is_drag_n_drop()) {
		const char *tool_name = rnd_gui->control_is_pressed(rnd_gui) ? "copy" : "move";
		rnd_trace("Drag&drop object; passing on to %s\n", tool_name);
		arrow.click = 0;
		rnd_tool_select_by_name(hl, tool_name);
		tool_movecopy_press_at(hl, arrow.press_chx, arrow.press_chy, arrow.click_obj, 1);
	}
}


static void tool_arrow_draw_attached(rnd_design_t *hl)
{
	if (arrow.click) {
		csch_coord_t x = P2C(sch_rnd_crosshair_x), y = P2C(sch_rnd_crosshair_y);
		if ((arrow.press_x != x) || (arrow.press_y != y)) {

			rnd_hid_set_line_cap(sch_rnd_crosshair_gc, rnd_cap_round);
			rnd_hid_set_line_width(sch_rnd_crosshair_gc, -1);
			rnd_render->draw_rect(sch_rnd_crosshair_gc, arrow.press_chx, arrow.press_chy, sch_rnd_crosshair_x, sch_rnd_crosshair_y);
		}
	}
}

/* XPM */
static const char *arrow_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c #000000",
". c #6EA5D7",
"o c None",
/* pixels */
"oo .. ooooooooooooooo",
"oo .... ooooooooooooo",
"ooo ...... oooooooooo",
"ooo ........ oooooooo",
"ooo ....... ooooooooo",
"oooo ..... oooooooooo",
"oooo ...... ooooooooo",
"ooooo .. ... oooooooo",
"ooooo . o ... ooooooo",
"oooooooooo ... oooooo",
"ooooooooooo .. oooooo",
"oooooooooooo  ooooooo",
"ooooooooooooooooooooo",
"ooo ooo    ooo    ooo",
"oo o oo ooo oo ooo oo",
"oo o oo ooo oo ooo oo",
"o ooo o    ooo    ooo",
"o ooo o ooo oo ooo oo",
"o     o ooo oo ooo oo",
"o ooo o ooo oo ooo oo",
"o ooo o ooo oo ooo oo"
};


static rnd_tool_t sch_rnd_tool_arrow = {
	"arrow", NULL, std_tools_cookie, 50, arrow_icon, RND_TOOL_CURSOR_NAMED("left_ptr"), 0,
	tool_arrow_init,
	tool_arrow_uninit,
	tool_arrow_press,
	tool_arrow_release,
	tool_arrow_adjust_attached_objects,
	tool_arrow_draw_attached,
	NULL, /* undo */
	NULL, /* redo */
	NULL, /* escape */
};
