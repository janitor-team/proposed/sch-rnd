/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  (icons copied from pcb-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/core/actions.h>
#include <librnd/hid/tool.h>
#include <sch-rnd/crosshair.h>
#include <sch-rnd/operation.h>
#include <sch-rnd/conf_core.h>


void csch_tool_rotate_notify_mode(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	sch_rnd_op_rotate90_xy(sheet, P2C(sch_rnd_crosshair_x), P2C(sch_rnd_crosshair_y),
		rnd_gui->shift_is_pressed(rnd_gui) ? 3 : 1);
	csch_sheet_set_changed(sheet, 1);
}

/* XPM */
static const char *rot_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 4 1",
"  c black",
". c #6EA5D7",
"X c gray100",
"o c None",
/* pixels */
"ooooooooooo.ooooooooo",
"oooooooooo..ooooooooo",
"ooooooooo....oooooooo",
"oooooooooo..o.ooooooo",
"ooooooooooo.oo.oooooo",
"oooooooooooooo.oooooo",
"oooooooooooooo.oooooo",
"oooooooooooooo.oooooo",
"oooooooooooooo.oooooo",
"ooooooooooooo.ooooooo",
"oooooooooooo.oooooooo",
"oooooooooo..ooooooooo",
"ooooooooooooooooooooo",
"ooo    ooo   oo     o",
"ooo ooo o ooo ooo ooo",
"ooo ooo o ooo ooo ooo",
"ooo    oo ooo ooo ooo",
"ooo   ooo ooo ooo ooo",
"ooo o  oo ooo ooo ooo",
"ooo oo  o ooo ooo ooo",
"ooo ooo oo   oooo ooo"
};

#define rotateIcon_width 16
#define rotateIcon_height 16
static unsigned char rotateIcon_bits[] = {
   0xf0, 0x03, 0xf8, 0x87, 0x0c, 0xcc, 0x06, 0xf8, 0x03, 0xb0, 0x01, 0x98,
   0x00, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x3f, 0x00, 0x19, 0x80, 0x0d, 0xc0,
   0x1f, 0x60, 0x3b, 0x30, 0xe1, 0x3f, 0xc0, 0x0f};

#define rotateMask_width 16
#define rotateMask_height 16
static unsigned char rotateMask_bits[] = {
   0xf0, 0x03, 0xf8, 0x87, 0x0c, 0xcc, 0x06, 0xf8, 0x03, 0xf0, 0x01, 0xf8,
   0x00, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x3f, 0x00, 0x1f, 0x80, 0x0f, 0xc0,
   0x1f, 0x60, 0x3b, 0x30, 0xe1, 0x3f, 0xc0, 0x0f};

rnd_tool_t sch_rnd_tool_rotate = {
	"rotate", NULL, NULL, 100, rot_icon, RND_TOOL_CURSOR_XBM(rotateIcon_bits, rotateMask_bits), 0,
	NULL,
	NULL,
	csch_tool_rotate_notify_mode,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL, /* escape */
};
