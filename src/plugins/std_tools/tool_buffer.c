/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  (icons copied from pcb-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/core/actions.h>
#include <librnd/hid/tool.h>

#include <libcschem/concrete.h>

#include <sch-rnd/conf_core.h>
#include <sch-rnd/crosshair.h>
#include <sch-rnd/buffer.h>
#include <sch-rnd/draw_xor.h>


static void buffer_init(void)
{
	TODO("check if buffer_number is in range and fix it if not");
}

static void buffer_uninit(void)
{
}

static void buffer_notify_mode(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	if (rnd_gui->shift_is_pressed(rnd_gui)) {
		rnd_actionva(hl, "ReplaceSymbol", "object", "", NULL);
		return;
	}

	sch_rnd_buffer_paste(sheet, SCH_RND_PASTEBUFFER, P2C(sch_rnd_crosshair_x), P2C(sch_rnd_crosshair_y));
}

static void buffer_draw_attached(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	sch_rnd_xor_draw_buffer(sheet, SCH_RND_PASTEBUFFER, P2C(sch_rnd_crosshair_x), P2C(sch_rnd_crosshair_y), sch_rnd_crosshair_gc, 1, 0);
}

/* XPM */
static const char *buf_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c #000000",
". c #7A8584",
"o c None",
/* pixels */
"oooooooo  oo  ooooooo",
"oooooo.. o  o ..ooooo",
"oooooooo oooo ooooooo",
"oooooo.. oooo ..ooooo",
"oooooooo oooo ooooooo",
"oooooo.. oooo ..ooooo",
"oooooooo oooo ooooooo",
"oooooo.. oooo ..ooooo",
"oooooooo oooo ooooooo",
"oooooo.. oooo ..ooooo",
"oooooooo      ooooooo",
"ooooooooooooooooooooo",
"oo     oo ooo o     o",
"ooo ooo o ooo o ooooo",
"ooo ooo o ooo o ooooo",
"ooo ooo o ooo o    oo",
"ooo    oo ooo o ooooo",
"ooo ooo o ooo o ooooo",
"ooo ooo o ooo o ooooo",
"oo     ooo   oo ooooo",
"ooooooooooooooooooooo"
};

rnd_tool_t sch_rnd_tool_buffer = {
	"buffer", NULL, NULL, 100, buf_icon, RND_TOOL_CURSOR_NAMED("hand"), 0,
	buffer_init,
	buffer_uninit,
	buffer_notify_mode,
	NULL, /* release */
	NULL, /* adjust */
	buffer_draw_attached,
	NULL,
	NULL,
	NULL, /* escape */
	
	0
};
