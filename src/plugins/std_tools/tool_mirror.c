/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/core/actions.h>
#include <librnd/hid/tool.h>
#include <sch-rnd/crosshair.h>
#include <sch-rnd/operation.h>
#include <sch-rnd/conf_core.h>


static void csch_tool_mirror_notify_mode(rnd_design_t *hl, int mirx, int miry)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	csch_rtree_box_t box;
	csch_chdr_t *obj;

	box.x1 = P2C(sch_rnd_crosshair_x) - sch_rnd_slop; box.y1 = P2C(sch_rnd_crosshair_y) - sch_rnd_slop;
	box.x2 = P2C(sch_rnd_crosshair_x) + sch_rnd_slop; box.y2 = P2C(sch_rnd_crosshair_y) + sch_rnd_slop;

	obj = csch_search_first_gui(sheet, &box);
	if (obj != NULL) {
		csch_mirror(sheet, obj, P2C(sch_rnd_crosshair_x), P2C(sch_rnd_crosshair_y), mirx, miry, 1);
		return;
	}
}

static void csch_tool_xmirror_notify_mode(rnd_design_t *hl)
{
	csch_tool_mirror_notify_mode(hl, 1, 0);
}

static void csch_tool_ymirror_notify_mode(rnd_design_t *hl)
{
	csch_tool_mirror_notify_mode(hl, 0, 1);
}

/* XPM */
static const char *xmirror_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c #000000",
". c #6EA5D7",
"o c None",
/* pixels */
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"oooooo  oooo  ooooooo",
"ooooo . oooo . oooooo",
"oooo ..      .. ooooo",
"ooo ............ oooo",
"ooo ............ oooo",
"ooo  ..      .. ooooo",
"oooo  . oooo . oooooo",
"ooooo   oooo  ooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
" ooo o ooo o   o    o",
" ooo o  o  oo oo oo o",
"  o  o     oo oo oo o",
"o   oo o o oo oo    o",
"o   oo ooo oo oo o oo",
"  o  o ooo oo oo oo o",
" ooo o ooo oo oo oo o",
" ooo o ooo o   o oo o"
};

/* XPM */
static const char *ymirror_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c #000000",
". c #6EA5D7",
"o c None",
/* pixels */
"ooooooooo  oooooooooo",
"oooooooo .. ooooooooo",
"ooooooo .... oooooooo",
"oooooo ...... ooooooo",
"oooooo   ..   ooooooo",
"oooooooo .. ooooooooo",
"oooooooo .. ooooooooo",
"oooooo   ..   ooooooo",
"oooooo ...... ooooooo",
"ooooooo .... oooooooo",
"oooooooo .. ooooooooo",
"ooooooooo  oooooooooo",
"ooooooooooooooooooooo",
" ooo o ooo o   o    o",
" ooo o  o  oo oo oo o",
"  o  o     oo oo oo o",
"o   oo o o oo oo    o",
"oo ooo ooo oo oo o oo",
"oo ooo ooo oo oo oo o",
"oo ooo ooo oo oo oo o",
"oo ooo ooo o   o oo o"
};


rnd_tool_t sch_rnd_tool_xmirror = {
	"xmirror", NULL, NULL, 100, xmirror_icon, RND_TOOL_CURSOR_NAMED("iron_cross"), 0,
	NULL,
	NULL,
	csch_tool_xmirror_notify_mode,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL  /* escape */
};

rnd_tool_t sch_rnd_tool_ymirror = {
	"ymirror", NULL, NULL, 100, ymirror_icon, RND_TOOL_CURSOR_NAMED("iron_cross"), 0,
	NULL,
	NULL,
	csch_tool_ymirror_notify_mode,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL  /* escape */
};
