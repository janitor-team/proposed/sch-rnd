/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/cnc_arc.h>

typedef struct {
	int clicked;
	rnd_coord_t x, y, r;
	csch_cpen_t *stroke;
} csch_tool_circle_t;

static csch_tool_circle_t csch_tool_circle;

static void tool_circle_init(void)
{
	csch_tool_circle.clicked = 0;
	csch_tool_circle.r = 0;
	csch_tool_circle.stroke = NULL;
}

static void tool_circle_uninit(void)
{
}

static void tool_circle_press(rnd_design_t *hl)
{
}

static void tool_circle_release(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	if (csch_tool_circle.clicked) {
		const char *stroke_name;
		csch_arc_t *arc = (csch_arc_t *)csch_op_create(sheet, &sheet->direct, CSCH_CTYPE_ARC);

		arc->spec.c.x = csch_tool_circle.x;
		arc->spec.c.y = csch_tool_circle.y;
		arc->spec.r = csch_tool_circle.r;
		arc->spec.start = 0;
		arc->spec.delta = 360 / RND_RAD_TO_DEG;

		csch_tool_circle.stroke = SCH_RND_DIRECT_PEN(sheet, tool_circle_stroke, &stroke_name);

		arc->hdr.stroke_name = csch_comm_str(sheet, stroke_name, 1);
		csch_arc_update(sheet, arc, 1);
		csch_sheet_set_changed(sheet, 1);

		csch_tool_circle.clicked = 0;

	}
	else {
		csch_tool_circle.clicked = 1;
		csch_tool_circle.x = P2C(sch_rnd_crosshair_x);
		csch_tool_circle.y = P2C(sch_rnd_crosshair_y);
	}
}

static void tool_circle_adjust_attached_objects(rnd_design_t *hl)
{
}


static void tool_circle_draw_attached(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

TODO("symedit: when editing symbol, use the symbol deco style");
	if (csch_tool_circle.clicked) {
		double dx, dy;
		rnd_coord_t ps;

		if (csch_tool_circle.stroke == NULL)
			csch_tool_circle.stroke = SCH_RND_DIRECT_PEN(sheet, tool_circle_stroke, NULL);

		ps = (csch_tool_circle.stroke == NULL) ? C2P(1000) : C2P(csch_tool_circle.stroke->size);
		dx = csch_tool_circle.x - P2C(sch_rnd_crosshair_x);
		dy = csch_tool_circle.y - P2C(sch_rnd_crosshair_y);

		csch_tool_circle.r = rnd_round(sqrt(dx*dx + dy * dy));

		rnd_hid_set_line_cap(sch_rnd_crosshair_gc, rnd_cap_round);
		rnd_hid_set_line_width(sch_rnd_crosshair_gc, -1);

		/* mark center */
		rnd_render->draw_line(sch_rnd_crosshair_gc,
			C2P(csch_tool_circle.x - 200), C2P(csch_tool_circle.y),
			C2P(csch_tool_circle.x + 200), C2P(csch_tool_circle.y));

		rnd_render->draw_line(sch_rnd_crosshair_gc,
			C2P(csch_tool_circle.x), C2P(csch_tool_circle.y - 200),
			C2P(csch_tool_circle.x), C2P(csch_tool_circle.y + 200));

		/* draw contour */
		rnd_hid_set_line_width(sch_rnd_crosshair_gc, ps);

		rnd_render->draw_arc(sch_rnd_crosshair_gc,
			C2P(csch_tool_circle.x), C2P(csch_tool_circle.y),
			C2P(csch_tool_circle.r), C2P(csch_tool_circle.r),
			0, 360);
	}

}

static void tool_circle_escape(rnd_design_t *hl)
{
	if (!csch_tool_circle.clicked)
		rnd_tool_select_by_name(hl, "arrow");
	else
		rnd_tool_select_by_name(hl, "circle");
}


/* XPM */
static const char *circle_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 4 1",
"  c #000000",
". c #7A8584",
"X c #6EA5D7",
"O c None",
/* pixels */
"OOOOOOOOO...OOOOOOOOO",
"OOOOOOO..OOO..OOOOOOO",
"OOOOOO.OOOOOOO.OOOOOO",
"OOOOOO.OOOOOOO.OOOOOO",
"OOOOO.OOOOxOOOO.OOOOO",
"OOOOO.OOOxxxOOO.OOOOO",
"OOOOO.OOOOxOOOO.OOOOO",
"OOOOOO.OOOOOOO.OOOOOO",
"OOOOOO.OOOOOOO.OOOOOO",
"OOOOOOO..OOO..OOOOOOO",
"OOOOOOOOO...OOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOO",
"    O   O    OOO    O",
" OOOOO OO OOO OO OOOO",
" OOOOO OO OOO OO OOOO",
" OOOOO OO OOO OO OOOO",
" OOOOO OO    OOO OOOO",
" OOOOO OO OO OOO OOOO",
" OOOOO OO OOO OO OOOO",
"    O   O OOO OO    O",
"OOOOOOOOOOOOOOOOOOOOO"
};


static rnd_tool_t sch_rnd_tool_circle = {
	"circle", NULL, std_tools_cookie, 100, circle_icon, RND_TOOL_CURSOR_NAMED("question_arrow"), 0,
	tool_circle_init,
	tool_circle_uninit,
	tool_circle_press,
	tool_circle_release,
	tool_circle_adjust_attached_objects,
	tool_circle_draw_attached,
	NULL, /* undo */
	NULL, /* redo */
	tool_circle_escape
};
