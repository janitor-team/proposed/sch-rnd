/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  (icons copied from pcb-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/core/actions.h>
#include <librnd/hid/tool.h>
#include <sch-rnd/crosshair.h>
#include <sch-rnd/operation.h>
#include <sch-rnd/conf_core.h>

typedef struct {
	csch_cpen_t *stroke;
} csch_tool_text_t;

static csch_tool_text_t csch_tool_text;

static void tool_text_init(void)
{
	csch_tool_text.stroke = NULL;
}

void sch_tool_text_notify_mode(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	char *new_str = rnd_hid_prompt_for(hl, "Enter text:", "", "text");

	if ((new_str != NULL) && (*new_str != '\0')) {
		const char *stroke_name;
		csch_text_t *text = (csch_text_t *)csch_op_create(sheet, &sheet->direct, CSCH_CTYPE_TEXT);

		text->spec1.x = P2C(sch_rnd_crosshair_x);
		text->spec1.y = P2C(sch_rnd_crosshair_y);
		text->text = new_str;

		csch_tool_text.stroke = SCH_RND_DIRECT_PEN(sheet, tool_text_stroke, &stroke_name);

		text->hdr.stroke_name = csch_comm_str(sheet, stroke_name, 1);
		csch_text_update(sheet, text, 1);
		csch_sheet_set_changed(sheet, 1);
		/* do not free new_str: owner is text now */
	}
}

void sch_tool_text_draw_attached(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	rnd_coord_t x1, y1, x2, y2, d, h;

	if (csch_tool_text.stroke == NULL)
		csch_tool_text.stroke = SCH_RND_DIRECT_PEN(sheet, tool_text_stroke, NULL);

	h = csch_tool_text.stroke->font_height;
	if (h <= 0)
		h = 1000;

	d = C2P(h);
	x1 = sch_rnd_crosshair_x;
	y1 = sch_rnd_crosshair_y;
	x2 = x1 + d;
	y2 = y1 + d;

	rnd_hid_set_line_cap(sch_rnd_crosshair_gc, rnd_cap_round);
	rnd_hid_set_line_width(sch_rnd_crosshair_gc, -1);

	rnd_render->draw_line(sch_rnd_crosshair_gc, x1, y1, x2, y2);
	rnd_render->draw_line(sch_rnd_crosshair_gc, x1, y2, x2, y1);
	rnd_render->draw_line(sch_rnd_crosshair_gc, x1, y1, x2, y1);
	rnd_render->draw_line(sch_rnd_crosshair_gc, x2, y1, x2, y2);
	rnd_render->draw_line(sch_rnd_crosshair_gc, x2, y2, x1, y2);
	rnd_render->draw_line(sch_rnd_crosshair_gc, x1, y2, x1, y1);

	/* baseline */
	rnd_render->draw_line(sch_rnd_crosshair_gc, x1 + d/8, y1 + d/4, x2 - d/8, y1 + d/4);
}

/* XPM */
static const char *text_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c #000000",
". c #6EA5D7",
"o c None",
/* pixels */
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"oo.ooo.ooo.ooo.ooo.oo",
"o.o.o.o.o.o.o.o.o.o.o",
"oo.ooo.ooo.ooo.ooo.oo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"ooooooooooooooooooooo",
"     o   o ooo o     ",
"oo ooo ooo ooo ooo oo",
"oo ooo oooo o oooo oo",
"oo ooo ooooo ooooo oo",
"oo ooo  ooo o oooo oo",
"oo ooo ooo ooo ooo oo",
"oo ooo ooo ooo ooo oo",
"oo ooo   o ooo ooo oo",
"ooooooooooooooooooooo"
};

rnd_tool_t sch_rnd_tool_text = {
	"text", NULL, NULL, 100, text_icon, RND_TOOL_CURSOR_NAMED("xterm"), 0,
	tool_text_init,
	NULL,
	sch_tool_text_notify_mode,
	NULL,
	NULL,
	sch_tool_text_draw_attached,
	NULL,
	NULL,
	NULL, /* escape */
	
	0
};

