/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard tools
 *  Copyright (C) 2020,2022 Tibor 'Igor2' Palinkas
 *  (copied from camv-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <string.h>

#include <librnd/core/global_typedefs.h>
#include <librnd/core/hidlib.h>
#include <librnd/hid/tool.h>
#include <librnd/core/plugins.h>

#include <libcschem/search.h>

#include <sch-rnd/draw.h>
#include <sch-rnd/draw_xor.h>
#include <sch-rnd/style.h>
#include <sch-rnd/select.h>
#include <sch-rnd/operation.h>
#include <sch-rnd/search.h>

static const char std_tools_cookie[] = "std_tools";

#include "line_common.c"
#include "tool_circle.c"
#include "tool_rect.c"
#include "tool_wirenet.c"
#include "tool_line.c"
#include "tool_arrow.c"
#include "tool_remove.c"
#include "tool_rotate.c"
#include "tool_movecopy.c"
#include "tool_text.c"
#include "tool_lock.c"
#include "tool_mirror.c"
#include "tool_buffer.c"


int pplg_check_ver_std_tools(int ver_needed) { return 0; }

void pplg_uninit_std_tools(void)
{
}

int pplg_init_std_tools(void)
{
	RND_API_CHK_VER;

	rnd_tool_reg(&sch_rnd_tool_arrow, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_circle, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_line, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_text, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_rect, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_wirenet, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_remove, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_buffer, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_rotate, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_lock, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_xmirror, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_ymirror, std_tools_cookie);

	rnd_tool_reg(&sch_rnd_tool_move, std_tools_cookie);
	rnd_tool_reg(&sch_rnd_tool_copy, std_tools_cookie);

	return 0;
}
