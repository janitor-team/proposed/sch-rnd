/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - object access for scripts
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas in pcb-rnd
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <libcschem/config.h>

#include <librnd/core/actions.h>
#include <librnd/core/plugins.h>
#include <librnd/core/misc_util.h>

#include <libcschem/concrete.h>
#include <libcschem/oidpath.h>

#include "keywords_sphash.h"

#include "act_idpath.c"

static const char csch_acts_GetValue[] = "GetValue(input, units, relative, default_unit)";
static const char csch_acth_GetValue[] = "Convert a coordinate value. Returns an unitless double or FGW_ERR_ARG_CONV. The 3rd parameter controls whether to require relative coordinates (+- prefix). Wraps rnd_get_value_ex().";
static fgw_error_t csch_act_GetValue(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *input, *units, *def_unit;
	int relative, a;
	double v;
	rnd_bool success;

	RND_ACT_CONVARG(1, FGW_STR, GetValue, input = argv[1].val.str);
	RND_ACT_CONVARG(2, FGW_STR, GetValue, units = argv[2].val.str);
	RND_ACT_CONVARG(3, FGW_INT, GetValue, relative = argv[3].val.nat_int);
	RND_ACT_CONVARG(4, FGW_STR, GetValue, def_unit = argv[1].val.str);

	if (*units == '\0')
		units = NULL;

	v = rnd_get_value_ex(input, units, &a, NULL, def_unit, &success);
	if (!success || (relative && a))
		return FGW_ERR_ARG_CONV;

	res->type = FGW_DOUBLE;
	res->val.nat_double = v;
	return 0;
}

rnd_action_t act_read_action_list[] = {
	{"GetValue", csch_act_GetValue, csch_acth_GetValue, csch_acts_GetValue},
	{"IDPList", csch_act_IDPList, csch_acth_IDPList, csch_acts_IDPList},
	{"IDP", csch_act_IDP, csch_acth_IDP, csch_acts_IDP},
	{"GetParentGrp", csch_act_GetParentGrp, csch_acth_GetParentGrp, csch_acts_GetParentGrp},
	{"GetObjType", csch_act_GetObjType, csch_acth_GetObjType, csch_acts_GetObjType}
};

static const char *act_read_cookie = "act_read";

int pplg_check_ver_act_read(int ver_needed) { return 0; }

void pplg_uninit_act_read(void)
{
	rnd_remove_actions_by_cookie(act_read_cookie);
}

int pplg_init_act_read(void)
{
	RND_API_CHK_VER;
	RND_REGISTER_ACTIONS(act_read_action_list, act_read_cookie)
	return 0;
}
