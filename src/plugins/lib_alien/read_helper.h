/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - alien file format helpers
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/concrete.h>

typedef struct csch_alien_read_ctx_s {
	csch_sheet_t *sheet;
	double coord_factor;
	double ox, oy;        /* in original format coords */
	unsigned flip_x:1;
	unsigned flip_y:1;

	/* print errors only once */
	unsigned warned_coord_range:1;
} csch_alien_read_ctx_t;

/* Set up ctx->sheet:
   - if pen is non-zero, copy default pens
*/
void csch_alien_sheet_setup(csch_alien_read_ctx_t *ctx, int pen);

/* Return the group attributes should be set in for obj; may create a new
   group around obj. */
csch_cgrp_t *csch_alien_attr_grp(csch_alien_read_ctx_t *ctx, csch_chdr_t **obj);

/* If obj is not a group, create a group around it and return it (else
   return obj). Useful when attributes are to be attached. Obj ptr
   may change in the process (when mvoed into a new group). */
csch_cgrp_t *csch_alien_convert_to_grp(csch_alien_read_ctx_t *ctx, csch_chdr_t **obj);

csch_chdr_t *csch_alien_mknet(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, double x1, double y1, double x2, double y2);
csch_chdr_t *csch_alien_mkline(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, double x1, double y1, double x2, double y2, const char *penname);
csch_chdr_t *csch_alien_mkarc(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, double cx, double cy, double r, double sa, double da, const char *penname);
csch_chdr_t *csch_alien_mkrect(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, double x1, double y1, double x2, double y2, const char *stroke_penname, const char *fill_penname);
csch_chdr_t *csch_alien_mktext(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, double x, double y, const char *penname);

csch_chdr_t *csch_alien_mkpoly(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, const char *stroke_name, const char *fill_name);
void csch_alien_append_poly_line(csch_alien_read_ctx_t *ctx, csch_chdr_t *poly, double x1, double y1, double x2, double y2);
void csch_alien_append_poly_arc(csch_alien_read_ctx_t *ctx, csch_chdr_t *poly_, double cx, double cy, double r, double sa, double da);
void csch_alien_append_poly_bezier(csch_alien_read_ctx_t *ctx, csch_chdr_t *poly_, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);

/* Frees src */
csch_chdr_t *csch_alien_mkpin_line(csch_alien_read_ctx_t *ctx, csch_source_arg_t *src, csch_cgrp_t *parent, double x1, double y1, double x2, double y2);

void csch_alien_mkbezier(csch_alien_read_ctx_t *ctx, csch_cgrp_t *parent, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, const char *penname);


/* Convert coordinate to cschem model */
csch_coord_t csch_alien_coord_x(csch_alien_read_ctx_t *ctx, double crd);
csch_coord_t csch_alien_coord_y(csch_alien_read_ctx_t *ctx, double crd);
csch_coord_t csch_alien_coord(csch_alien_read_ctx_t *ctx, double crd);
