/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* The preferences dialog, application specific tabs */

#include <libcschem/config.h>

#include <librnd/plugins/lib_hid_common/dlg_pref.h>

#undef  PREF_TAB
#define PREF_TAB 0
#include "dlg_pref_general.c"

#undef  PREF_TAB
#define PREF_TAB 1
#include "dlg_pref_sheet.c"

#undef  PREF_TAB
#define PREF_TAB 2
#include "dlg_pref_lib.c"

#undef  PREF_TAB
#define PREF_TAB 3
#include "dlg_pref_color.c"

int sch_dlg_pref_tab = PREF_TAB;
void (*sch_dlg_pref_first_init)(pref_ctx_t *ctx, int tab) = PREF_INIT_FUNC;

