/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  Copied from pcb-rnd, interactive printed circuit board design
 *  Copyright (C) 2018,2021 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Preferences dialog, sheet tab */

#include <libcschem/config.h>
#include <librnd/core/conf.h>
#include <librnd/core/event.h>
#include <librnd/core/actions.h>
#include <librnd/plugins/lib_hid_common/dlg_pref.h>
#include <libcschem/concrete.h>
#include "dlg_pen.h"
#include <sch-rnd/conf_core.h>

typedef struct {
	int wname, wthermscale, wtype;
} pref_sheet_t;

#define RND_EMPTY(a)           ((a) ? (a) : "")

#undef DEF_TABDATA
#define DEF_TABDATA pref_sheet_t *tabdata = PREF_TABDATA(ctx)

/* Actual sheet meta to dialog box */
static void pref_sheet_brd2dlg(pref_ctx_t *ctx, rnd_design_t *dsg)
{
	rnd_design_t *hl = rnd_multi_get_current();
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	DEF_TABDATA;

#ifdef HIDLIB_NAME
	RND_DAD_SET_VALUE(ctx->dlg_hid_ctx, tabdata->wname, str, RND_EMPTY(hl->name));
#endif

	RND_DAD_SET_VALUE(ctx->dlg_hid_ctx, tabdata->wtype, str, (sheet->is_symbol ? "symbol" : "schematics sheet"));
}

/* Dialog box to actual sheet meta */
static void pref_sheet_dlg2brd(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_design_t *hl = rnd_gui->get_dad_design(hid_ctx);
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	int changed = 0;

#ifdef HIDLIB_NAME
	pref_ctx_t *ctx = caller_data;
	DEF_TABDATA;
	const char *newname, *oldname;
	newname = RND_EMPTY(ctx->dlg[tabdata->wname].val.str);
	oldname = RND_EMPTY(hl->name);
	if (strcmp(oldname, newname) != 0) {
		free(hl->name);
		hl->name = rnd_strdup(newname);
		rnd_event(hl, RND_EVENT_DESIGN_FN_CHANGED, NULL);
		changed = 1;
	}
#endif

	if (changed) {
		sheet->changed = 1;
		rnd_event(hl, RND_EVENT_DESIGN_META_CHANGED, NULL); /* always generate the event to make sure visible changes are flushed */
	}
}

static void pref_sheet_edit_attr(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_design_t *hl = rnd_multi_get_current();
	rnd_actionva(hl, "Propedit", "sheet", NULL);
}

static void pref_sheet_pen(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_design_t *hl = rnd_multi_get_current();
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	sch_rnd_pen_dlg(sheet, &sheet->direct, NULL, 0, 0);
}

void csch_dlg_pref_sheet_create(pref_ctx_t *ctx, rnd_design_t *dsg)
{
	rnd_design_t *hl = rnd_multi_get_current();
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	DEF_TABDATA;

	RND_DAD_BEGIN_TABLE(ctx->dlg, 2);
#ifdef HIDLIB_NAME
		RND_DAD_LABEL(ctx->dlg, "Sheet name");
		RND_DAD_STRING(ctx->dlg);
			tabdata->wname = RND_DAD_CURRENT(ctx->dlg);
			ctx->dlg[tabdata->wname].val.str = rnd_strdup(RND_EMPTY(hl->name));
			RND_DAD_CHANGE_CB(ctx->dlg, pref_sheet_dlg2brd);
#endif
		RND_DAD_LABEL(ctx->dlg, "Type");
		RND_DAD_LABEL(ctx->dlg, "");
			tabdata->wtype = RND_DAD_CURRENT(ctx->dlg);
			ctx->dlg[tabdata->wtype].name = rnd_strdup((sheet->is_symbol ? "symbol" : "schematics sheet"));
			RND_DAD_CHANGE_CB(ctx->dlg, pref_sheet_dlg2brd);
		RND_DAD_LABEL(ctx->dlg, "Sheet attributes");
		RND_DAD_BUTTON(ctx->dlg, "Edit...");
			RND_DAD_CHANGE_CB(ctx->dlg, pref_sheet_edit_attr);
		RND_DAD_LABEL(ctx->dlg, "Sheet pens");
		RND_DAD_BUTTON(ctx->dlg, "Edit...");
			RND_DAD_CHANGE_CB(ctx->dlg, pref_sheet_pen);
		RND_DAD_END(ctx->dlg);
}

static const rnd_pref_tab_hook_t pref_sheet = {
	"Sheet meta", RND_PREFTAB_AUTO_FREE_DATA,
	NULL, NULL,
	csch_dlg_pref_sheet_create,
	pref_sheet_brd2dlg, pref_sheet_brd2dlg  /* sheet change, meta change */
};

static void sch_dlg_pref_sheet_init(pref_ctx_t *ctx, int tab)
{
	PREF_INIT(ctx, &pref_sheet);
	PREF_TABDATA(ctx) = calloc(sizeof(pref_sheet_t), 1);
}
#undef PREF_INIT_FUNC
#define PREF_INIT_FUNC sch_dlg_pref_sheet_init
