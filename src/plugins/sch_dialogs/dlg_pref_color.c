/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *  Copied from pcb-rnd, interactive printed circuit board design
 *  Copyright (C) 2018,2021 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Preferences dialog, color tab */

#include <librnd/core/conf.h>
#include <librnd/plugins/lib_hid_common/dlg_pref.h>
#include <sch-rnd/conf_core.h>
#include "dlg_pen.h"

typedef struct {
	int *wclr;    /* color nodes (dynamic and hardwired items) */
	int nclr;

	int wpen[64]; /* pen (preference list) nodes (hardwired items only) */
	int npen;

	int wenab[64]; /* enable bool nodes (hardwired items only) */
	int nenab;

	int walldeco;  /* widget ID for the "all decoration" button */
	int decopen_min, decopen_max; /* wpen[] index bounds for all-deco pen buttons */
} pref_color_t;

#undef DEF_TABDATA
#define DEF_TABDATA pref_color_t *tabdata = PREF_TABDATA(ctx)

static char *all_deco = "all decoration pens";

static char *print_cfg_list(rnd_conf_native_t *nat)
{
	gds_t tmp = {0};

	rnd_conf_listitem_t *ci;
	rnd_conflist_t *lst = nat->val.list;
	int n = 0;

	for(ci = rnd_conflist_first(lst); ci != NULL; ci = rnd_conflist_next(ci)) {
		const char *pn = ci->val.string[0];
		if ((pn != NULL) && (*pn != '\0')) {
			if (n > 0)
				gds_append_str(&tmp, ", ");
			gds_append_str(&tmp, pn);
			n++;
		}
	}

	return tmp.array;
}

static void pref_color_brd2dlg(pref_ctx_t *ctx, rnd_design_t *dsg)
{
	DEF_TABDATA;
	rnd_conf_native_t *nat;
	int n;

	for(n = 0; n < tabdata->nclr; n++) {
		int w = tabdata->wclr[n];
		const char *path = ctx->dlg[w].user_data;
		nat = rnd_conf_get_field(path);
		if (nat != NULL)
			RND_DAD_SET_VALUE(ctx->dlg_hid_ctx, w, clr, nat->val.color[0]);
	}

	for(n = 0; n < tabdata->npen; n++) {
		int w = tabdata->wpen[n];
		const char *path = ctx->dlg[w].user_data;
		nat = rnd_conf_get_field(path);
		if (nat != NULL) {
			char *lab = print_cfg_list(nat);
			RND_DAD_SET_VALUE(ctx->dlg_hid_ctx, w, str, (lab == NULL ? "" : lab));
			free(lab);
		}
	}

	for(n = 0; n < tabdata->nenab; n++) {
		int w = tabdata->wenab[n];
		const char *path = ctx->dlg[w].user_data;
		nat = rnd_conf_get_field(path);
		if (nat != NULL)
			RND_DAD_SET_VALUE(ctx->dlg_hid_ctx, w, lng, nat->val.integer[0]);
	}


}


void csch_dlg_pref_color_open(pref_ctx_t *ctx, rnd_design_t *dsg, const char *tabdatareq)
{
	pref_color_brd2dlg(ctx, dsg);
}

void csch_dlg_pref_color_close(pref_ctx_t *ctx, rnd_design_t *dsg)
{
	DEF_TABDATA;
	int n;

	for(n = 0; n < tabdata->nclr; n++) {
		int w = tabdata->wclr[n];
		free(ctx->dlg[w].user_data);
	}

	free(tabdata->wclr);
}

static void pref_color_click_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_design_t *hl = rnd_gui->get_dad_design(hid_ctx);
	pref_ctx_t *ctx = caller_data;
	const char *path = attr->user_data;

	if (rnd_pref_dlg2conf_pre(hl, ctx) == NULL)
		return;

	rnd_conf_setf(ctx->role, path, -1, "%s", attr->val.clr.str);

	rnd_pref_dlg2conf_post(hl, ctx);

	rnd_gui->invalidate_all(rnd_gui); /* redraw because global color may have changed */
}

static void pref_pen_click_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_design_t *hl = rnd_gui->get_dad_design(hid_ctx);
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	pref_ctx_t *ctx = caller_data;
	DEF_TABDATA;
	int n, w = attr - ctx->dlg;
	const char *path = attr->user_data;
	const char *penname;

	if (rnd_pref_dlg2conf_pre(hl, ctx) == NULL)
		return;

	penname = sch_rnd_pen_dlg(sheet, &sheet->direct, NULL, 1, 0);
	if (penname == NULL)
		return; /* cancel */

	if (w == tabdata->walldeco) {
		for(n = tabdata->decopen_min; n < tabdata->decopen_max; n++) {
			rnd_hid_attribute_t *btna = &ctx->dlg[tabdata->wpen[n]];
			const char *path = btna->user_data;
			rnd_conf_setf(ctx->role, path, -1, "%s", penname);
		}
	}
	else
		rnd_conf_setf(ctx->role, path, -1, "%s", penname);

	pref_color_brd2dlg(ctx, hl);
	rnd_pref_dlg2conf_post(hl, ctx);
}

static void pref_enable_click_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_design_t *hl = rnd_gui->get_dad_design(hid_ctx);
	pref_ctx_t *ctx = caller_data;
	const char *path = attr->user_data;

	if (rnd_pref_dlg2conf_pre(hl, ctx) == NULL)
		return;

	rnd_conf_setf(ctx->role, path, -1, "%d", attr->val.lng);

	rnd_pref_dlg2conf_post(hl, ctx);
}

static void empty_col(pref_ctx_t *ctx)
{
	RND_DAD_BEGIN_VBOX(ctx->dlg);
	RND_DAD_END(ctx->dlg);
}

static void enable_col(pref_ctx_t *ctx, const char *enable_path, int *wenable)
{
	if (enable_path != NULL) {
		int w;

		RND_DAD_BEGIN_HBOX(ctx->dlg);
			RND_DAD_BOOL(ctx->dlg);
				w = *wenable = RND_DAD_CURRENT(ctx->dlg);
				ctx->dlg[w].user_data = rnd_strdup(enable_path);
				RND_DAD_CHANGE_CB(ctx->dlg, pref_enable_click_cb);
			RND_DAD_LABEL(ctx->dlg, "enable");
		RND_DAD_END(ctx->dlg);
	}
	else
		empty_col(ctx);
}


static int color_box(pref_ctx_t *ctx, const char *conf_path, const char *desc, const char *enable_path, int *wenable)
{
	int w;

	RND_DAD_LABEL(ctx->dlg, desc);
	RND_DAD_BEGIN_VBOX(ctx->dlg);
		RND_DAD_COLOR(ctx->dlg);
			w = RND_DAD_CURRENT(ctx->dlg);
			ctx->dlg[w].user_data = rnd_strdup(conf_path);
			RND_DAD_CHANGE_CB(ctx->dlg, pref_color_click_cb);
	RND_DAD_END(ctx->dlg);
	enable_col(ctx, enable_path, wenable);

	return w;
}

static int pen_box(pref_ctx_t *ctx, const char *conf_path, const char *desc, const char *enable_path, int *wenable)
{
	int w;
	const char *btn = (conf_path == all_deco) ? "<change>" : "";

	RND_DAD_LABEL(ctx->dlg, desc);
	RND_DAD_BUTTON(ctx->dlg, btn);
		w = RND_DAD_CURRENT(ctx->dlg);
		ctx->dlg[w].user_data = rnd_strdup(conf_path);
		RND_DAD_CHANGE_CB(ctx->dlg, pref_pen_click_cb);
	enable_col(ctx, enable_path, wenable);

	return w;
}

void csch_dlg_pref_color_create(pref_ctx_t *ctx, rnd_design_t *dsg)
{
	static const char *tabs[] = { "Tool/pen colors", "Generic colors", NULL };
	rnd_conf_native_t *nat;
	htsp_entry_t *e;
	int pl, w;
	const char *path_prefix = "appearance/color";
	DEF_TABDATA;

	tabdata->nclr = 64; /* make room for hardwired colors plus count dynamic conf colors */
	pl = strlen(path_prefix);
	rnd_conf_fields_foreach(e) {
		nat = e->value;
		if ((strncmp(e->key, path_prefix, pl) == 0) && (nat->type == RND_CFN_COLOR) && (nat->array_size == 1))
			tabdata->nclr++;
	}
	tabdata->wclr = calloc(sizeof(int), tabdata->nclr);

	tabdata->nclr = 0;
	tabdata->npen = 0;
	tabdata->nenab = 0;


	RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL);
	RND_DAD_BEGIN_TABBED(ctx->dlg, tabs);
		RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_LEFT_TAB);

		RND_DAD_BEGIN_VBOX(ctx->dlg); /* tool tab */
			RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_FRAME);
			RND_DAD_LABEL(ctx->dlg, "Decoration:");
			RND_DAD_BEGIN_TABLE(ctx->dlg, 3);
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_FRAME);
				tabdata->decopen_min = tabdata->npen;
				tabdata->wpen[tabdata->npen++] = tabdata->walldeco = pen_box(ctx, all_deco, "ALL DECORATION STROKES", NULL, NULL);
				tabdata->wpen[tabdata->npen++] = pen_box(ctx, "editor/style/tool_circle_stroke", "Circle tool stroke", NULL, NULL);
				tabdata->wpen[tabdata->npen++] = pen_box(ctx, "editor/style/tool_line_stroke", "Line tool stroke", NULL, NULL);
				tabdata->wpen[tabdata->npen++] = pen_box(ctx, "editor/style/tool_rect_stroke", "Rect. tool: stroke", "editor/style/tool_rect_has_stroke", &w);
				tabdata->wenab[tabdata->nenab++] = w;
				tabdata->wpen[tabdata->npen++] = pen_box(ctx, "editor/style/tool_rect_fill", "Rect. tool: fill", "editor/style/tool_rect_has_fill", &w);
				tabdata->wenab[tabdata->nenab++] = w;
				tabdata->decopen_max = tabdata->npen;
			RND_DAD_END(ctx->dlg);
			RND_DAD_LABEL(ctx->dlg, "Wiring:");
			RND_DAD_BEGIN_TABLE(ctx->dlg, 3);
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_FRAME);
				tabdata->wpen[tabdata->npen++] = pen_box(ctx, "editor/style/tool_wire_stroke", "Wire tool: wiring", NULL, NULL);
				tabdata->wpen[tabdata->npen++] = pen_box(ctx, "editor/style/tool_wire_junction_stroke", "Wire tool: junction dot", NULL, NULL);
			RND_DAD_END(ctx->dlg);
		RND_DAD_END(ctx->dlg);


		RND_DAD_BEGIN_VBOX(ctx->dlg); /* generic tab */
			RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL);
			RND_DAD_BEGIN_TABLE(ctx->dlg, 3);
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL);
				rnd_conf_fields_foreach(e) {
					nat = e->value;
					if ((strncmp(e->key, path_prefix, pl) == 0) && (nat->type == RND_CFN_COLOR) && (nat->array_size == 1))
						tabdata->wclr[tabdata->nclr++] = color_box(ctx, e->key, nat->description, NULL, NULL);
				}
			RND_DAD_END(ctx->dlg);
		RND_DAD_END(ctx->dlg);

	RND_DAD_END(ctx->dlg);
}

static const rnd_pref_tab_hook_t pref_color = {
	"Colors", RND_PREFTAB_AUTO_FREE_DATA | RND_PREFTAB_NEEDS_ROLE,
	csch_dlg_pref_color_open, csch_dlg_pref_color_close,
	csch_dlg_pref_color_create,
	pref_color_brd2dlg, pref_color_brd2dlg  /* board change, meta change */
};

static void csch_dlg_pref_color_init(pref_ctx_t *ctx, int tab)
{
	PREF_INIT(ctx, &pref_color);
	PREF_TABDATA(ctx) = calloc(sizeof(pref_color_t), 1);
}
#undef PREF_INIT_FUNC
#define PREF_INIT_FUNC csch_dlg_pref_color_init
