
extern const char csch_acts_TreeDialog[];
extern const char csch_acth_TreeDialog[];
fgw_error_t csch_act_TreeDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);


/* Timed regeneration of content */
void csch_dlg_tree_edit(csch_sheet_t *sheet);

/* (Almost) immediate regeneration of content */
void csch_dlg_tree_chg_sheet(csch_sheet_t *sheet);

/* Redraw preview after compilation */
void csch_dlg_tree_prj_compiled(csch_project_t *project);


void csch_dlg_tree_init(void);
void csch_dlg_tree_uninit(void);



