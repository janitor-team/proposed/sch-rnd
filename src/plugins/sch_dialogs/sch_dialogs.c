/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2020,2022 Tibor 'Igor2' Palinkas
 *  (copied from camv-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <string.h>

#include <librnd/core/event.h>
#include <librnd/core/actions.h>
#include <librnd/core/hidlib.h>
#include <librnd/core/rnd_conf.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/compat_fs.h>
#include <librnd/core/globalconst.h>
#include <librnd/core/plugins.h>
#include <librnd/core/conf_multi.h>
#include <librnd/plugins/lib_hid_common/dialogs_conf.h>
#include <librnd/plugins/lib_hid_common/dlg_pref.h>
#include <librnd/plugins/lib_hid_common/dlg_export.h>

#include <libcschem/event.h>

#include "dlg_attrib.h"
#include "dlg_about.h"
#include "dlg_abstract.h"
#include "dlg_undo.h"
#include "dlg_pen.h"
#include "dlg_text.h"
#include "dlg_tree.h"
#include "dlg_library.h"
#include "dlg_infobar.c"
#include "dlg_view.h"
#include "quick_attr.h"

#include "adialogs_conf.h"
#include "conf_internal.c"

conf_adialogs_t adialogs_conf;

static const char *sch_dialogs_cookie = "sch_dialogs";

static rnd_action_t sch_dialogs_action_list[] = {
	{"PrintGUI", rnd_act_PrintDialog, rnd_acth_PrintDialog, rnd_acts_PrintDialog},

	{"AttributeDialog", csch_act_AttributeDialog, csch_acth_AttributeDialog, csch_acts_AttributeDialog},
	{"AttributePick", csch_act_AttributePick, csch_acth_AttributePick, csch_acts_AttributePick},
	{"PenDialog", csch_act_PenDialog, csch_acth_PenDialog, csch_acts_PenDialog},
	{"EditText", csch_act_EditText, csch_acth_EditText, csch_acts_EditText},
	{"UndoDialog", csch_act_UndoDialog, csch_acth_UndoDialog, csch_acts_UndoDialog},
	{"TreeDialog", csch_act_TreeDialog, csch_acth_TreeDialog, csch_acts_TreeDialog},
	{"AbstractDialog", csch_act_AbstractDialog, csch_acth_AbstractDialog, csch_acts_AbstractDialog},
	{"LibraryDialog", csch_act_LibraryDialog, csch_acth_LibraryDialog, csch_acts_LibraryDialog},
	{"InfoBarFileChanged", csch_act_InfoBarFileChanged, csch_acth_InfoBarFileChanged, csch_acts_InfoBarFileChanged},
	{"ViewDialog", csch_act_ViewDialog, csch_acth_ViewDialog, csch_acts_ViewDialog},
	{"About", csch_act_About, csch_acth_About, csch_acts_About},

	{"quick_attr_role", csch_act_quick_attr_role, csch_acth_quick_attr_role, csch_acts_quick_attr_role},
	{"QuickAttr", csch_act_QuickAttr, csch_acth_QuickAttr, csch_acts_QuickAttr},
	{"QuickAttrEditable", csch_act_QuickAttrEditable, csch_acth_QuickAttrEditable, csch_acts_QuickAttrEditable}
};

extern int sch_dlg_pref_tab;
extern void (*sch_dlg_pref_first_init)(pref_ctx_t *ctx, int tab);


static void csch_dlg_ev_preunload(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_dlg_pen_preunload(sheet);
	csch_dlg_attr_preunload(sheet);
	csch_dlg_library_preunload(sheet);
	csch_dlg_tree_chg_sheet(sheet);
}

static void csch_dlg_ev_sheet_edit(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_dlg_tree_edit(sheet);
	csch_dlg_attr_edit(sheet);
}

static void csch_dlg_ev_obj_attr_edit(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	if (argv[1].type == RND_EVARG_PTR)
		csch_dlg_attr_obj_attr_edit(sheet, argv[1].d.p);
}

static void csch_dlg_ev_prj_compiled(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_dlg_attr_compiled((csch_project_t *)sheet->hidlib.project);
	csch_dlg_abst_compiled((csch_project_t *)sheet->hidlib.project);
	csch_dlg_tree_prj_compiled((csch_project_t *)sheet->hidlib.project);
}

static void csch_dlg_ev_library_changed(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_dlg_library_changed(sheet);
}

static void csch_dlg_ev_board_changed(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_dlg_undo_brd_changed_ev(sheet);
}

static void csch_dlg_ev_sheet_postload(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	csch_dlg_library_changed(sheet);
	csch_dlg_tree_chg_sheet(sheet);
}


int pplg_check_ver_sch_dialogs(int ver_needed) { return 0; }

void pplg_uninit_sch_dialogs(void)
{
	csch_dlg_undo_uninit();
	rnd_event_unbind_allcookie(sch_dialogs_cookie);
	rnd_remove_actions_by_cookie(sch_dialogs_cookie);
	csch_dlg_tree_uninit();
	csch_dlg_abst_uninit();
	csch_dlg_library_uninit();
	sch_rnd_attr_quick_uninit();
	rnd_dlg_pref_uninit();
	rnd_conf_plug_unreg("plugins/dialogs/", adialogs_conf_internal, sch_dialogs_cookie);

}

int pplg_init_sch_dialogs(void)
{
	RND_API_CHK_VER;

	RND_REGISTER_ACTIONS(sch_dialogs_action_list, sch_dialogs_cookie);
	rnd_dlg_pref_init(sch_dlg_pref_tab, sch_dlg_pref_first_init);
	csch_dlg_tree_init();
	csch_dlg_abst_init();
	csch_dlg_library_init();
	csch_dlg_undo_init();
	rnd_event_bind(CSCH_EVENT_SHEET_PREUNLOAD, csch_dlg_ev_preunload, NULL, sch_dialogs_cookie);
	rnd_event_bind(CSCH_EVENT_SHEET_EDITED, csch_dlg_ev_sheet_edit, NULL, sch_dialogs_cookie);
	rnd_event_bind(CSCH_EVENT_OBJ_ATTR_EDITED, csch_dlg_ev_obj_attr_edit, NULL, sch_dialogs_cookie);
	rnd_event_bind(CSCH_EVENT_PRJ_COMPILED, csch_dlg_ev_prj_compiled, NULL, sch_dialogs_cookie);
	rnd_event_bind(CSCH_EVENT_LIBRARY_CHANGED, csch_dlg_ev_library_changed, NULL, sch_dialogs_cookie);
	rnd_event_bind(RND_EVENT_DESIGN_SET_CURRENT, csch_dlg_ev_board_changed, NULL, sch_dialogs_cookie);
	rnd_event_bind(CSCH_EVENT_SHEET_POSTLOAD, csch_dlg_ev_sheet_postload, NULL, sch_dialogs_cookie);


	rnd_conf_plug_reg(adialogs_conf, adialogs_conf_internal, sch_dialogs_cookie);

#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(adialogs_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "adialogs_conf_fields.h"

	return 0;
}
