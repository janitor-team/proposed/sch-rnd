/* Macros only so that plugins using this file do not start depending on
   sch_dialogs - all communication are made through central infra,
   e.g. actions */

#define QUICK_ATTR_GET_GRP(grp, actname) \
do { \
	if (argc < 2) { \
		rnd_message(RND_MSG_ERROR, actname ": missing argument 1 (group object)\n"); \
		return FGW_ERR_PTR_DOMAIN; \
	} \
	grp = argv[1].val.ptr_void; \
	if ((argv[1].type != (FGW_PTR | FGW_STRUCT)) || (!fgw_ptr_in_domain(&rnd_fgw, &argv[1], CSCH_PTR_DOMAIN_COBJ))) { \
		rnd_message(RND_MSG_ERROR, actname ": argument 1 needs to be a concrete group object\n"); \
		return FGW_ERR_PTR_DOMAIN; \
	} \
	if (!csch_obj_is_grp(&grp->hdr)) { \
		rnd_message(RND_MSG_ERROR, actname ": object is not a group, can't set role\n"); \
		return FGW_ERR_ARG_CONV; \
	} \
} while(0)
