/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* abstract tree dialog */

#include <libcschem/config.h>

#include <genht/htpp.h>
#include <genht/htpi.h>
#include <genht/hash.h>

#include <librnd/core/event.h>
#include <librnd/core/actions.h>
#include <librnd/core/hidlib.h>
#include <librnd/core/rnd_conf.h>
#include <librnd/hid/hid_dad.h>
#include <librnd/hid/hid_dad_tree.h>
#include <librnd/core/rnd_printf.h>

#include <libcschem/abstract.h>
#include <libcschem/project.h>
#include <libcschem/actions_csch.h>

#include "abst_attr.h"

typedef struct abst_dlg_ctx_s {
	RND_DAD_DECL_NOINIT(dlg)

	abst_attrdlg_ctx_t right;
	csch_project_t *prj;
	gds_t tmp;

	int wtree;

	htip_t aid2row;
} abst_dlg_ctx_t;

static htpp_t prj2dlg;

static void abst_dlg_close_cb(void *caller_data, rnd_hid_attr_ev_t ev)
{
	abst_dlg_ctx_t *ctx = caller_data;
	gds_uninit(&ctx->tmp);
	htpp_pop(&prj2dlg, ctx->prj);
	htip_uninit(&ctx->aid2row);
	free(ctx);
}

static rnd_hid_row_t *put_in_tree(abst_dlg_ctx_t *ctx, rnd_hid_attribute_t *attr, csch_ahdr_t *a);

static char *get_aname(abst_dlg_ctx_t *ctx, rnd_hid_attribute_t *attr, csch_ahdr_t *a, rnd_hid_row_t **parent)
{
	rnd_hid_tree_t *tree = attr->wdata;

	switch(a->type) {
		case CSCH_ATYPE_NET:
			{
				static char tmp[] = "net";
				*parent = rnd_dad_tree_mkdirp(tree, tmp, NULL);
				return rnd_strdup(((csch_anet_t *)a)->name);
			}
		case CSCH_ATYPE_COMP:
			{
				static char tmp[] = "comp";
				*parent = rnd_dad_tree_mkdirp(tree, tmp, NULL);
				return rnd_strdup(((csch_acomp_t *)a)->name);
			}
		case CSCH_ATYPE_PORT:
			{
				static char tmp[] = "port";
				csch_aport_t *p = (csch_aport_t *)a;
				csch_acomp_t *c = p->parent;
				if ((c != NULL) && (c->hdr.type == CSCH_ATYPE_COMP)) {
					ctx->tmp.used = 0;
					gds_append_str(&ctx->tmp, "comp/");
					gds_append_str(&ctx->tmp, c->name);
					*parent = htsp_get(&tree->paths, ctx->tmp.array);
					if (*parent == NULL)
						*parent = put_in_tree(ctx, attr, &c->hdr);
					return rnd_strdup(p->name);
				}
				*parent = rnd_dad_tree_mkdirp(tree, tmp, NULL);
				return rnd_strdup(p->name);
			}

		case CSCH_ATYPE_BUSNET:
		case CSCH_ATYPE_BUSCHAN:
		case CSCH_ATYPE_BUSPORT:
		case CSCH_ATYPE_HUB:
		case CSCH_ATYPE_INVALID:
			break;
	}

	{
		static char tmp[] = "unknwon";
		*parent = rnd_dad_tree_mkdirp(tree, tmp, NULL);
	}
	return rnd_strdup_printf("%ld", a->aid);
}

static rnd_hid_row_t *put_in_tree(abst_dlg_ctx_t *ctx, rnd_hid_attribute_t *attr, csch_ahdr_t *a)
{
	char *cell[2];
	rnd_hid_row_t *parent, *r;

	cell[0] = get_aname(ctx, attr, a, &parent);
	cell[1] = NULL;
	r = rnd_dad_tree_append_under(attr, parent, cell);
	r->user_data = a;

	htip_set(&ctx->aid2row, a->aid, r);

	return r;
}

static void abst_prj2dlg(abst_dlg_ctx_t *ctx)
{
	rnd_hid_attribute_t *attr = &ctx->dlg[ctx->wtree];
	rnd_hid_tree_t *tree = attr->wdata;
	rnd_hid_row_t *r;
	char *cursor_path = NULL;
	htip_entry_t *e;

	htip_clear(&ctx->aid2row);

	/* remember cursor */
	r = rnd_dad_tree_get_selected(attr);
	if (r != NULL)
		cursor_path = rnd_strdup(r->cell[0]);

	/* remove existing items */
	rnd_dad_tree_clear(tree);

	/* add all non-ghost items recursively */
	if (ctx->prj->abst != NULL) {
		for(e = htip_first(&ctx->prj->abst->aid2obj); e != NULL; e = htip_next(&ctx->prj->abst->aid2obj, e)) {
			csch_ahdr_t *a = e->value;
			if (a->ghost == NULL)
				put_in_tree(ctx, attr, a);
		}

		/* restore cursor */
		if (cursor_path != NULL) {
			rnd_hid_attr_val_t hv;
			hv.str = cursor_path;
			rnd_gui->attr_dlg_set_value(ctx->dlg_hid_ctx, ctx->wtree, &hv);
			free(cursor_path);
		}
	}
}

static csch_ahdr_t *get_dlg_obj(abst_dlg_ctx_t *ctx)
{
	rnd_hid_attribute_t *attr = &ctx->dlg[ctx->wtree];
	rnd_hid_row_t *r = rnd_dad_tree_get_selected(attr);

	return r == NULL ? NULL : r->user_data;
}

static void abst_select_node_cb(rnd_hid_attribute_t *attrib, void *hid_ctx, rnd_hid_row_t *row)
{
	rnd_hid_tree_t *tree = attrib->wdata;
	abst_dlg_ctx_t *ctx = tree->user_ctx;
	csch_ahdr_t *a = get_dlg_obj(ctx);
	aattr_dlg_sheet2dlg_abstract(&ctx->right, a);
}

static void aattr_select(rnd_hid_attribute_t *attrib, void *hid_ctx, rnd_hid_row_t *row)
{
	rnd_hid_tree_t *tree = attrib->wdata;
	abst_dlg_ctx_t *ctx = tree->user_ctx;
	csch_ahdr_t *a = get_dlg_obj(ctx);

	aattr_dlg_ahist2dlg(&ctx->right, a); /* update history */
}

static void aattr_sources_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	abst_dlg_ctx_t *ctx = caller_data;
	aattr_sources(&ctx->right);
}

static void aattr_attr_src_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	abst_dlg_ctx_t *ctx = caller_data;
	aattr_attr_src(&ctx->right);
}


void sch_rnd_abst_dlg(csch_project_t *prj, long aid, const char *attr_name)
{
	abst_dlg_ctx_t *ctx;
	rnd_hid_dad_buttons_t clbtn[] = {{"Close", 0}, {NULL, 0}};
	const char *hdr[] = {"name", NULL};

	ctx = htpp_get(&prj2dlg, prj);
	if (ctx != NULL) {
		TODO("raise?");
		goto jump;
	}

	ctx = calloc(sizeof(abst_dlg_ctx_t), 1);
	ctx->prj = prj;
	htpp_set(&prj2dlg, prj, ctx);
	htip_init(&ctx->aid2row, longhash, longkeyeq);

	RND_DAD_BEGIN_VBOX(ctx->dlg);
		RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL);
		RND_DAD_BEGIN_HPANE(ctx->dlg, "left-right");
			RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL);
			RND_DAD_BEGIN_VBOX(ctx->dlg); /* left */
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL);
				RND_DAD_TREE(ctx->dlg, 1, 1, hdr); /* top left: tree */
					RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL);
					RND_DAD_TREE_SET_CB(ctx->dlg, selected_cb, abst_select_node_cb);
					RND_DAD_TREE_SET_CB(ctx->dlg, ctx, ctx);
					ctx->wtree = RND_DAD_CURRENT(ctx->dlg);
			RND_DAD_END(ctx->dlg);

			RND_DAD_BEGIN_VBOX(ctx->dlg); /* right */
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL);
				aattr_dlg_create(&ctx->right, ctx->dlg, prj);
				RND_DAD_BUTTON_CLOSES(ctx->dlg, clbtn);
			RND_DAD_END(ctx->dlg);
		RND_DAD_END(ctx->dlg);

	RND_DAD_END(ctx->dlg);

	RND_DAD_DEFSIZE(ctx->dlg, 300, 400);
	RND_DAD_NEW("AbstDialog", ctx->dlg, "Abstract model", ctx, 0, abst_dlg_close_cb); /* type=local */

	aattr_dlg_init(&ctx->right);
	abst_prj2dlg(ctx);

	jump:;
	if (aid >= 0) {
		rnd_hid_row_t *r = htip_get(&ctx->aid2row, aid);
		if (r != NULL) {
			rnd_hid_attr_val_t hv;
			hv.str = r->path;
			rnd_gui->attr_dlg_set_value(ctx->dlg_hid_ctx, ctx->wtree, &hv);
			aattr_dlg_sheet2dlg_abstract(&ctx->right, r->user_data);
			if (attr_name != NULL)
				aattr_dlg_select_attr(&ctx->right, attr_name);
		}
	}
}

const char csch_acts_AbstractDialog[] = "AbstractDialog([abst_id [,attr_name]])";
const char csch_acth_AbstractDialog[] = "Bring up the current project's abstract model dialog. If abstract object id, abst_id is specified, left-side cursor is set at that object initially. If attr_name is specified, right-side cursor is set on that attribute.\n";
fgw_error_t csch_act_AbstractDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hidlib = RND_ACT_DESIGN;
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	long aid = -1;
	const char *attr_name;

	RND_ACT_MAY_CONVARG(1, FGW_LONG, AbstractDialog, aid = argv[1].val.nat_long);
	RND_ACT_MAY_CONVARG(2, FGW_STR,  AbstractDialog, attr_name = argv[2].val.str);

	sch_rnd_abst_dlg((csch_project_t *)sheet->hidlib.project, aid, attr_name);
	return 0;
}

void csch_dlg_abst_compiled(csch_project_t *prj)
{
	abst_dlg_ctx_t *ctx = htpp_get(&prj2dlg, prj);
	if (ctx != NULL)
		abst_prj2dlg(ctx);
}


void csch_dlg_abst_init(void)
{
	htpp_init(&prj2dlg, ptrhash, ptrkeyeq);
}

void csch_dlg_abst_uninit(void)
{
	rnd_dad_retovr_t retovr = {0};
	htpp_entry_t *e;

	for(e = htpp_first(&prj2dlg); e != NULL; e = htpp_next(&prj2dlg, e)) {
		abst_dlg_ctx_t *ctx = e->value;
		rnd_hid_dad_close(ctx->dlg_hid_ctx, &retovr, 0);
	}

	htpp_uninit(&prj2dlg);
}
