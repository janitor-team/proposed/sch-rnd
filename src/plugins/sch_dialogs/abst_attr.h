/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/abstract.h>
#include <libcschem/project.h>

typedef struct {
	rnd_hid_attribute_t **dlg;
	void **dlg_hid_ctx;
	csch_project_t *prj;
	csch_ahdr_t *last_ahdr;
	int wright, waattrs, wahistory, waname;
	int wdetaillab, wdetailtree;
} abst_attrdlg_ctx_t;


void aattr_dlg_ahist2dlg(abst_attrdlg_ctx_t *actx, csch_ahdr_t *aobj);
void aattr_dlg_sheet2dlg_abstract(abst_attrdlg_ctx_t *actx, csch_ahdr_t *a);

/* Call this after the dialog is created */
void aattr_dlg_init(abst_attrdlg_ctx_t *actx);

/* Call this from aattr_sources_cb() */
void aattr_sources(abst_attrdlg_ctx_t *actx);

/* Call this from aattr_attr_src_cb() */
void aattr_attr_src(abst_attrdlg_ctx_t *actx);


/* Create widgets for an abst_attrdlg_ctx_t in DAD context dlg */
#define aattr_dlg_create(actx, dlg, prj)       aattr_dlg_create_((actx), dlg, (prj))

/* Select the row for attr_name */
void aattr_dlg_select_attr(abst_attrdlg_ctx_t *actx, const char *attr_name);


/*** implementation ***/

#define aattr_dlg_create_(actx, Dlg, Prj) \
	do { \
		static const char *ahdr[] = {"key", "value", NULL}; \
\
		actx->wright = RND_DAD_CURRENT(Dlg); \
		actx->dlg = &Dlg; \
		actx->dlg_hid_ctx = &Dlg ## _hid_ctx; \
		actx->prj = Prj; \
\
		RND_DAD_BEGIN_HBOX(Dlg); \
			RND_DAD_LABEL(Dlg, "Abstract:"); \
\
			RND_DAD_BEGIN_HBOX(ctx->dlg); \
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL); \
			RND_DAD_END(ctx->dlg); \
\
			RND_DAD_LABEL(Dlg, "<none>"); \
				actx->waname = RND_DAD_CURRENT(Dlg); \
		RND_DAD_END(Dlg); \
\
		RND_DAD_TREE(Dlg, 2, 0, ahdr); \
			RND_DAD_COMPFLAG(Dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL); \
			actx->waattrs = RND_DAD_CURRENT(Dlg); \
			RND_DAD_TREE_SET_CB(Dlg, selected_cb, aattr_select); \
			RND_DAD_TREE_SET_CB(Dlg, ctx, ctx); \
\
		RND_DAD_LABEL(Dlg, "Attribute history:"); \
		RND_DAD_TREE(Dlg, 1, 0, NULL); \
			RND_DAD_COMPFLAG(Dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL); \
			actx->wahistory = RND_DAD_CURRENT(Dlg); \
\
		RND_DAD_BEGIN_HBOX(ctx->dlg); \
			RND_DAD_LABEL(Dlg, "Details:"); \
				actx->wdetaillab = RND_DAD_CURRENT(Dlg); \
			RND_DAD_BEGIN_HBOX(ctx->dlg); \
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_EXPFILL); \
			RND_DAD_END(ctx->dlg); \
			RND_DAD_BUTTON(Dlg, "history src"); \
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_TIGHT); \
				RND_DAD_CHANGE_CB(ctx->dlg, aattr_attr_src_cb); \
				RND_DAD_HELP(Dlg, "Go to the source of the selected row of attribute history"); \
			RND_DAD_BUTTON(Dlg, "sources"); \
				RND_DAD_COMPFLAG(ctx->dlg, RND_HATF_TIGHT); \
				RND_DAD_CHANGE_CB(ctx->dlg, aattr_sources_cb); \
				RND_DAD_HELP(Dlg, "Present a list of concrete model objects\nthat contributed to this abstract object\nin the last compilation"); \
		RND_DAD_END(ctx->dlg); \
		RND_DAD_TREE(Dlg, 1, 0, NULL); \
			RND_DAD_COMPFLAG(Dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL); \
			actx->wdetailtree = RND_DAD_CURRENT(Dlg); \
\
	} while(0)


