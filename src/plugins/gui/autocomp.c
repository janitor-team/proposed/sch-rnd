/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#define ACOMP_TIMER_RES_MS 250


static void autocomp_update_bar(csch_sheet_t *sheet)
{
	rnd_hid_attr_val_t hv;

	if (!status.active)
		return;

	if (sheet->acp_starting == 0)
		hv.dbl = 0;
	else
		hv.dbl = (double)sheet->acp_remain / (double)sheet->acp_starting;
	rnd_gui->attr_dlg_set_value(status.rdsub.dlg_hid_ctx, status.wcomp_bar, &hv);
}

static void autocomp_compile(rnd_design_t *hl)
{
	rnd_actionva(hl, "CompileProject", NULL);
	autocomp_update_bar((csch_sheet_t *)hl);
}

static void autocomp_tick(rnd_hidval_t user_data)
{
	csch_sheet_t *sheet;

	if (!status.acomp_timer_want) {
		status.acomp_timer_active = 0;
		return;
	}
	status.acomp_timer = rnd_gui->add_timer(rnd_gui, autocomp_tick, ACOMP_TIMER_RES_MS, user_data);
	status.acomp_timer_active = 1;

	sheet = (csch_sheet_t *)rnd_multi_get_current();

/*rnd_trace("acomp tick: %ld / %ld\n", sheet->acp_remain, sheet->acp_starting);*/

	if (sheet != NULL) {
		if (sheet->acp_remain > 0) {
			sheet->acp_remain -= ACOMP_TIMER_RES_MS;
			if (sheet->acp_remain <= 0) {
				sheet->acp_remain = 0;
				autocomp_compile(&sheet->hidlib);
			}
			else
				autocomp_update_bar(sheet);
		}
	}
}

static void autocomp_restart_timer(rnd_design_t *hl)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;

	if ((csch_project_t *)sheet->hidlib.project == &sch_rnd_buffer_prj)
		return; /* do not compile buffers */

	sheet->acp_remain = sheet->acp_starting = conf_core.editor.autocomp_time;

	/* immediate compile */
	if (sheet->acp_remain <= 0) {
		sheet->acp_remain = 0;
		autocomp_compile(hl);
		return;
	}

	/* start timer if it's not running */
	if (!status.acomp_timer_active) {
		rnd_hidval_t hv = {0};
		rnd_gui->add_timer(rnd_gui, autocomp_tick, ACOMP_TIMER_RES_MS, hv);
		status.acomp_timer_want = 1;
		status.acomp_timer_active = 1;
	}

	autocomp_update_bar(sheet);
}

static void autocomp_stop_timer(rnd_design_t *hl)
{
	status.acomp_timer_want = 0;
}
