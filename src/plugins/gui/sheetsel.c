/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <librnd/config.h>

#include <genvector/vti0.h>
#include <genvector/vtp0.h>

#include <librnd/hid/hid.h>
#include <librnd/core/hid_cfg.h>
#include <librnd/hid/hid_dad.h>
#include <librnd/hid/hid_dad_tree.h>

#include <librnd/core/actions.h>
#include <librnd/core/event.h>
#include <librnd/core/rnd_conf.h>

#include <libcschem/concrete.h>
#include <libcschem/project.h>

#include <sch-rnd/multi.h>

#include "sheetsel.h"

typedef struct sheetsel_ctx_s sheetsel_ctx_t;
struct sheetsel_ctx_s {
	rnd_hid_dad_subdialog_t sub;
	int sub_inited, lock;
	int wtree;
};

static sheetsel_ctx_t sheetsel;

static void sheetsel_prj2dlg(sheetsel_ctx_t *ss)
{
	rnd_hid_attribute_t *attr = &ss->sub.dlg[ss->wtree];
	rnd_hid_tree_t *tree = attr->wdata;
	rnd_hid_row_t *r, *rprj;
	htsp_entry_t *e;
	char *cell[2];

	/* remove existing items */
	rnd_dad_tree_clear(tree);

	/* add all items */
	cell[1] = NULL;
	for(e = htsp_first(&rnd_projects); e != NULL; e = htsp_next(&rnd_projects, e)) {
		long n;
		csch_project_t *prj = e->value;
		const char *bn;
		char *end;
		int seps;

		seps = 0;
		for(bn = e->key + strlen(e->key) - 1; bn > e->key; bn--) {
			if ((bn[0] == '/') && (bn[1] != '/')) {
				seps++;
				if (seps == 2) {
					bn++;
					break;
				}
			}
		}

		cell[0] = rnd_strdup(bn);
		end = strchr(cell[0], '/');
		if (end != NULL)
			*end = '\0';

		rprj = rnd_dad_tree_append(attr, NULL, cell);
		rprj->user_data = NULL;

		for(n = 0; n < prj->hdr.designs.used; n++) {
			csch_sheet_t *sheet = prj->hdr.designs.array[n];
			if (sheet->hidlib.loadname != NULL) {
				bn = strrchr(sheet->hidlib.loadname, '/');
				if (bn != NULL)
					bn++;
				else
					bn = sheet->hidlib.loadname;
			}
			else
				bn = "<none>";

			if (sheet->prj_non_root)
				cell[0] = rnd_concat("(", bn, ")", NULL);
			else
				cell[0] = rnd_strdup(bn);
			r = rnd_dad_tree_append_under(attr, rprj, cell);
			r->user_data = sheet;
		}
	}

	rnd_dad_tree_expcoll(attr, NULL, 1, 1);

}

static void sheetsel_select_current(sheetsel_ctx_t *ss)
{
	rnd_hid_attribute_t *attr = &ss->sub.dlg[ss->wtree];
	rnd_hid_tree_t *tree = attr->wdata;
	rnd_design_t *curr = rnd_multi_get_current();
	rnd_hid_row_t *r, *actr = NULL;
	htsp_entry_t *e;

	for(e = htsp_first(&tree->paths); e != NULL; e = htsp_next(&tree->paths, e)) {
		r = e->value;
		if (r->user_data == curr)
			actr = e->value;
	}

	/* set cursor to currently active sheet */
	if (actr != NULL) {
		rnd_hid_attr_val_t hv;
		hv.str = actr->path;
		rnd_gui->attr_dlg_set_value(ss->sub.dlg_hid_ctx, ss->wtree, &hv);
	}
}


static void sheetsel_select_cb(rnd_hid_attribute_t *attrib, void *hid_ctx, rnd_hid_row_t *row)
{
	rnd_hid_tree_t *tree = attrib->wdata;
	sheetsel_ctx_t *ctx = tree->user_ctx;

	if ((row == NULL) || (row->user_data == NULL))
		return;

	ctx->lock++;
	sch_rnd_multi_switch_to(row->user_data);
	ctx->lock--;
}

static void sheetsel_docked_create(sheetsel_ctx_t *ss)
{
	RND_DAD_BEGIN_VBOX(ss->sub.dlg);
		RND_DAD_COMPFLAG(ss->sub.dlg, RND_HATF_EXPFILL);

		RND_DAD_TREE(ss->sub.dlg, 1, 1, NULL);
			RND_DAD_COMPFLAG(ss->sub.dlg, RND_HATF_EXPFILL | RND_HATF_SCROLL | RND_HATF_TREE_NO_AUTOEXP);
			RND_DAD_TREE_SET_CB(ss->sub.dlg, selected_cb, sheetsel_select_cb);
			RND_DAD_TREE_SET_CB(ss->sub.dlg, ctx, ss);
			ss->wtree = RND_DAD_CURRENT(ss->sub.dlg);

	RND_DAD_END(ss->sub.dlg);
	RND_DAD_DEFSIZE(ss->sub.dlg, 210, 200);
	RND_DAD_MINSIZE(ss->sub.dlg, 100, 100);
}


static void sheetsel_build(void)
{
	sheetsel_docked_create(&sheetsel);
	if (rnd_hid_dock_enter(&sheetsel.sub, RND_HID_DOCK_LEFT, "sheetsel") == 0) {
		sheetsel.sub_inited = 1;
		sheetsel_prj2dlg(&sheetsel);
		sheetsel_select_current(&sheetsel);
	}
}

void sch_rnd_sheetsel_gui_init_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	if ((RND_HAVE_GUI_ATTR_DLG) && (rnd_gui->get_menu_cfg != NULL))
		sheetsel_build();
}

void sch_sheetsel_board_changed_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	if (sheetsel.sub_inited && !sheetsel.lock)
		sheetsel_select_current(&sheetsel);
}

void sch_sheetsel_load_post_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	if (sheetsel.sub_inited && !sheetsel.lock) {
		sheetsel_prj2dlg(&sheetsel);
		sheetsel_select_current(&sheetsel);
	}
}

void sch_sheetsel_unload_post_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	sch_sheetsel_load_post_ev(hidlib, user_data, argc, argv);
}
