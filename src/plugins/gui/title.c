/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - GUI
 *  Copyright (C) 2019,2022 Tibor 'Igor2' Palinkas - in pcb-rnd
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas - tuned for sch-rnd
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <libcschem/config.h>

#include <genvector/gds_char.h>

#include <libcschem/concrete.h>

static gds_t title_buf;
static int gui_inited = 0, brd_changed;

static void update_title(rnd_design_t *hl, int changed, int is_symbol)
{
	const char *filename, *name;

	if ((rnd_gui == NULL) || (rnd_gui->set_top_title == NULL) || (!gui_inited))
		return;

	/* change title only if current sheet's changed */
	if (rnd_multi_get_current() != hl)
		return;

	if ((hl->name == NULL) || (*hl->name == '\0'))
		name = "Unnamed";
	else
		name = hl->name;

	if ((hl->fullpath == NULL) || (*hl->fullpath == '\0'))
		filename = "<sheet with no file name or format>";
	else
		filename = hl->fullpath;

	title_buf.used = 0;
	rnd_append_printf(&title_buf, "%s%s (%s) - %s - sch-rnd", changed ? "*" : "", name, filename, is_symbol ? "symbol" : "sheet");
	rnd_gui->set_top_title(rnd_gui, title_buf.array);
}

static void sch_title_board_changed_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *pcb = (csch_sheet_t *)hidlib;
	brd_changed = 0;
	update_title(hidlib, pcb->changed, pcb->is_symbol);
}

static void sch_title_meta_changed_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *pcb = (csch_sheet_t *)hidlib;
	brd_changed = pcb->changed;
	update_title(hidlib, pcb->changed, pcb->is_symbol);
}

static void sch_title_gui_init_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	csch_sheet_t *pcb = (csch_sheet_t *)hidlib;
	gui_inited = 1;
	update_title(hidlib, pcb->changed, pcb->is_symbol);
}

