void sch_status_gui_init_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void sch_status_st_update_conf(rnd_conf_native_t *cfg, int arr_idx, void *user_data);
void sch_status_st_update_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void sch_status_rd_update_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void sch_status_rd_edit_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void sch_status_view_activated_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void sch_status_postload_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);

extern const char csch_acts_StatusSetText[];
extern const char csch_acth_StatusSetText[];
extern fgw_error_t csch_act_StatusSetText(fgw_arg_t *res, int argc, fgw_arg_t *argv);

