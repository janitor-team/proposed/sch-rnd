refdes U??
attr_invis -sym-source       sch-rnd default symbol lib
attr_invis -sym-copyright    (C) 2022 Tibor 'Igor2' Palinkas
attr_invis -sym-license-dist GPLv2+
attr_invis -sym-license-use  Public Domain

pinalign bottom center
pinalign left center
pinalign right center

shape box
begin pin in
	num 1
	loc left
end pin

begin pin gnd
	num 2
	loc bottom
end pin

begin pin out
	num 3
	loc right
end pin


