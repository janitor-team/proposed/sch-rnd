BEGIN {
	help_auto()
	set_arg(P, "?sequence", "normal")
	set_arg(P, "?nx", "1")

	proc_args(P, "ny,nx,eshift,etrunc", "ny")

	P["nx"] = int(P["nx"])
	P["ny"] = int(P["ny"])

	eshift=tolower(P["eshift"])
	etrunc=tobool(P["etrunc"])
	if ((eshift != "x") && (eshift != "y") && (eshift != "") && (eshift != "none"))
		error("eshift must be x or y or none (got: " eshift ")");

	if ((eshift == "x") || (eshift == "y"))
		step = pin_grid*2;
	else
		step = pin_grid;

	if (P["nx"] > 2) {
		mult=int(P["nx"]/2)
		while(mult > 0) {
			step = step * 2;
			mult--
		}
	}

	half = int(step/2);


	sym_begin("CONN??", 0, -half)

	for(x = 0; x < P["nx"]; x++) {
		if ((eshift == "x") && ((x % 2) == 1))
			yo = step/2
		else
			yo = 0
		for(y = 0; y < P["ny"]; y++) {
			if ((eshift == "y") && ((y % 2) == 1)) {
				xo = step/2
				if ((etrunc) && (x == P["nx"]-1))
					continue
			}
			else {
				xo = 0
				if ((etrunc) && (y == P["ny"]-1) && (yo != 0))
					continue
			}
			if (P["sequence"] == "normal") {
				pinno++
			}
			else if (P["sequence"] == "pivot") {
				pinno = y * P["nx"] + x + 1
			}
			else if (P["sequence"] == "zigzag") {
				if (x % 2) {
					pinno = (x+1) * P["ny"] - y
					if ((etrunc) && (eshift == "x"))
						pinno -= int(x/2)+1
					else if ((etrunc) && (eshift == "y"))
						pinno += int(x/2)
				}
				else {
					pinno = x * P["ny"] + y + 1
					if ((etrunc) && (eshift == "x"))
						pinno -= x/2
					else if ((etrunc) && (eshift == "y"))
						pinno += x/2-1
				}
			}

			if (x < 1)
				pdir="left";
			else
				pdir="right";
			sym_term(SYM,   x * step + xo, y * step + yo, pdir, pinno)
		}
	}

	xo = 0
	yo = 0
	if (!etrunc) {
		if (eshift == "y")
			xo = step/2
		if (eshift == "x")
			yo = step/2
	}

	xs = P["nx"]-1;
	if (xs == 0)
		xs = 1;
	sym_rect(SYM, 0, -half,  xs * step + xo, P["ny"] * step + yo - half)

	for(x = 1; x < P["nx"]-1; x++) {
		xx = x * step + xo
		sym_line(SYM, xx, -half, xx, P["ny"] * step + yo - half)
	}

	sym_end()

}
